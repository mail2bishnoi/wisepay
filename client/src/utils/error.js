import { toast } from "react-toastify";
export const getErrorMessage = error => {
  let message = "Network Error ...";

  if (typeof error !== "undefined") {
    if (error.hasOwnProperty("message")) {
      message = error.message;
    }
  }
  if (typeof error.response !== "undefined") {
    //Setup Generic Response Messages
    if (error.response.status === 401) {
      message = "UnAuthorized";
    } else if (error.response.status === 404) {
      message = "API Route is Missing or Undefined";
    } else if (error.response.status === 405) {
      message = "API Route Method Not Allowed";
    } else if (error.response.status === 422) {
      //Validation Message
    } else if (error.response.status >= 500) {
      message.html = "Server Error";
    }

    //Try to Use the Response Message
    if (
      error.hasOwnProperty("response") &&
      error.response.hasOwnProperty("data")
    ) {
      if (
        error.response.data.hasOwnProperty("message") &&
        error.response.data.message.length > 0
      ) {
        message = error.response.data.message;
      }
    }
  }
  toast.error(message);
};

export const handleBbpsError = responseData => {
  console.log(responseData);
  if (responseData.jsonResponse) {
    if (
      responseData.jsonResponse.billFetchResponse &&
      responseData.jsonResponse.billFetchResponse.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.billFetchResponse.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    } else if (
      responseData.jsonResponse.ExtBillPayResponse &&
      responseData.jsonResponse.ExtBillPayResponse.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.ExtBillPayResponse.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    } else if (
      responseData.jsonResponse.transactionStatusResp &&
      responseData.jsonResponse.transactionStatusResp.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.transactionStatusResp.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    } else if (
      responseData.jsonResponse.complaintRegistrationResp &&
      responseData.jsonResponse.complaintRegistrationResp.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.complaintRegistrationResp.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    } else if (
      responseData.jsonResponse.complaintRegistrationResp &&
      responseData.jsonResponse.complaintRegistrationResp.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.complaintRegistrationResp.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    } else if (
      responseData.jsonResponse.complaintTrackingResp &&
      responseData.jsonResponse.complaintTrackingResp.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.complaintTrackingResp.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    } else if (
      responseData.jsonResponse.billValidationResponse &&
      responseData.jsonResponse.billValidationResponse.responseCode === 1
    ) {
      const {
        errorCode,
        errorMessage
      } = responseData.jsonResponse.billValidationResponse.errorInfo.error;
      toast.error(`${errorCode} - ${errorMessage}`);
      return true;
    }

    return false;
  }
  return false;
};
