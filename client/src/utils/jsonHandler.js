export const handleInputParams = (biller) => {
  const billerInputParams = biller.billerInputParams.paramInfo;
  let temp = [];
  if (Array.isArray(billerInputParams)) {
    return billerInputParams;
  } else {
    temp.push(billerInputParams);
    return temp;
  }
};

export const getInputParamsFromFormValues = (formValues) => {
  let inputParamsArray = [];
  Object.keys(formValues).forEach(function (key, idx) {
    let tempInputObject = {
      paramName: key,
      paramValue: formValues[key],
    };
    inputParamsArray.push(tempInputObject);
  });
  return inputParamsArray;
};

export const getInitialFormValueFromState = (state) => {
  const initialFormValues = {},
    retObject = {};
  const {
    selectedBiller,
    customerInfo,
    lastBillFetchRequestId,
  } = state.bbpsState;
  const { billFetchResponse } = state.bbpsState.billData;

  const { billerResponse, inputParams } = billFetchResponse;
  const { billerName } = selectedBiller;
  const { customerMobile } = customerInfo;

  const {
    billAmount,
    billDate,
    billNumber,
    billPeriod,
    customerName,
    dueDate,
  } = billerResponse;

  initialFormValues.billAmount = parseFloat(billAmount * 0.01);
  initialFormValues.amount = parseFloat(billAmount * 0.01);
  initialFormValues.billDate = billDate;
  initialFormValues.billNumber = billNumber;
  initialFormValues.billPeriod = billPeriod;
  initialFormValues.customerName = customerName;
  initialFormValues.dueDate = dueDate;
  initialFormValues.billerName = billerName;
  initialFormValues.customerMobile = customerMobile;
  initialFormValues.custConvFee = 0;

  let inputTemp = [];

  if (Array.isArray(inputParams.input)) {
    inputTemp = inputParams.input;
  } else {
    inputTemp.push(inputParams.input);
  }
  inputParams.input = inputTemp;

  inputParams.input.map((inputParam) => {
    initialFormValues[`${inputParam.paramName}`] = inputParam.paramValue;
  });

  retObject.inputParams = inputParams;
  retObject.initialFormValues = initialFormValues;
  retObject.billFetchResponse = billFetchResponse;
  retObject.lastBillFetchRequestId = lastBillFetchRequestId;

  return retObject;
};
