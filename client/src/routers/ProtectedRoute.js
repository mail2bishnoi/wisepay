import React from "react";
import { Route, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const nonKYCRoutes = [
  "/kyc",
  "/user/edit/self",
  "/user/changepassword",
  "/dashboard",
  "/bbps-log",
  "/bbps-admin-log",
];

const ProtectedRoute = ({
  layout: Layout,
  component: Component,
  isAuthenticated,
  isKycDone,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticated && isKycDone) {
          return (
            <Layout>
              <Component {...props} />
            </Layout>
          );
        } else if (
          isAuthenticated &&
          !isKycDone &&
          nonKYCRoutes.indexOf(props.match.path) !== -1
        ) {
          return (
            <Layout>
              <Component {...props} />
            </Layout>
          );
        } else if (
          isAuthenticated &&
          isKycDone === false &&
          props.match.path !== "/kyc"
        ) {
          return (
            <Redirect
              to={{ pathname: "/kyc", state: { from: props.location } }}
            />
          );
        } else if (isAuthenticated === false || !isAuthenticated) {
          return (
            <Redirect
              to={{ pathname: "/signin", state: { from: props.location } }}
            />
          );
        }
      }}
    />
  );
};

ProtectedRoute.propTypes = {
  isAuthenticated: PropTypes.bool,
};
ProtectedRoute.defaultProps = {
  isAuthenticated: false,
  isKycDone: false,
};

function mapStateToProps(state) {
  let isKycDone = false;
  if (state.authState.user) {
    isKycDone = state.authState.user.kyc;
  }
  return {
    isAuthenticated: state.authState.isAuthenticated,
    isKycDone: isKycDone,
  };
}
export default connect(mapStateToProps)(ProtectedRoute);
