import _ from "lodash";
import { GET_ACTION_HISTORY } from "../types";

const INITIAL_STATE = {
  actions: {},
  count: 0,
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_ACTION_HISTORY:
      return {
        ...state,
        actions: _.mapKeys(payload.actions, "_id"),
        count: payload.count,
      };
    default:
      return state;
  }
};
