import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { loadingBarReducer } from "react-redux-loading-bar";
import auth from "./auth";
import loading from "./loading";
import profile from "./profile";
import billers from "./billers";
import bbps from "./bbps";
import users from "./users";
import actions from "./actions";
import eco from "./eco";
export default combineReducers({
  loadingBar: loadingBarReducer,
  form: formReducer,
  authState: auth,
  profileState: profile,
  toggleLoading: loading,
  billersState: billers,
  bbpsState: bbps,
  userState: users,
  actionState: actions,
  ecoState: eco,
});
