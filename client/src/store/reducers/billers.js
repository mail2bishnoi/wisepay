import { LOAD_BILLERS } from "../types";
const billersData = require("data/billers.json");

const initialState = {
  billers: billersData
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOAD_BILLERS:
    default:
      return state;
  }
};
