import {
  ADD_BILL,
  ADD_TRANSACTION,
  SELECT_BILLER,
  CHECK_TRANSACTION_STATUS,
  COMPLAIN_REGISTRATION_STATUS,
  CHANGE_CUSTOMER_MOBILE,
  SELECT_BILLER_CATEGORY,
  GET_BBPS_LOGS,
} from "../types";
const initialState = {
  billData: null,
  selectedBiller: null,
  selectedBillerCategory: null,
  lastBillFetchRequestId: 0,
  transactionDetails: null,
  transactionStatus: null,
  complainRegistrationStatus: null,
  customerInfo: {
    customerMobile: null,
    customerPan: "BBQPB8074D",
  },
  bbpsLogs: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case ADD_BILL:
      return {
        ...state,
        billData: payload.jsonResponse,
        lastBillFetchRequestId: payload.requestId,
      };
    case ADD_TRANSACTION:
      return { ...state, transactionDetails: payload.jsonResponse };
    case SELECT_BILLER:
      return { ...state, selectedBiller: payload };
    case CHECK_TRANSACTION_STATUS:
      return { ...state, transactionStatus: payload };
    case COMPLAIN_REGISTRATION_STATUS:
      return { ...state, complainRegistrationStatus: payload };
    case CHANGE_CUSTOMER_MOBILE:
      return {
        ...state,
        customerInfo: { ...state.customerInfo, customerMobile: payload },
      };
    case SELECT_BILLER_CATEGORY:
      return { ...state, selectedBillerCategory: payload };
    case GET_BBPS_LOGS:
      return { ...state, bbpsLogs: payload };
    default:
      return state;
  }
};
