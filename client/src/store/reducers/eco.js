import _ from "lodash";
import { GET_ALL_ECO_SERVICES, GET_ALL_ACTIVATE_SERVICES } from "../types";

const INITIAL_STATE = {
  all_services: null,
  activate_services: {}
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_ALL_ECO_SERVICES:
      return { ...state, all_services: _.mapKeys(payload, "service_code") };
    case GET_ALL_ACTIVATE_SERVICES:
      return {
        ...state,
        activate_services: _.mapKeys(payload, "service_code")
      };
    default:
      return state;
  }
};
