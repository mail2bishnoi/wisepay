import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT
} from "../types";
//Todo => when hard refresh page, isAuthenticated,user, and refresh token will be null
const initialState = {
  token: localStorage.getItem("jwtToken"),
  isAuthenticated: null,
  user: null
};
export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case USER_LOADED:
      return { ...state, user: payload, isAuthenticated: true };
    case REGISTER_SUCCESS:
    case LOGIN_SUCCESS:
      localStorage.setItem("jwtToken", payload.token);
      return { ...state, ...payload, isAuthenticated: true };
    case REGISTER_FAIL:
    case LOGIN_FAIL:
    case AUTH_ERROR:
    case LOGOUT:
      localStorage.removeItem("jwtToken");
      return initialState;
    default:
      return state;
  }
};
