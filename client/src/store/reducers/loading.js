import { TOGGLE_LOADING } from "store/types";
const INITIAL_STATE = {
  toggleLoading: false
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TOGGLE_LOADING:
      return { ...state, toggleLoading: action.payload };
    default:
      return state;
  }
}
