import _ from "lodash";
import {
  GET_USERS,
  GET_USER,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER
} from "../types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USERS:
      return { ...state, ..._.mapKeys(payload, "_id") };
    case GET_USER:
      return { ...state, [payload._id]: payload };
    case ADD_USER:
      return { ...state, [payload._id]: payload };
    case UPDATE_USER:
      return { ...state, [payload._id]: payload };
    case DELETE_USER:
      return _.omit(state, payload);
    default:
      return state;
  }
};
