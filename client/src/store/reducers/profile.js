import {
  UPDATE_PASSWORD,
  UPDATE_PIN,
  UPDATE_EMAIL,
  UPDATE_PHONE,
  RESET_PASSWORD,
  VERIFY_PASSWORD,
  VERIFY_PIN,
  VERIFY_EMAIL,
  VERIFY_PHONE,
  VERIFY_IP
} from "../types";

import { getUser } from "utils/auth";

const initialState = {
  user: getUser()
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case UPDATE_PASSWORD:
    case UPDATE_PIN:
      return state;
    case UPDATE_EMAIL:
      return { ...state, user: payload };
    case UPDATE_PHONE:
      return { ...state, user: payload };
    case RESET_PASSWORD:
    case VERIFY_PASSWORD:
    case VERIFY_PIN:
    case VERIFY_EMAIL:
    case VERIFY_PHONE:
    case VERIFY_IP:
    default:
      return state;
  }
};
