import { api } from "api";
import { TOGGLE_LOADING } from "../types";
import { toast } from "react-toastify";
import { getErrorMessage } from "utils/error";
/*
export const updateUser = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("User update is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/update/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("User updated Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response.data);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};*/

export const updatePhone = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("Phone update is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/update/phone/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.info("Please enter otp to verify your phone");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const verifyPhone = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("OTP verification is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/verify/phone/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("OTP verified Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const updatePassword = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("Password update is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/update/password/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("Password updated Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const updateEmail = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("Email update is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  formValues.email = formValues.newemail;
  console.log(formValues);
  try {
    const response = await api.post("/users/update/email/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("Email updated Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const verifyEmail = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("Email verification is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/verify/email/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("Email verified Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const updatePin = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("Pin update is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/update/pin/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("Pin updated Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const verifyPin = (formValues, callback) => async dispatch => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  toast.info("Pin verification is in progress");
  const token = localStorage.getItem("access_token");
  const user = await JSON.parse(localStorage.getItem("user"));
  formValues.userName = user.userName;
  try {
    const response = await api.post("/users/verify/pin/", formValues, {
      params: {
        access_token: token
      }
    });
    toast.success("Pin verified Successfully");
    dispatch({ type: TOGGLE_LOADING, payload: false });
    callback(response);
  } catch (e) {
    const msg = getErrorMessage(e);
    toast.error(msg);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
