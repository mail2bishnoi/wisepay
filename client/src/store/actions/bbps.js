import { api } from "api";
import {
  ADD_BILL,
  ADD_TRANSACTION,
  TOGGLE_LOADING,
  SELECT_BILLER,
  CHANGE_CUSTOMER_MOBILE,
  CHECK_TRANSACTION_STATUS,
  COMPLAIN_REGISTRATION_STATUS,
  SELECT_BILLER_CATEGORY,
  GET_BBPS_LOGS,
  USER_LOADED,
} from "../types";

import { getErrorMessage, handleBbpsError } from "../../utils/error";

export const onSelectBiller = (biller) => ({
  type: SELECT_BILLER,
  payload: biller,
});

export const onSelectBillerCategory = (billerCategory) => ({
  type: SELECT_BILLER_CATEGORY,
  payload: billerCategory,
});

export const onCustomerMobileChange = (mobile) => ({
  type: CHANGE_CUSTOMER_MOBILE,
  payload: mobile,
});
export const mobilePostpaidBillFetch = (formProps, callback) => async (
  dispatch
) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const response = await api.post("billFetch", formProps);
    dispatch({ type: TOGGLE_LOADING, payload: false });
    const responseData = response.data;
    if (!handleBbpsError(responseData)) {
      dispatch({ type: ADD_BILL, payload: responseData });
      callback(responseData);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const mobilePostpaidBillPayment = (formProps, callback) => async (
  dispatch
) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const response = await api.post("billPay", formProps);
    const responseData = response.data;
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (!handleBbpsError(responseData)) {
      dispatch({ type: ADD_TRANSACTION, payload: responseData });
      dispatch({ type: USER_LOADED, payload: responseData.user });
      callback(responseData);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const quickBillPayment = (formProps, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const response = await api.post("quickBillPay", formProps);
    dispatch({ type: TOGGLE_LOADING, payload: false });
    console.log("quick bill payment -->> ", response);
    const responseData = response.data;
    if (!handleBbpsError(responseData)) {
      dispatch({ type: ADD_TRANSACTION, payload: responseData });
      callback(responseData);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const checkTransactionStatus = (formProps, callback) => async (
  dispatch
) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const response = await api.post("transactionStatus", formProps);
    const responseData = response.data;
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (!handleBbpsError(responseData)) {
      dispatch({ type: CHECK_TRANSACTION_STATUS, payload: responseData });
      callback(responseData);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const complainRegister = (formProps, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const response = await api.post(
      "transactionComplainRegistration",
      formProps
    );
    const responseData = response.data;
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (!handleBbpsError(responseData)) {
      dispatch({ type: COMPLAIN_REGISTRATION_STATUS, payload: responseData });
      callback(responseData);
    }
  } catch (error) {
    dispatch({ type: TOGGLE_LOADING, payload: false });
    getErrorMessage(error);
  }
};

export const checkComplainStatus = (formProps, callback) => async (
  dispatch
) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const response = await api.post("complainTracking", formProps);
    const responseData = response.data;
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (!handleBbpsError(responseData)) {
      dispatch({ type: COMPLAIN_REGISTRATION_STATUS, payload: responseData });
      callback(responseData);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const getBBPSLog = () => async (dispatch) => {
  try {
    const response = await api.get("getBBPSLog");
    const responseData = response.data;
    dispatch({ type: GET_BBPS_LOGS, payload: responseData.jsonResponse });
  } catch (error) {
    getErrorMessage(error);
  }
};
export const getBBPSLogByAgentId = (agentId) => async (dispatch) => {
  try {
    const response = await api.get(`getBBPSLogByAgentId/${agentId}`);
    const responseData = response.data;
    dispatch({ type: GET_BBPS_LOGS, payload: responseData.jsonResponse });
  } catch (error) {
    getErrorMessage(error);
  }
};
