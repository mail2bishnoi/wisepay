import { api } from "api";

import {
  GET_USERS,
  UPDATE_USER,
  TOGGLE_LOADING,
  GET_ALL_ECO_SERVICES,
  GET_ALL_ACTIVATE_SERVICES,
  GET_ACTION_HISTORY,
} from "../types";

import { getErrorMessage } from "utils/error";
import { toast } from "react-toastify";

export const getUsers = () => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const token = localStorage.getItem("jwtToken");
    const response = await api.get("/users", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    const responseData = response.data;
    if (response.status === 200) {
      dispatch({ type: GET_USERS, payload: responseData });
    }
  } catch (error) {
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const addCredit = (formValues, id, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const data = JSON.stringify(formValues);
  try {
    const response = await api.post(`/users/credit/${id}`, data);
    dispatch({ type: TOGGLE_LOADING, payload: false });
    console.log(response);
    if (response.status === 200) {
      callback({ redirect: true });
    }
  } catch (e) {
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const updateUserById = (id, formValues, callback) => async (
  dispatch
) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const token = localStorage.getItem("jwtToken");
  const data = JSON.stringify(formValues);
  try {
    const response = await api.patch(`/users/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    const responseData = response.data;
    if (response.status === 200) {
      dispatch({ type: UPDATE_USER, payload: responseData });
      callback();
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const getAllEcoServices = () => async (dispatch) => {
  try {
    const token = localStorage.getItem("jwtToken");
    const response = await api.get("/eco/user/services", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const responseData = response.data;
    if (response.status === 200) {
      dispatch({
        type: GET_ALL_ECO_SERVICES,
        payload: responseData.jsonResponse,
      });
    }
  } catch (e) {
    getErrorMessage(e);
  }
};

export const getAllActivateServices = (user_code) => async (dispatch) => {
  try {
    const token = localStorage.getItem("jwtToken");
    const response = await api.get(
      `/eco/user/service/activated/user_code:${user_code}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const responseData = response.data;
    console.log("Activate service response is =>", responseData);
    if (response.status === 200) {
      dispatch({
        type: GET_ALL_ACTIVATE_SERVICES,
        payload: responseData.jsonResponse,
      });
    }
  } catch (e) {
    getErrorMessage(e);
  }
};

export const onboardEcoUser = (id, formValues, callback) => async (
  dispatch
) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const token = localStorage.getItem("jwtToken");
  try {
    const response = await api.post("/eco/user/onboard", formValues, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (response.status === 200) {
      //dispatch({ type: UPDATE_USER, payload: responseData }); No need of this because oncallback list user will fetch updated user
      callback();
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const ecoAePSActivate = (formValues) => async (dispatch) => {
  const data = new FormData();
  data.append("pan_card", formValues.pan_card);
  data.append("aadhar_front", formValues.aadhar_front);
  data.append("aadhar_back", formValues.aadhar_back);

  data.append("service_code", formValues.service_code);
  data.append("user_code", formValues.user_code);
  data.append("initiator_id", formValues.initiator_id);
  data.append("device_number", formValues.device_number);
  data.append("modelname", formValues.modelname);
  data.append("line", formValues.line);
  data.append("city", formValues.city);
  data.append("state", formValues.state);
  data.append("pincode", formValues.pincode);

  const token = localStorage.getItem("jwtToken");
  try {
    const response = await api.post("eco/user/service/activate/aeps", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    console.log(response.data);
    if (response.status === 200) {
      const { status, message } = response.data;
      //dispatch({ type: UPDATE_USER, payload: responseData });
      toast.info(`${status} ${message}`);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const ecoServiceActivate = (formValues) => async (dispatch) => {
  const token = localStorage.getItem("jwtToken");
  try {
    const response = await api.post("eco/user/service/activate", formValues, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    console.log(response.data);
    if (response.status === 200) {
      toast.info(`${response.data.jsonResponse.message}`);
    }
  } catch (error) {
    getErrorMessage(error);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const getEcoSecrets = (cb) => async (dispatch) => {
  try {
    const response = await api.get("/eco/getecosecrets");
    cb(response.data.jsonResponse);
  } catch (error) {
    console.log("get eco secrets error ", error);
  }
};

export const getActionHistory = (page, limit) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const token = localStorage.getItem("jwtToken");
    const response = await api.get(`/actions?page=${page}&limit=${limit}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    const responseData = response.data;
    if (response.status === 200) {
      dispatch({ type: GET_ACTION_HISTORY, payload: responseData });
    }
  } catch (error) {
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
