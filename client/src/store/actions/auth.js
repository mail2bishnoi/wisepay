import { api } from "api";
import {
  REGISTER_SUCCESS,
  USER_LOADED,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  TOGGLE_LOADING,
} from "../types";
import { setUser, removeUser, isLoggedIn } from "utils/auth";
import { setAuthHeaders } from "utils/setAuthHeaders";
import { getErrorMessage } from "utils/error";
import { toast } from "react-toastify";

export const signup = (formProps, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const data = JSON.stringify(formProps);
  try {
    const response = await api.post("/signup", data);
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (response.status === 201) {
      const { user } = response.data;
      user && setUser(user);
      dispatch({ type: REGISTER_SUCCESS, payload: response.data });
      callback({ redirect: true });
    }
  } catch (e) {
    getErrorMessage(e);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const signin = (formProps, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  try {
    const data = JSON.stringify(formProps);
    const response = await api.post("/signin", data);
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (response.status === 200) {
      const { user } = response.data;
      user && setUser(user);
      dispatch({ type: LOGIN_SUCCESS, payload: response.data });
      callback({ redirect: true });
    }
  } catch (e) {
    getErrorMessage(e);
    dispatch({ type: TOGGLE_LOADING, payload: false });
    dispatch({ type: LOGIN_FAIL });
  }
};

export const signout = (callback) => async (dispatch) => {
  try {
    const token = localStorage.getItem("jwtToken");
    const response = await api.post(
      "/signout",
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.status === 200) {
      sessionStorage.removeItem("lastScreen");
      removeUser();
      dispatch({ type: LOGOUT });
      callback({ redirect: true });
    }
  } catch (e) {
    getErrorMessage(e);
  }
};
export const updateUser = (formValues) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const token = localStorage.getItem("jwtToken");
  const data = JSON.stringify(formValues);
  try {
    const response = await api.post("/users/me", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (response.status === 200) {
      const user = response.data;
      user && setUser(user);
      dispatch({ type: USER_LOADED, payload: user });
    }
  } catch (e) {
    getErrorMessage(e);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const changePassword = (formValues, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const token = localStorage.getItem("jwtToken");
  const data = JSON.stringify(formValues);
  try {
    const response = await api.post("/users/changepassword", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (response.status === 200) {
      removeUser();
      dispatch({ type: LOGOUT });
      callback({ redirect: true });
    }
  } catch (e) {
    getErrorMessage(e);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};
export const resetPassword = (formValues, callback) => async (dispatch) => {
  dispatch({ type: TOGGLE_LOADING, payload: true });
  const token = localStorage.getItem("jwtToken");
  const data = JSON.stringify(formValues);
  try {
    const response = await api.post("/users/resetpassword", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch({ type: TOGGLE_LOADING, payload: false });
    if (response.status === 200) {
      toast.success(response.data.message);
      callback({ redirect: true });
    }
  } catch (e) {
    getErrorMessage(e);
    dispatch({ type: TOGGLE_LOADING, payload: false });
  }
};

export const loadUser = () => async (dispatch) => {
  if (!isLoggedIn()) return;
  try {
    const response = await api.get("/users/me", {
      headers: setAuthHeaders(),
    });
    const responseData = response.data;
    if (response.status === 200) {
      const { user } = responseData;
      user && setUser(user);
      dispatch({ type: USER_LOADED, payload: responseData });
    }
  } catch (e) {
    getErrorMessage(e);
  }
};
