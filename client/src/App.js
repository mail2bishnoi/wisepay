import 'react-perfect-scrollbar/dist/css/styles.css';
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, CssBaseline } from "@material-ui/core";

//Redux
import { Provider } from "react-redux";
import store from "./store";
import { loadUser, getAllEcoServices } from "./store/actions";

import "./assets/scss/index.scss";

import theme from "./theme";
import Routes from "./Routes";

import LoadingBar from "react-redux-loading-bar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const useStyles = makeStyles(theme => ({
  root: {}
}));

const App = props => {
  const classes = useStyles();
  React.useEffect(() => {
    store.dispatch(loadUser());
    store.dispatch(getAllEcoServices());
  });
  return (
    <div className={classes.root}>
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <LoadingBar style={{ zIndex: 9999 }} />
          <ToastContainer autoClose={10000} />
          <CssBaseline />
          <Routes />
        </MuiThemeProvider>
      </Provider>
    </div>
  );
};

export default App;
