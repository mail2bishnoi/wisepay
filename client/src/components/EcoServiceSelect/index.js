import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: "100%"
  }
}));

const EcoServiceSelect = props => {
  console.log(props);
  const classes = useStyles();
  const [data] = useState(props.data);
  const [selectedData, updateSelectedData] = useState("");

  const handleChange = event => {
    updateSelectedData(event.target.value);
    if (props.input.onChange) props.input.onChange(event.target.value);
  };

  let options = data.map(data => (
    <MenuItem key={data.service_code} value={data.service_code}>
      {data.service_name}
    </MenuItem>
  ));

  return (
    <FormControl variant="filled" className={classes.formControl}>
      <InputLabel id="demo-simple-select-filled-label">
        Select Service
      </InputLabel>
      <Select
        labelId="demo-simple-select-filled-label"
        id="ecoserviceselect"
        value={selectedData}
        onChange={handleChange}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {options}
      </Select>
    </FormControl>
  );
};

export default EcoServiceSelect;
