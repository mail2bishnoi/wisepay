import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  option: {
    fontSize: 14
  }
});

const DispositionSelect = props => {
  const classes = useStyles();
  const {
    billers,
    input: { onBlur, ...inputProps }
  } = props;
  const [value, setValue] = React.useState(null);

  const onChange = (event, newValue) => {
    setValue(newValue);
    if (newValue) inputProps.onChange(newValue);
  };
  return (
    <Autocomplete
      id="disposition-select"
      options={DispositionTypes}
      value={value}
      onChange={onChange}
      classes={{
        option: classes.option
      }}
      autoHighlight
      getOptionLabel={option => option.dispositionType}
      renderOption={option => (
        <React.Fragment>{option.dispositionType}</React.Fragment>
      )}
      renderInput={params => (
        <TextField
          {...params}
          label="Choose Disposition Type"
          variant="filled"
          size="small"
          margin="normal"
          inputProps={{
            ...params.inputProps,
            autoComplete: "off" // disable autocomplete and autofill
          }}
        />
      )}
    />
  );
};

export default DispositionSelect;

const DispositionTypes = [
  { dispositionType: "Transaction Successful, account not updated" },
  {
    dispositionType:
      "Amount deducted, biller account credited but transaction ID not received"
  },
  {
    dispositionType:
      "Amount deducted, biller account not credited & transaction ID not received"
  },
  { dispositionType: "Amount deducted multiple times" },
  { dispositionType: "Double payment updated" },
  { dispositionType: "Erroneously paid in wrong account" },
  { dispositionType: "Others, provide details in description" }
];
