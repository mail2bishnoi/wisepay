import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  headingSecondary: {
    textTransform: "uppercase",
    display: "inline-block",
    backgroundImage: `linear-gradient(to right, ${theme.palette.primary.main}, ${theme.palette.secondary.main})`,
    "-webkitBackgroundClip": "text",
    color: "transparent",
    marginBottom: "64"
  }
}));

const H2 = props => {
  const classes = useStyles();
  return <h2 className={classes.headingSecondary}>{props.children}</h2>;
};

export default H2;
