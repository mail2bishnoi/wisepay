import React from "react";
import TextField from "@material-ui/core/TextField";

export const renderInput = ({ input, label, type, id, meta, ...rest }) => {
  const showError = meta.error && meta.touched ? true : false;
  return (
    <TextField
      {...input}
      type={type}
      label={label}
      autoComplete={rest.autoComplete}
      required={rest.required}
      variant="filled"
      className={rest.className}
      size="small"
      margin="dense"
      error={showError}
      helperText={showError ? meta.error : rest.helperText}
      autoFocus={rest.autoFocus}
      fullWidth={rest.fullWidth}
      defaultValue={rest.defaultValue}
      disabled={rest.disabled}
      multiline={rest.multiline}
      rows={rest.rows}
    />
  );
};

//Todo - https://blog.bitsrc.io/build-awesome-forms-in-react-using-redux-form-d1e4c96f5850
