// https://codesandbox.io/s/m58q8l054x  --> may be helpful in future
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1)
    }
  },
  input: {
    display: "none"
  }
}));
const FileUpload = props => {
  const classes = useStyles();
  const { file, label, name, id } = props;

  const handleChange = event => {};
  return (
    <div className={classes.root}>
      <input
        accept="image/*"
        className={classes.input}
        id={`"${id}"`}
        name={name}
        type="file"
        onChange={handleChange}
        multiple
      />
      <label htmlFor={`"${id}"`}>
        <Button variant="outlined" color="primary" component="span">
          {label}
        </Button>
      </label>
      <span>{file ? file.name : "Select Image"}</span>
    </div>
  );
};

export default FileUpload;
