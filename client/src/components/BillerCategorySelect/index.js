import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  option: {
    fontSize: 14
  }
});

const BillerCategorySelect = props => {
  const classes = useStyles();
  const {
    billers,
    input: { onBlur, ...inputProps }
  } = props;
  const [value, setValue] = React.useState(null);

  const onChange = (event, newValue) => {
    setValue(newValue);
    if (newValue) inputProps.onChange(newValue);
  };
  return (
    <Autocomplete
      id="biller-category-select"
      options={billerCategories}
      value={value}
      onChange={onChange}
      classes={{
        option: classes.option
      }}
      autoHighlight
      getOptionLabel={option => option.billerCategory}
      renderOption={option => (
        <React.Fragment>{option.billerCategory}</React.Fragment>
      )}
      renderInput={params => (
        <TextField
          {...params}
          label="Choose Biller Category"
          variant="filled"
          size="small"
          margin="normal"
          inputProps={{
            ...params.inputProps,
            autoComplete: "off" // disable autocomplete and autofill
          }}
        />
      )}
    />
  );
};

export default BillerCategorySelect;

const billerCategories = [
  { billerCategory: "Electricity" },
  { billerCategory: "Water" },
  { billerCategory: "Broadband Postpaid" },
  { billerCategory: "Gas" },
  { billerCategory: "Loan Repayment" },
  { billerCategory: "Mobile Postpaid" },
  { billerCategory: "Landline Postpaid" },
  { billerCategory: "DTH" },
  { billerCategory: "Fastag" },
  { billerCategory: "Life Insurance" },
  { billerCategory: "LPG Gas" },
  { billerCategory: "Cable TV" },
  { billerCategory: "Health Insurance" },
  { billerCategory: "Housing Society" },
  { billerCategory: "Insurance" },
  { billerCategory: "Municipal Taxes" },
  { billerCategory: "Education Fees" }
];
