import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "row-reverse",
    "& > *": {
      margin: theme.spacing(1)
    }
  },
  image: {
    width: theme.spacing(12),
    height: theme.spacing(9)
  }
}));

const BbpsLogo = props => {
  const logo = props.logo;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img
        alt="Product"
        className={classes.image}
        src={`/static/images/${logo}.png`}
      />
    </div>
  );
};

export default BbpsLogo;
