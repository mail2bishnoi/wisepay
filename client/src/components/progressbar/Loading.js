import React from "react";
import { CircularProgress } from "@material-ui/core";

export default function () {
  return <CircularProgress color="inherit" size={16} />;
}
