import React from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

const ComplainTypeRadio = props => {
  const [value, setValue] = React.useState("Transaction");
  const handleChange = event => {
    setValue(event.target.value);
  };
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Complaint Type</FormLabel>
      <RadioGroup
        aria-label="complaintType"
        name="complaintType"
        value={value}
        onChange={handleChange}
        row
      >
        <FormControlLabel
          value="Transaction"
          control={<Radio />}
          label="Transaction"
        />
      </RadioGroup>
    </FormControl>
  );
};

export default ComplainTypeRadio;
