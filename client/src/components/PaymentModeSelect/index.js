import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: "100%"
  }
}));

const PaymentModeSelect = props => {
  const classes = useStyles();
  const {
    input: { onBlur, ...inputProps }
  } = props;

  const handleChange = event => {
    setValue(event.target.value);
    inputProps.onChange(event.target.value);
  };
  const [value, setValue] = React.useState("");
  return (
    <FormControl variant="filled" className={classes.formControl}>
      <InputLabel id="demo-simple-select-filled-label">Payment Mode</InputLabel>
      <Select
        labelId="demo-simple-select-filled-label"
        id="paymentmodeselect"
        value={value}
        onChange={handleChange}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        <MenuItem value={`Cash`}>Cash</MenuItem>
      </Select>
    </FormControl>
  );
};

export default PaymentModeSelect;
