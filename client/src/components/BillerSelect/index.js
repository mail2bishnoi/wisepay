import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  option: {
    fontSize: 14
  }
});

const BillerSelect = props => {
  const classes = useStyles();
  const {
    billers,
    input: { onBlur, ...inputProps }
  } = props;
  const [value, setValue] = React.useState(null);

  const onChange = (event, newValue) => {
    setValue(newValue);
    if (newValue) inputProps.onChange(newValue);
  };
  return (
    <Autocomplete
      id="biller-select"
      options={billers}
      value={value}
      onChange={onChange}
      classes={{
        option: classes.option
      }}
      autoHighlight
      getOptionLabel={option => option.billerName}
      renderOption={option => (
        <React.Fragment>{option.billerName}</React.Fragment>
      )}
      renderInput={params => (
        <TextField
          {...params}
          label="Choose a Biller"
          variant="filled"
          size="small"
          margin="normal"
          inputProps={{
            ...params.inputProps,
            autoComplete: "off" // disable autocomplete and autofill
          }}
        />
      )}
    />
  );
};

export default BillerSelect;
