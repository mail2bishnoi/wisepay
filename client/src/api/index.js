//https://daveceddia.com/multiple-environments-with-react/

import axios from "axios";

import { showLoading, hideLoading } from "react-redux-loading-bar";
import store from "./../store";
import { getErrorMessage } from "utils/error";

let baseURL;
const hostname = window && window.location && window.location.hostname;

if (hostname === "localhost") {
  baseURL = "http://localhost:8000/api";
} else if (hostname === "wisepayapp.com") {
  baseURL = "https://wisepayapp.com/api";
} else {
  baseURL = "https://wisepay.app/api";
}

const api = axios.create({
  baseURL: baseURL,
  headers: {
    "Content-Type": "application/json",
  },
});

api.interceptors.request.use((req) => {
  const token = localStorage.getItem("jwtToken");
  store.dispatch(showLoading());
  req.headers.Authorization = `Bearer ${token}`; //further improvement
  // Important: request interceptors **must** return the request.
  return req;
});
api.interceptors.response.use(
  (res) => {
    store.dispatch(hideLoading());
    return res;
  },
  (err) => {
    getErrorMessage(err);
    store.dispatch(hideLoading());
    return err;
  }
);
const signinapi = axios.create({
  baseURL: "https://test.exchangerpoint.com/oauth/token",
  headers: {
    "Content-Type": "application/json",
  },
  auth: {
    username: "web_truested_cleint",
    password: "33b18e054ee604fa383f46ec6e28ea1d",
  },
});

export { api, signinapi };
