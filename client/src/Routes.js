import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { ProtectedRoute, AdmindRoute } from "./routers";
import { AdminLayout, SuperAdminLayout } from "./layouts";

import DashBoard from "./pages/Dashboard";
import Signin from "./pages/auth/Signin";
import Signup from "./pages/auth/Signup";
import Kyc from "./pages/auth/Kyc";

import Home from "./pages/public/Home";

//bbps bill payment import
import ElectricityBillPayment from "./pages/bbps-bill-payment/ElectricityBillPayment";
import DthRecharge from "./pages/bbps-bill-payment/DthRecharge";
import DataCardRecharge from "./pages/bbps-bill-payment/DataCardRecharge";
import BroadbandBillPayment from "./pages/bbps-bill-payment/BroadbandBillPayment";
import LandlineBillPayment from "./pages/bbps-bill-payment/LandlineBillPayment";
import GasBillPayment from "./pages/bbps-bill-payment/GasBillPayment";
import WaterBillPayment from "./pages/bbps-bill-payment/WaterBillPayment";
import InsurancePayment from "./pages/bbps-bill-payment/InsurancePayment";
import EducationFeePayment from "./pages/bbps-bill-payment/EducationFeePayment";
import MobileBillPayment from "./pages/bbps-bill-payment/MobileBillPayment";
import ShowBillDetails from "./pages/bbps-bill-payment/ShowBillDetails";
import ShowTransactionDetails from "./pages/bbps-bill-payment/ShowTransactionDetails";
import QuickBillPayment from "./pages/bbps-bill-payment/QuickBillPayment";
import CheckTransactionStatus from "./pages/transaction/CheckTransactionStatus";
import ComplainRegister from "./pages/complain/ComplainRegister";
import CheckComplainStatus from "./pages/complain/CheckComplainStatus";
import BBPSLog from "./pages/bbps-logs/BBPSLog";
import BBPSAdminLog from "./pages/bbps-logs/BBPSAdminLog";

import UserListView from "./pages/users/UserListView";
import UserEdit from "./pages/users/UserEdit";
import UserCredit from "./pages/users/UserCredit";
import EcoUserOnboard from "./pages/users/EcoUserOnboard";
import EcoServiceList from "./pages/users/EcoServiceList";
import EcoServiceActivate from "./pages/users/EcoServiceActivate";
import EcoAePSActivate from "./pages/users/EcoAePSActivate";
import UserEditSelf from "./pages/users/UserEditSelf";
import ChangePassword from "./pages/users/ChangePassword";
import ResetPassword from "./pages/users/ResetPassword";
import ActionHistoryList from "./pages/users/ActionHistoryList";
import AepsGateway from "./pages/aeps/AepsGateway";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/signin" component={Signin} />
        <Route exact path="/signup" component={Signup} />
        <AdmindRoute
          exact
          path="/users"
          layout={SuperAdminLayout}
          component={UserListView}
        />
        <AdmindRoute
          exact
          path="/users/edit/:id"
          layout={SuperAdminLayout}
          component={UserEdit}
        />
        <AdmindRoute
          exact
          path="/users/credit/:id"
          layout={SuperAdminLayout}
          component={UserCredit}
        />
        <AdmindRoute
          exact
          path="/users/resetpassword/:email"
          layout={SuperAdminLayout}
          component={ResetPassword}
        />
        <AdmindRoute
          exact
          path="/users/eco/onboard/:id"
          layout={SuperAdminLayout}
          component={EcoUserOnboard}
        />
        <AdmindRoute
          exact
          path="/eco/user/services/activated/:user_code"
          layout={SuperAdminLayout}
          component={EcoServiceList}
        />
        <AdmindRoute
          exact
          path="/eco/user/service/activate/:user_code"
          layout={SuperAdminLayout}
          component={EcoServiceActivate}
        />
        <AdmindRoute
          exact
          path="/eco/user/aeps/activate/:user_code"
          layout={SuperAdminLayout}
          component={EcoAePSActivate}
        />
        <ProtectedRoute
          exact
          path="/dashboard"
          layout={AdminLayout}
          component={DashBoard}
        />
        <ProtectedRoute
          exact
          path="/kyc"
          layout={AdminLayout}
          component={Kyc}
        />
        <ProtectedRoute
          exact
          path="/user/edit/self"
          layout={AdminLayout}
          component={UserEditSelf}
        />
        <ProtectedRoute
          exact
          path="/user/changepassword"
          layout={AdminLayout}
          component={ChangePassword}
        />
        <ProtectedRoute
          exact
          path="/user/actionhistory"
          layout={AdminLayout}
          component={ActionHistoryList}
        />
        <ProtectedRoute
          exact
          path="/electricity-bill-payment"
          layout={AdminLayout}
          component={ElectricityBillPayment}
        />
        <ProtectedRoute
          exact
          path="/dth-recharge"
          layout={AdminLayout}
          component={DthRecharge}
        />
        <ProtectedRoute
          exact
          path="/datacard-recharge"
          layout={AdminLayout}
          component={DataCardRecharge}
        />
        <ProtectedRoute
          exact
          path="/broadband-bill-payment"
          layout={AdminLayout}
          component={BroadbandBillPayment}
        />
        <ProtectedRoute
          exact
          path="/landline-bill-payment"
          layout={AdminLayout}
          component={LandlineBillPayment}
        />
        <ProtectedRoute
          exact
          path="/gas-bill-payment"
          layout={AdminLayout}
          component={GasBillPayment}
        />
        <ProtectedRoute
          exact
          path="/water-bill-payment"
          layout={AdminLayout}
          component={WaterBillPayment}
        />
        <ProtectedRoute
          exact
          path="/insurance-payment"
          layout={AdminLayout}
          component={InsurancePayment}
        />
        <ProtectedRoute
          exact
          path="/education-fee-payment"
          layout={AdminLayout}
          component={EducationFeePayment}
        />
        <ProtectedRoute
          exact
          path="/mobile-postpaid-payment"
          layout={AdminLayout}
          component={MobileBillPayment}
        />
        <ProtectedRoute
          exact
          path="/show-bill-details"
          layout={AdminLayout}
          component={ShowBillDetails}
        />
        <ProtectedRoute
          exact
          path="/show-transaction-details"
          layout={AdminLayout}
          component={ShowTransactionDetails}
        />
        <ProtectedRoute
          exact
          path="/check-transaction-status"
          layout={AdminLayout}
          component={CheckTransactionStatus}
        />
        <ProtectedRoute
          exact
          path="/complain-register"
          layout={AdminLayout}
          component={ComplainRegister}
        />
        <ProtectedRoute
          exact
          path="/check-complain-status"
          layout={AdminLayout}
          component={CheckComplainStatus}
        />
        <ProtectedRoute
          exact
          path="/quick-bill-payment"
          layout={AdminLayout}
          component={QuickBillPayment}
        />
        <ProtectedRoute
          exact
          path="/bbps-log"
          layout={AdminLayout}
          component={BBPSLog}
        />
        <ProtectedRoute
          exact
          path="/bbps-admin-log/:agentid"
          layout={AdminLayout}
          component={BBPSAdminLog}
        />
        <ProtectedRoute
          exact
          path="/aeps-gateway"
          layout={AdminLayout}
          component={AepsGateway}
        />
      </Switch>
    </Router>
  );
};

export default Routes;
