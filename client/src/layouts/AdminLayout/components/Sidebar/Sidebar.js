import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import { Link } from "react-router-dom";

import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  links: {
    textDecoration: "none",
    color: "inherit",
  },
  nested: {
    paddingLeft: theme.spacing(8),
  },
  subnested: {
    paddingLeft: theme.spacing(10),
  },
}));

const Sidebar = (props) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [subOpen, setSubOpen] = React.useState(false);
  const [openProfile, setOpenProfile] = React.useState(false);
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  //Todo - when user navigate to nested link directly from browser url, then handle opening nested itm and select it

  React.useState(() => {
    const path = props.children.props.match.path;
    if (path === "/electricity-bill-payment") {
      setSelectedIndex(100);
      setOpen(true);
    } else if (path === "/dth-recharge") {
      setSelectedIndex(101);
      setOpen(true);
    } else if (path === "/fastag-recharge") {
      setSelectedIndex(102);
      setOpen(true);
    } else if (path === "/landline-bill-payment") {
      setSelectedIndex(103);
      setOpen(true);
    } else if (path === "/broadband-bill-payment") {
      setSelectedIndex(104);
      setOpen(true);
    } else if (path === "/gas-bill-payment") {
      setSelectedIndex(105);
    } else if (path === "/loan-payment") {
      setSelectedIndex(106);
      setOpen(true);
    } else if (path === "/water-bill-payment") {
      setSelectedIndex(107);
      setOpen(true);
    } else if (path === "/health-insurance-payment") {
      setSelectedIndex(109);
      setOpen(true);
    } else if (path === "/life-insurance-payment") {
      setSelectedIndex(110);
      setOpen(true);
    } else if (path === "/education-fee-payment") {
      setSelectedIndex(111);
      setOpen(true);
    } else if (path === "/mobile-postpaid-payment") {
      setSelectedIndex(112);
      setOpen(true);
    } else if (path === "/lpg-gas-payment") {
      setSelectedIndex(113);
      setOpen(true);
    } else if (path === "/cable-tv-payment") {
      setSelectedIndex(114);
      setOpen(true);
    } else if (path === "/housing-society-payment") {
      setSelectedIndex(115);
      setOpen(true);
    } else if (path === "/municipal-taxes-payment") {
      setSelectedIndex(116);
      setOpen(true);
    } else if (path === "/quick-bill-payment") {
      setSelectedIndex(200);
      setOpen(false);
    } else if (path === "/check-transaction-status") {
      setSelectedIndex(201);
      setOpen(false);
    } else if (path === "/complain-register") {
      setSelectedIndex(202);
      setOpen(false);
    } else if (path === "/check-complain-status") {
      setSelectedIndex(203);
      setOpen(false);
    } else if (path === "/user/edit/self") {
      setSelectedIndex(50);
      setOpen(false);
      setOpenProfile(true);
    } else if (path === "/user/changepassword") {
      setSelectedIndex(49);
      setOpen(false);
      setOpenProfile(true);
    } else if (path === "/user/actionhistory") {
      setSelectedIndex(48);
      setOpen(false);
      setOpenProfile(true);
    } else if (path === "/bbps-log") {
      setSelectedIndex(47);
      setOpen(false);
      setOpenProfile(true);
    } else if (path === "/dashboard") {
      setSelectedIndex(0);
      setOpen(false);
      setOpenProfile(false);
    } else if (path === "/aeps-gateway") {
      setSelectedIndex(2);
      setOpen(false);
      setOpenProfile(false);
    }
  });
  const handleClick = () => {
    setOpen(!open);
  };
  const handleSubClick = () => {
    setSubOpen(!subOpen);
  };

  const handleOpenProfileClick = () => {
    setOpenProfile(!openProfile);
  };
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    if (index >= 200) {
      setOpen(false);
    }
  };
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
      dense
    >
      <ListItem button onClick={handleOpenProfileClick}>
        <ListItemText primary="My Account" />
        {openProfile ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={openProfile} timeout="auto" unmountOnExit>
        <List component="div" dense>
          <ListItem
            button
            selected={selectedIndex === 50}
            onClick={(event) => handleListItemClick(event, 50)}
            className={classes.nested}
          >
            <Link to="/user/edit/self" className={classes.links}>
              <ListItemText primary="My Profile" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 49}
            onClick={(event) => handleListItemClick(event, 49)}
            className={classes.nested}
          >
            <Link to="/user/changepassword" className={classes.links}>
              <ListItemText primary="Change Password" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 48}
            onClick={(event) => handleListItemClick(event, 48)}
            className={classes.nested}
          >
            <Link to="/user/actionhistory" className={classes.links}>
              <ListItemText primary="Action History" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 47}
            onClick={(event) => handleListItemClick(event, 47)}
            className={classes.nested}
          >
            <Link to="/bbps-log" className={classes.links}>
              <ListItemText primary="BBPS Logs" />
            </Link>
          </ListItem>
        </List>
      </Collapse>
      <ListItem
        button
        selected={selectedIndex === 0}
        onClick={(event) => handleListItemClick(event, 0)}
      >
        <Link to="/dashboard" className={classes.links}>
          <ListItemText primary="Dashboard" />
        </Link>
      </ListItem>
      <ListItem
        button
        selected={selectedIndex === 2}
        onClick={(event) => handleListItemClick(event, 2)}
      >
        <Link to="/aeps-gateway" className={classes.links}>
          <ListItemText primary="AEPS" />
        </Link>
      </ListItem>
      <ListItem button onClick={handleClick}>
        <ListItemText primary="Bill Payment" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" dense>
          <ListItem
            button
            selected={selectedIndex === 100}
            onClick={(event) => handleListItemClick(event, 100)}
            className={classes.nested}
          >
            <Link to="/electricity-bill-payment" className={classes.links}>
              <ListItemText primary="Electricity" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 101}
            onClick={(event) => handleListItemClick(event, 101)}
            className={classes.nested}
            disabled
          >
            <Link to="/dth-recharge" className={classes.links}>
              <ListItemText primary="DTH" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 102}
            onClick={(event) => handleListItemClick(event, 102)}
            className={classes.nested}
            disabled
          >
            <Link to="/fastag-recharge" className={classes.links}>
              <ListItemText primary="Fastag" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 103}
            onClick={(event) => handleListItemClick(event, 103)}
            className={classes.nested}
            disabled
          >
            <Link to="/landline-bill-payment" className={classes.links}>
              <ListItemText primary="Landline Postpaid" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 104}
            onClick={(event) => handleListItemClick(event, 104)}
            className={classes.nested}
            disabled
          >
            <Link to="/broadband-bill-payment" className={classes.links}>
              <ListItemText primary="Broadband Postpaid" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 105}
            onClick={(event) => handleListItemClick(event, 105)}
            className={classes.nested}
            disabled
          >
            <Link to="/gas-bill-payment" className={classes.links}>
              <ListItemText primary="Gas" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 106}
            onClick={(event) => handleListItemClick(event, 106)}
            className={classes.nested}
            disabled
          >
            <Link to="/loan-payment" className={classes.links}>
              <ListItemText primary="Loan Repayment" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 107}
            onClick={(event) => handleListItemClick(event, 107)}
            className={classes.nested}
            disabled
          >
            <Link to="/water-bill-payment" className={classes.links}>
              <ListItemText primary="Water" />
            </Link>
          </ListItem>
          <ListItem button onClick={handleSubClick} className={classes.nested}>
            <ListItemText primary="Insurance" />
            {subOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={subOpen} timeout="auto" unmountOnExit>
            <List component="div" dense>
              <ListItem
                button
                selected={selectedIndex === 109}
                onClick={(event) => handleListItemClick(event, 109)}
                className={classes.subnested}
                disabled
              >
                <Link to="/health-insurance-payment" className={classes.links}>
                  <ListItemText primary="Health Insurance" />
                </Link>
              </ListItem>
              <ListItem
                button
                selected={selectedIndex === 110}
                onClick={(event) => handleListItemClick(event, 110)}
                className={classes.subnested}
                disabled
              >
                <Link to="/life-insurance-payment" className={classes.links}>
                  <ListItemText primary="Life Insurance" />
                </Link>
              </ListItem>
            </List>
          </Collapse>
          <ListItem
            button
            selected={selectedIndex === 111}
            onClick={(event) => handleListItemClick(event, 111)}
            className={classes.nested}
            disabled
          >
            <Link to="/education-fee-payment" className={classes.links}>
              <ListItemText primary="Education Fees" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 112}
            onClick={(event) => handleListItemClick(event, 112)}
            className={classes.nested}
          >
            <Link to="/mobile-postpaid-payment" className={classes.links}>
              <ListItemText primary="Mobile Postpaid" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 113}
            onClick={(event) => handleListItemClick(event, 113)}
            className={classes.nested}
            disabled
          >
            <Link to="/lpg-gas-payment" className={classes.links}>
              <ListItemText primary="LPG Gas" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 114}
            onClick={(event) => handleListItemClick(event, 114)}
            className={classes.nested}
            disabled
          >
            <Link to="/cable-tv-payment" className={classes.links}>
              <ListItemText primary="Cable TV" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 115}
            onClick={(event) => handleListItemClick(event, 115)}
            className={classes.nested}
            disabled
          >
            <Link to="/housing-society-payment" className={classes.links}>
              <ListItemText primary="Housing Society" />
            </Link>
          </ListItem>
          <ListItem
            button
            selected={selectedIndex === 116}
            onClick={(event) => handleListItemClick(event, 116)}
            className={classes.nested}
            disabled
          >
            <Link to="/municipal-taxes-payment" className={classes.links}>
              <ListItemText primary="Municipal Taxes" />
            </Link>
          </ListItem>
        </List>
      </Collapse>
      <ListItem
        button
        selected={selectedIndex === 200}
        onClick={(event) => handleListItemClick(event, 200)}
      >
        <Link to="/quick-bill-payment" className={classes.links}>
          <ListItemText primary="Quick Bill Payment" />
        </Link>
      </ListItem>
      <ListItem
        button
        selected={selectedIndex === 201}
        onClick={(event) => handleListItemClick(event, 201)}
      >
        <Link to="/check-transaction-status" className={classes.links}>
          <ListItemText primary="Check Transaction Status" />
        </Link>
      </ListItem>
      <ListItem
        button
        selected={selectedIndex === 202}
        onClick={(event) => handleListItemClick(event, 202)}
      >
        <Link to="/complain-register" className={classes.links}>
          <ListItemText primary="Register Complain" />
        </Link>
      </ListItem>
      <ListItem
        button
        selected={selectedIndex === 203}
        onClick={(event) => handleListItemClick(event, 203)}
      >
        <Link to="/check-complain-status" className={classes.links}>
          <ListItemText primary="Check Complain Status" />
        </Link>
      </ListItem>
    </List>
  );
};

export default Sidebar;
