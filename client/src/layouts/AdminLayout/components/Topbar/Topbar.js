import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

import { connect } from "react-redux";
import * as actions from "store/actions";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  title: {
    flexGrow: 1,
  },
}));

const Topbar = (props) => {
  const classes = useStyles();

  const Logout = () => {
    props.signout((result) => {
      if (result.redirect) {
        props.children.props.history.push("/signin");
      }
    });
  };

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Typography
          component="h1"
          variant="h4"
          color="inherit"
          noWrap
          className={classes.title}
        >
          WisePay
        </Typography>
        Welcome : {props.firstname}
        {` | Credit : ${props.credit}`}
        <IconButton color="inherit" onClick={Logout}>
          <ExitToAppIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};
function mapStateToProps(state) {
  return {
    firstname: state.authState.user.firstname,
    credit: state.authState.user.credit,
  };
}
export default connect(mapStateToProps, actions)(Topbar);
