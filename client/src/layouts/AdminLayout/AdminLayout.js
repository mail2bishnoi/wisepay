import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Drawer } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import { Footer, Sidebar, Topbar } from "./components";

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  drawerContainer: {
    overflow: "auto"
  },
  content: {
    display: "flex",
    flexGrow: 1,
    flexDirection: "column",
    padding: theme.spacing(3),
    paddingLeft: 256,
    minHeight: "100vh"
  }
}));

const AdminLayout = props => {
  const { title, children } = props;
  const classes = useStyles();
  return (
    <Fragment>
      <Topbar title={title} {...props} />
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <Toolbar />
        <Sidebar className={classes.sidebar} {...props} />
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        {children}
        <Footer />
      </main>
    </Fragment>
  );
};
AdminLayout.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string
};
export default AdminLayout;
