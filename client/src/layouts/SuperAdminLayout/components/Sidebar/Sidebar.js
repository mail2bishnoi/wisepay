import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import { Link } from "react-router-dom";

import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  links: {
    textDecoration: "none",
    color: "inherit"
  },
  nested: {
    paddingLeft: theme.spacing(8)
  },
  subnested: {
    paddingLeft: theme.spacing(10)
  }
}));

const Sidebar = props => {
  const classes = useStyles();

  React.useState(() => {
    const path = props.children.props.match.path;
  });
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
      dense
    >
      <ListItem
        button
      >
        <Link to="/users" className={classes.links}>
          <ListItemText primary="Manage User" />
        </Link>
      </ListItem>
    </List>
  );
};

export default Sidebar;
