import React from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import Moment from "react-moment";

import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";

const Results = ({ logs }) => {
  return (
    <>
      <Card>
        <PerfectScrollbar>
          <Box minWidth={760}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Transaction Id</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell>Customer Name</TableCell>
                  <TableCell>Bill Number</TableCell>
                  <TableCell>Transaction Time</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {logs.map((log) => (
                  <TableRow key={log.id} hover>
                    <TableCell>{log.txnrefid}</TableCell>
                    <TableCell>{log.responsereason}</TableCell>
                    <TableCell>{log.respamount * 0.01}</TableCell>
                    <TableCell>{log.customername}</TableCell>
                    <TableCell>{log.billnumber}</TableCell>
                    <TableCell>
                      <Moment format="YYYY-MM-DD HH:mm">{log.time}</Moment>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
      </Card>
    </>
  );
};

export default Results;
