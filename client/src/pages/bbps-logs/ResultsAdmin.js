import React from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import Moment from "react-moment";

import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";

const ResultsAdmin = ({ logs }) => {
  return (
    <>
      <Card>
        <PerfectScrollbar>
          <Box minWidth={760}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Type</TableCell>
                  <TableCell>Request Id</TableCell>
                  <TableCell>Response Code</TableCell>
                  <TableCell>Response</TableCell>
                  <TableCell>Agent Id</TableCell>
                  <TableCell>Transaction Id</TableCell>
                  <TableCell>Transaction Resp</TableCell>
                  <TableCell>Amount</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {logs.map((log) => (
                  <TableRow key={log.id} hover>
                    <TableCell>{log.requesttype}</TableCell>
                    <TableCell>{log.requestid}</TableCell>
                    <TableCell>
                      {log.responsecode === 0 ? "Success" : ""}
                    </TableCell>
                    <TableCell>{log.responsereason}</TableCell>
                    <TableCell>{log.agentid}</TableCell>
                    <TableCell>{log.txnrefid}</TableCell>
                    <TableCell>{log.txnresptype}</TableCell>
                    <TableCell>{log.respamount}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
      </Card>
    </>
  );
};

export default ResultsAdmin;
