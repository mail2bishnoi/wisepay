import React from "react";
import { connect } from "react-redux";
import * as actions from "store/actions";
import Results from "./Results";

class BBPSAdminLog extends React.Component {
  componentDidMount() {
    sessionStorage.setItem(
      "lastScreen",
      `/bbps-admin-log/${this.props.match.params.agentid}`
    );
    this.props.getBBPSLogByAgentId(this.props.match.params.agentid);
  }
  render() {
    return (
      <>
        <Results logs={this.props.bbpsLogs} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    bbpsLogs: state.bbpsState.bbpsLogs,
  };
};

export default connect(mapStateToProps, actions)(BBPSAdminLog);
