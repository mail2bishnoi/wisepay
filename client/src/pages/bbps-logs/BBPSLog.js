import React from "react";
import { connect } from "react-redux";
import * as actions from "store/actions";
import Results from "./Results";

class BBPSLog extends React.Component {
  componentDidMount() {
    sessionStorage.setItem("lastScreen", "/bbps-log");
    this.props.getBBPSLog();
  }
  render() {
    return (
      <>
        <Results logs={this.props.bbpsLogs} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    bbpsLogs: state.bbpsState.bbpsLogs,
  };
};

export default connect(mapStateToProps, actions)(BBPSLog);
