import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Title from "components/Title";
import Toolbar from "@material-ui/core/Toolbar";
import Moment from "react-moment";
import "moment-timezone";

const useStyles = makeStyles(theme => ({
  table: {
    marginTop: theme.spacing(2)
  }
}));

const TransactionDetails = props => {
  const status = props.status;
  const classes = useStyles();
  return (
    <>
      <Toolbar></Toolbar>
      <Title>Transaction Details</Title>
      <TableContainer className={classes.table} component="div">
        <Table size="small" aria-label="a dense table">
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                Agent Id
              </TableCell>
              <TableCell align="right">{status.agentId}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Amount
              </TableCell>
              <TableCell align="right">
                {parseFloat(status.amount) / 100}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Biller Id
              </TableCell>
              <TableCell align="right">{status.billerId}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Transaction Date
              </TableCell>
              <TableCell align="right">
                <Moment format="YYYY-MM-DD HH:mm">{status.txnDate}</Moment>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Transaction Status
              </TableCell>
              <TableCell align="right">{status.txnStatus}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Mobile
              </TableCell>
              <TableCell align="right">{status.mobile}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default TransactionDetails;
