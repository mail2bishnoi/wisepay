import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import TransactionDetails from "./TransactionDetails";
import Title from "components/Title";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const CheckTransactionStatus = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/check-transaction-status");
  }, []);
  const { handleSubmit } = props;

  const onSubmit = formValues => {
    props.checkTransactionStatus(formValues, result => {
      console.log(result);
      setOpen(true);
      //props.history.push("/show-bill-details");
    });
  };
  return (
    <>
      <Helmet>
        <title>Check Transaction Status</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo logo="BharatBillPay" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Check Transaction Status</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Field
                id="trackValue"
                name="trackValue"
                label="Transaction Ref ID"
                component={renderInput}
                required={false}
                autoComplete="off"
                fullWidth
              />
              <Typography variant="subtitle2" align="center" gutterBottom>
                OR
              </Typography>
              <Field
                id="customerMobile"
                name="customerMobile"
                label="Mobile Number"
                component={renderInput}
                required={false}
                autoComplete="off"
                fullWidth
              />
              <Field
                id="startDate"
                name="startDate"
                label="Start Date"
                component={renderInput}
                required={false}
                autoComplete="off"
                fullWidth
              />
              <Field
                id="endDate"
                name="endDate"
                label="End Date"
                component={renderInput}
                required={false}
                autoComplete="off"
                fullWidth
              />
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Check Status"}
              </Button>
            </form>
            {open && <TransactionDetails status={props.transactionStatus} />}
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  trackValue: {
    presence: { allowEmpty: false, message: "is required" }
  }
};
const validater = formValues => {
  let errors = {};
  errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  if (!state.bbpsState.transactionStatus) {
    return {
      loading: state.toggleLoading.toggleLoading
    };
  }
  return {
    transactionStatus:
      state.bbpsState.transactionStatus.jsonResponse.transactionStatusResp
        .txnList,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "checkTransactionStatus", validate: validater })
)(CheckTransactionStatus);
