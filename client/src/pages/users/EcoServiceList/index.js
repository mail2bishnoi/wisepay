import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { Box, Container, makeStyles, Button } from "@material-ui/core";

import Results from "./Results";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "store/actions";

import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const EcoServiceList = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem(
      "lastScreen",
      `/eco/user/services/activated/${props.match.params.user_code}`
    );
    props.getAllActivateServices(props.match.params.user_code);
  }, []);
  return (
    <>
	  <Helmet>
        <title>Eko Service List - wisepay.app admin</title>
      </Helmet>
      <Container maxWidth={false}>
        <Box mt={3}>
          {props.role === "superadmin" ? (
            <>
              <Button
                component={RouterLink}
                to={`/eco/user/service/activate/${props.match.params.user_code}`}
                variant="contained"
                color="secondary"
              >
                Activate Service
              </Button>
              <Button
                component={RouterLink}
                to={`/eco/user/aeps/activate/${props.match.params.user_code}`}
                variant="contained"
                color="secondary"
              >
                Activate AePS
              </Button>
            </>
          ) : (
            ""
          )}

          <Results services={props.services} />
        </Box>
      </Container>
    </>
  );
};

function mapStateToProps(state) {
  return {
    services: Object.values(state.ecoState.activate_services),
    role: state.authState.user.role
  };
}

export default compose(connect(mapStateToProps, actions))(EcoServiceList);
