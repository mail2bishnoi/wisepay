import React, { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";

import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  makeStyles
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {}
}));

const Results = ({ services }) => {
  console.log(services);
  const classes = useStyles();
  return (
    <>
      <Card>
        <PerfectScrollbar>
          <Box minWidth={760}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Service Code</TableCell>
                  <TableCell>Service Name</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Provider</TableCell>
                  <TableCell>Comment</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {services.map(service => (
                  <TableRow key={service.service_code} hover>
                    <TableCell>{service.service_code}</TableCell>
                    <TableCell>{}</TableCell>
                    <TableCell>{service.status_desc}</TableCell>
                    <TableCell>{service.service_provider}</TableCell>
                    <TableCell>{service.comments}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
      </Card>
    </>
  );
};

export default Results;
