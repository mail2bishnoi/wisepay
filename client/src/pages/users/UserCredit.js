import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { makeStyles } from "@material-ui/core/styles";

import { Button, Grid } from "@material-ui/core";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";

const useStyles = makeStyles((theme) => ({
  root: {},
  submit: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(3),
  },
}));

const UserCredit = (props) => {
  const classes = useStyles();
  const _id = props.match.params.id;
  useEffect(() => {
    sessionStorage.setItem("lastScreen", `users/credit/${_id}`);
  }, [_id]);
  const { handleSubmit } = props;
  const onSubmit = (formValues) => {
    props.addCredit(formValues, _id, (result) => {
      if (result.redirect) props.history.push("/users");
    });
  };
  const goBack = () => {
    props.history.goBack();
  };
  return (
    <>
      <Helmet>
        <title>Add User Credit</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <Title>Add Credit</Title>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Field
              id="credit"
              name="credit"
              label="Credit"
              type="text"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="outlined"
              onClick={goBack}
            >
              Go Back
            </Button>
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Add Credit"}
            </Button>
          </form>
        </Grid>
      </Grid>
    </>
  );
};

function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading,
  };
}
export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
    form: "addCredit",
  })
)(UserCredit);
