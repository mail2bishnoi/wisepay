import React from "react";
import { Helmet } from "react-helmet";
import { makeStyles } from "@material-ui/core/styles";

import { Button, Grid } from "@material-ui/core";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import EcoServiceSelect from "components/EcoServiceSelect";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  submit: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(3)
  }
}));

const EcoAePSActivate = props => {
  const classes = useStyles();
  const { handleSubmit } = props;
  const onSubmit = formValues => {
    console.log(formValues);
    props.ecoAePSActivate(formValues, result => {
      if (result) {
        props.history.push(
          `eco/user/services/activated/${props.match.params.user_code}`
        );
      }
    });
  };
  const goBack = () => {
    props.history.goBack();
  };
  const handleFileChange = (event, input) => {
    event.preventDefault();
    let imageFile = event.target.files[0];
    if (imageFile) {
      const localImageUrl = URL.createObjectURL(imageFile);
      const imageObject = new window.Image();

      imageObject.onload = () => {
        imageFile.width = imageObject.naturalWidth;
        imageFile.height = imageObject.naturalHeight;
        input.onChange(imageFile);
        URL.revokeObjectURL(imageFile);
      };
      imageObject.src = localImageUrl;
    }
  };
  const renderFile = ({ input, type, label, meta }) => {
    const { mime } = props;
    return (
      <div>
        <label>{label}</label>
        <input
          name={input.name}
          type={type}
          accept={mime}
          onChange={event => handleFileChange(event, input)}
        />
      </div>
    );
  };
  return (
    <>
      <Helmet>
        <title>Activate Eco AePS Service</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={6}>
          <Title>Activate Eco Service</Title>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <Field
                  id="service_code"
                  name="service_code"
                  label="Service Code"
                  component={EcoServiceSelect}
                  data={props.options}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="user_code"
                  name="user_code"
                  label="User Code"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="initiator_id"
                  name="initiator_id"
                  label="Initiator Id"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="devicenumber"
                  name="devicenumber"
                  label="Device Number"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="modelname"
                  name="modelname"
                  label="Model Name"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="line"
                  name="line"
                  label="Address Line"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="city"
                  name="city"
                  label="City"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="state"
                  name="state"
                  label="State"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  id="pincode"
                  name="pincode"
                  label="Pincode"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  label="Pancard"
                  name="pan_card"
                  type="file"
                  component={renderFile}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  label="AADHAR Front"
                  name="aadhar_front"
                  type="file"
                  component={renderFile}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Field
                  label="AADHAR Back"
                  name="aadhar_back"
                  type="file"
                  component={renderFile}
                />
              </Grid>
            </Grid>
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="outlined"
              onClick={goBack}
            >
              Go Back
            </Button>
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Activate"}
            </Button>
          </form>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  service_code: {
    presence: { allowEmpty: false, message: "is required" }
  },
  user_code: {
    presence: { allowEmpty: false, message: "is required" }
  },
  initiator_id: {
    presence: { allowEmpty: false, message: "is required" }
  },
  devicenumber: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      minimum: 10,
      maximum: 128
    }
  },
  modelname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 32
    }
  },

  line: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  city: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  state: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  pincode: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state, ownProps) {
  return {
    loading: state.toggleLoading.toggleLoading,
    options: Object.values(state.ecoState.all_services).filter(
      service => service.service_code === "43"
    )
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
    form: "ecoAePSActivate",
    validate: validater
  })
)(EcoAePSActivate);
