import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { Box, Container } from "@material-ui/core";

import Results from "./Results";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "store/actions";

const ActionHistoryList = (props) => {
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/user/actionhistory");
    props.getActionHistory(1, 25);
  }, []);

  const onChangePage = (page, limit) => {
    props.getActionHistory(page, limit);
  };
  return (
    <>
      <Helmet>
        <title>ActionHistory List - wisepay.app admin</title>
      </Helmet>
      <Container maxWidth={false}>
        <Box mt={3}>
          <Results
            actions={props.actions}
            onChangePage={onChangePage}
            count={props.count}
          />
        </Box>
      </Container>
    </>
  );
};

function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading,
    actions: Object.values(state.actionState.actions),
    count: state.actionState.count,
  };
}

export default compose(connect(mapStateToProps, actions))(ActionHistoryList);
