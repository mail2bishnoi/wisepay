import React, { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import Moment from "react-moment";

import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TablePagination,
} from "@material-ui/core";

const Results = ({ actions, onChangePage, count }) => {
  const [limit, setLimit] = useState(25);
  const [page, setPage] = useState(1);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    onChangePage(page, event.target.value);
  };
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
    onChangePage(newPage, limit);
  };

  return (
    <>
      <Card>
        <PerfectScrollbar>
          <Box minWidth={760}>
            <Table dense>
              <TableHead>
                <TableRow>
                  <TableCell>Username/Email</TableCell>
                  <TableCell>Action</TableCell>
                  <TableCell>Time</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {actions.map((action) => (
                  <TableRow key={action.id} hover>
                    <TableCell>{action.username}</TableCell>
                    <TableCell>{action.action}</TableCell>
                    <TableCell>
                      <Moment format="YYYY-MM-DD HH:mm">
                        {action.actiontime}
                      </Moment>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
        <TablePagination
          component="div"
          count={count}
          onChangePage={handlePageChange}
          onChangeRowsPerPage={handleLimitChange}
          page={page - 1}
          rowsPerPage={limit}
          rowsPerPageOptions={[10, 25, 50, 100]}
        />
      </Card>
    </>
  );
};

export default Results;
