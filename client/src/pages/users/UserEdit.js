import _ from "lodash";
import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "store/actions";

import UserEditForm from "./UserEditForm";

const UserEdit = props => {
  useEffect(() => {
    sessionStorage.setItem(
      "lastScreen",
      `/users/edit/${props.match.params.id}`
    );
  }, [props.match.params.id]);
  const onSubmit = function(formValues) {
    props.updateUserById(props.match.params.id, formValues, response => {
      props.history.push("/users");
    });
  };
  return (
    <>
	 <Helmet>
        <title>Update User</title>
      </Helmet>
      <UserEditForm initialValues={props.initialValues} onSubmit={onSubmit} />
    </>
  );
};
function mapStateToProps(state, ownProps) {
  return {
    initialValues: _.pick(
      state.userState[ownProps.match.params.id],
      "firstname",
      "middlename",
      "lastname",
      "phone",
      "email",
      "dob",
      "businessname",
      "line",
      "city",
      "area",
      "district",
      "state",
      "pincode",
      "kyc",
      "credit",
      "agentid",
      "ecousercode",
      "activate"
    ),
    loading: state.toggleLoading.toggleLoading
  };
}
export default compose(connect(mapStateToProps, actions))(UserEdit);
