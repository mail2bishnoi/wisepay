import _ from "lodash";
import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "store/actions";

import UserEditForm from "./UserEditForm";

const UserEditSelf = props => {
  const onSubmit = function(formValues) {
    props.updateUser(formValues);
  };
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/user/edit/self");
  }, []);
  return (
    <>
      <Helmet>
        <title>Update Profile</title>
      </Helmet>
      <UserEditForm initialValues={props.initialValues} onSubmit={onSubmit} />
    </>
  );
};

function mapStateToProps(state, ownProps) {
  return {
    initialValues: _.pick(
      state.authState.user,
      "firstname",
      "middlename",
      "lastname",
      "phone",
      "email",
      "dob",
      "businessname",
      "line",
      "city",
      "area",
      "district",
      "state",
      "pincode"
    ),
    loading: state.toggleLoading.toggleLoading
  };
}
export default compose(connect(mapStateToProps, actions))(UserEditSelf);
