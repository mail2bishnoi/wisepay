import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { makeStyles } from "@material-ui/core/styles";

import { Button, Grid } from "@material-ui/core";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  root: {},
  submit: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(3)
  }
}));
const ResetPassword = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem(
      "lastScreen",
      `users/resetpassword/${props.match.params.email}`
    );
  }, [props.match.params.email]);
  const { handleSubmit } = props;
  const onSubmit = formValues => {
    props.resetPassword(formValues, result => {
      if (result.redirect) props.history.push("/users");
    });
  };
  const goBack = () => {
    props.history.goBack();
  };
  return (
    <>
      <Helmet>
        <title>Reset Password</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <Title>Change Password</Title>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Field
              id="email"
              name="email"
              label="Email"
              type="text"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Field
              id="password"
              name="password"
              label="New Password"
              type="password"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="outlined"
              onClick={goBack}
            >
              Go Back
            </Button>
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Reset Password"}
            </Button>
          </form>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  email: {
    presence: { allowEmpty: false, message: "is required" },
    email: true
  },
  password: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      minimum: 8,
      maximum: 64
    }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
    form: "resetPassword",
    validate: validater
  })
)(ResetPassword);
