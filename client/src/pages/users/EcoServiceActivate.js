import React from "react";
import { Helmet } from "react-helmet";
import { makeStyles } from "@material-ui/core/styles";

import { Button, Grid } from "@material-ui/core";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import EcoServiceSelect from "components/EcoServiceSelect";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  submit: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(3)
  }
}));

const EcoServiceActivate = props => {
  const classes = useStyles();
  const { handleSubmit } = props;
  const onSubmit = formValues => {
    console.log(formValues);
    props.ecoServiceActivate(formValues);
  };
  const goBack = () => {
    props.history.goBack();
  };
  return (
    <>
      <Helmet>
        <title>Activate Eco Service</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <Title>Activate Eco Service</Title>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Field
              id="service_code"
              name="service_code"
              label="Service Code"
              component={EcoServiceSelect}
              data={props.options}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Field
              id="user_code"
              name="user_code"
              label="User Code"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Field
              id="initiator_id"
              name="initiator_id"
              label="Initiator Id"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="outlined"
              onClick={goBack}
            >
              Go Back
            </Button>
            <Button
              className={classes.submit}
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Activate"}
            </Button>
          </form>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  service_code: {
    presence: { allowEmpty: false, message: "is required" }
  },
  user_code: {
    presence: { allowEmpty: false, message: "is required" }
  },
  initiator_id: {
    presence: { allowEmpty: false, message: "is required" }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading,
    options: Object.values(state.ecoState.all_services)
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
    form: "ecoServiceActivate",
    validate: validater
  })
)(EcoServiceActivate);
