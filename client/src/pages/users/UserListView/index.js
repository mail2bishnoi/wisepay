import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { Box, Container, makeStyles } from "@material-ui/core";

import Results from "./Results";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "store/actions";

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const UserListView = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/users");
    props.getUsers();
  }, []);
  return (
    <>
	  <Helmet>
        <title>User List - wisepay.app admin</title>
      </Helmet>
      <Container maxWidth={false}>
        <Box mt={3}>
          <Results users={props.users} />
        </Box>
      </Container>
    </>
  );
};

function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading,
    users: Object.values(state.userState)
  };
}

export default compose(connect(mapStateToProps, actions))(UserListView);
