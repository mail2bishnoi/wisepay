import React, { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import { Link } from "react-router-dom";

import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  makeStyles,
  Tooltip,
} from "@material-ui/core";

import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import HowToRegIcon from "@material-ui/icons/HowToReg";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import PageviewIcon from "@material-ui/icons/Pageview";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";

const useStyles = makeStyles((theme) => ({
  root: {},
}));

const Results = ({ users }) => {
  console.log(users);
  const classes = useStyles();
  return (
    <>
      <Card>
        <PerfectScrollbar>
          <Box minWidth={760}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>KYC</TableCell>
                  <TableCell>Phone</TableCell>
                  <TableCell>Eco Code</TableCell>
                  <TableCell>Agent Id</TableCell>
                  <TableCell>#Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {users.map((user) => (
                  <TableRow key={user.id} hover>
                    <TableCell>{`${user.firstname} ${user.lastname}`}</TableCell>
                    <TableCell>{user.email}</TableCell>
                    <TableCell>{user.kyc ? "Done" : "Pending"}</TableCell>
                    <TableCell>{user.phone}</TableCell>
                    <TableCell>{user.ecousercode}</TableCell>
                    <TableCell>{user.agentid}</TableCell>
                    <TableCell>
                      <Link to={`users/edit/${user._id}`}>
                        <Tooltip title="Edit">
                          <EditIcon fontSize="small" color="primary" />
                        </Tooltip>
                      </Link>
                      <Link to={`users/credit/${user._id}`}>
                        <Tooltip title="Add Credit">
                          <AccountBalanceWalletIcon
                            fontSize="small"
                            color="primary"
                          />
                        </Tooltip>
                      </Link>
                      <Link to={`users/delete/${user._id}`}>
                        <Tooltip title="Delete">
                          <DeleteIcon fontSize="small" color="secondary" />
                        </Tooltip>
                      </Link>
                      <Link to={`users/eco/onboard/${user._id}`}>
                        <Tooltip title="Eco Onboard">
                          <HowToRegIcon fontSize="small" color="primary" />
                        </Tooltip>
                      </Link>
                      <Link
                        to={`eco/user/services/activated/${user.ecousercode}`}
                      >
                        <Tooltip title="Eco Service Activate">
                          <VpnKeyIcon fontSize="small" color="secondary" />
                        </Tooltip>
                      </Link>
                      <Link to={`users/resetpassword/${user.email}`}>
                        <Tooltip title="Reset Password">
                          <LockOpenIcon fontSize="small" color="primary" />
                        </Tooltip>
                      </Link>
                      <Link to={`bbps-admin-log/${user.agentid}`}>
                        <Tooltip title="BBPS Transaction Logs">
                          <PageviewIcon fontSize="small" color="secondary" />
                        </Tooltip>
                      </Link>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
      </Card>
    </>
  );
};

export default Results;
