import _ from "lodash";
import React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import validate from "validate.js";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(3),
  },
  links: {
    color: "inherit",
    textDecoration: "none",
  },
}));

const UserEditForm = (props) => {
  const classes = useStyles();

  const { handleSubmit } = props;

  const onSubmit = function (formValues) {
    props.onSubmit(formValues);
  };
  const goBack = () => {
    //console.log(props);
  };
  return (
    <>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className={classes.form}
        noValidate
      >
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <Field
              name="firstname"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="firstname"
              label="First Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="middlename"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="middlename"
              label="Middle Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="lastname"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="lastname"
              label="Last Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="dob"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="dob"
              label="DOB:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="phone"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="phone"
              label="Mobile:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="email"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="email"
              label="Email:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="businessname"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="businessname"
              label="Shop/Business Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="line"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="line"
              label="Address Line:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="city"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="city"
              label="City:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="area"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="area"
              label="Area:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="district"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="district"
              label="District:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name="pincode"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              fullWidth
              id="pincode"
              label="Pincode:"
              autoComplete="off"
            />
          </Grid>
          {props.role === "superadmin" ? (
            <>
              <Grid item xs={4}>
                <Field
                  name="kyc"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="kyc"
                  label="KYC:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name="activate"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="activate"
                  label="Activate:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name="credit"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="credit"
                  label="Credit:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={4}>
                <Field
                  name="agentid"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="agentid"
                  label="Agent Id:"
                  autoComplete="off"
                />
              </Grid>
            </>
          ) : (
            ""
          )}
        </Grid>
        {props.role === "superadmin" ? (
          <Button
            className={classes.submit}
            size="large"
            color="primary"
            variant="outlined"
            onClick={goBack}
          >
            Go Back
          </Button>
        ) : (
          ""
        )}
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.submit}
          disabled={!props.valid}
        >
          {props.loading ? <Loading /> : "Update User"}
        </Button>
      </form>
    </>
  );
};

const schema = {
  firstname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128,
    },
  },
  lastname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128,
    },
  },
  phone: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 10,
    },
  },
  email: {
    presence: { allowEmpty: false, message: "is required" },
    email: true,
    length: {
      maximum: 128,
    },
  },
  businessname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128,
    },
  },
};
const validater = (formValues) => {
  const errors = validate(formValues, schema);
  return errors;
};

function mapStateToProps(state, ownProps) {
  return {
    loading: state.toggleLoading.toggleLoading,
    role: state.authState.user.role,
  };
}

export default compose(
  connect(mapStateToProps, {}),
  reduxForm({ form: "userEdit", validate: validater })
)(UserEditForm);
