import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { makeStyles } from "@material-ui/core/styles";

import { Button, Grid } from "@material-ui/core";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const ChangePassword = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/user/changepassword");
  }, []);
  const { handleSubmit } = props;
  const onSubmit = formValues => {
    props.changePassword(formValues, result => {
      if (result.redirect) props.history.push("/signin");
    });
  };
  return (
    <>
      <Helmet>
        <title>Change Password</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <Title>Change Password</Title>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Field
              id="password"
              name="password"
              label="Current Password"
              type="password"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Field
              id="newpassword"
              name="newpassword"
              label="New Password"
              type="text"
              component={renderInput}
              required={true}
              autoComplete="off"
              fullWidth
            />
            <Button
              className={classes.registerButton}
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Change Password"}
            </Button>
          </form>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  password: {
    presence: { allowEmpty: false, message: "is required" }
  },
  newpassword: {
    presence: { allowEmpty: false, message: "is required" }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
    form: "changePassword",
    validate: validater
  })
)(ChangePassword);
