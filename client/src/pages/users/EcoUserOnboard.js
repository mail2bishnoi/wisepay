import _ from "lodash";
import React from "react";
import { Helmet } from "react-helmet";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(3)
  },
  links: {
    color: "inherit",
    textDecoration: "none"
  }
}));

const EcoUserOnboard = props => {
  const classes = useStyles();

  const { handleSubmit } = props;

  const onSubmit = function(formValues) {
    props.onboardEcoUser(props.match.params.id, formValues, response => {
      props.history.push("/users");
    });
  };
  const goBack = () => {
    props.history.goBack();
  };
  return (
    <>
      <Helmet>
        <title>Eko User Onboard</title>
      </Helmet>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className={classes.form}
        noValidate
      >
        <Grid container spacing={2}>
          <Grid item xs={6} md={4}>
            <Field
              name="initiator_id"
              type="number"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="initiator_id"
              label="Initiator Id:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="firstname"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="firstname"
              label="First Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="lastname"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="lastname"
              label="Last Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="phone"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="phone"
              label="Mobile:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="email"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="email"
              label="Email:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="businessname"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="businessname"
              label="Shop/Business Name:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="pannumber"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="pannumber"
              label="Pan Card No:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="dob"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="dob"
              label="Date of Birth:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="line"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="line"
              label="Address Line:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="area"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="area"
              label="Area:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="city"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="city"
              label="City:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="district"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="district"
              label="District:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="state"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="state"
              label="State:"
              autoComplete="off"
            />
          </Grid>
          <Grid item xs={6} md={4}>
            <Field
              name="pincode"
              type="number"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="pincode"
              label="Pincode:"
              autoComplete="off"
            />
          </Grid>
        </Grid>
        <Button
          className={classes.submit}
          size="large"
          color="primary"
          variant="outlined"
          onClick={goBack}
        >
          Go Back
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.submit}
          disabled={!props.valid}
        >
          {props.loading ? <Loading /> : "Onboard User"}
        </Button>
      </form>
    </>
  );
};

const schema = {
  firstname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  lastname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  phone: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 10
    }
  },
  email: {
    presence: { allowEmpty: false, message: "is required" },
    email: true,
    length: {
      maximum: 128
    }
  },
  businessname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  pannumber: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 10
    }
  },
  line: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  city: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  area: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  district: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  state: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  pincode: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};

function mapStateToProps(state, ownProps) {
  return {
    initialValues: _.pick(
      state.userState[ownProps.match.params.id],
      "firstname",
      "lastname",
      "phone",
      "email",
      "businessname",
      "pannumber",
      "line",
      "city",
      "state",
      "pincode",
      "district",
      "area"
    ),
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "onboardEcoUser", validate: validater })
)(EcoUserOnboard);
