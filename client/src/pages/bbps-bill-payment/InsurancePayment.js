import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import BillerSelect from "components/BillerSelect";
import Title from "components/Title";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const InsurancePayment = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/insurance-payment");
  }, []);
  const { handleSubmit } = props;
  const [inputParams, setInputParams] = React.useState([]);

  const onBillerSelect = selectedBiller => {
    const billerInputParams = selectedBiller.billerInputParams.paramInfo;
    if (Array.isArray(billerInputParams)) {
      setInputParams(billerInputParams);
    } else {
      let temp = [];
      temp.push(billerInputParams);
      setInputParams(temp);
    }
  };
  const onSubmit = formValues => {
    console.log("inside on submit function");
  };
  return (
    <>
      <Helmet>
        <title>Life Insurance Policy Premium Online</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo />
          <Paper className={classes.paper} elevation={3}>
            <Title>Insurance Payment</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Field
                id="biller"
                name="biller"
                label="Biller"
                component={BillerSelect}
                required={true}
                autoComplete="off"
                billers={props.billers}
                onChange={selectedBiller => onBillerSelect(selectedBiller)}
              />
              {inputParams.map(inputParam => {
                return (
                  <Field
                    id={inputParam.paramName}
                    name={inputParam.paramName}
                    label={inputParam.paramName}
                    type={inputParam.dataType === "NUMERIC" ? "number" : "text"}
                    component={renderInput}
                    required={inputParam.isOptional === false ? true : false}
                    autoComplete="off"
                    fullWidth
                    key={inputParam.paramName}
                  />
                );
              })}
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Go"}
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
const schema = {
  biller: {
    presence: { allowEmpty: false, message: "is required" }
  }
};

const validater = formValues => {
  let inputParams = [];
  if (
    formValues.biller &&
    formValues.biller.billerInputParams &&
    formValues.biller.billerInputParams.paramInfo
  ) {
    inputParams = formValues.biller.billerInputParams.paramInfo;
    inputParams.map(inputParam => {
      schema[`${inputParam.paramName}`] = {
        presence: {
          allowEmpty: `${inputParam.isOptional}`,
          message: "is required"
        },
        length: {
          minimum: parseInt(`${inputParam.minLength}`),
          maximum: parseInt(`${inputParam.maxLength}`)
        },
        format: {
          pattern: `${inputParam.regEx ? inputParam.regEx : ".*"}`
        }
      };
    });
  }
  const errors = validate(formValues, schema);
  return errors;
};

function mapStateToProps(state) {
  const billers = state.billersState.billers.billerInfoResponse.biller;
  const insuranceBillers = billers.filter(
    biller => biller.billerCategory === "Insurance"
  );
  return {
    billers: insuranceBillers,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "insurancePayment", validate: validater })
)(InsurancePayment);
