import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import BillerCategorySelect from "components/BillerCategorySelect";
import BillerSelect from "components/BillerSelect";
import Title from "components/Title";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const QuickBillPayment = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/quick-bill-payment");
  }, []);
  const { handleSubmit } = props;
  const [inputParams, setInputParams] = React.useState([]);
  const [open, setOpen] = React.useState(false);

  const onBillerCategorySelect = selectedBillerCategory => {
    props.onSelectBillerCategory(selectedBillerCategory);
  };

  const onBillerSelect = selectedBiller => {
    setOpen(true);
    props.onSelectBiller(selectedBiller);
    const billerInputParams = selectedBiller.billerInputParams.paramInfo;
    if (Array.isArray(billerInputParams)) {
      setInputParams(billerInputParams);
    } else {
      let temp = [];
      temp.push(billerInputParams);
      setInputParams(temp);
    }
  };
  const onCustomerMobileChange = event => {
    props.onCustomerMobileChange(event.target.value);
  };
  const onSubmit = formValues => {
    console.log("inside on submit function");
    const amountInfo = {
      amount: parseFloat(formValues.billAmount) * 100,
      currency: 356,
      custConvFee: 0,
      amountTags: ""
    };
    const paymentMethod = {
      paymentMode: "Cash",
      quickPay: "Y",
      splitPay: "N"
    };
    const paymentInfo = {
      info: [{ infoName: "Remarks", infoValue: "Received" }]
    };
    let root = {};
    root.amountInfo = amountInfo;
    root.paymentMethod = paymentMethod;
    root.paymentInfo = paymentInfo;

    props.quickBillPayment(root, result => {
      props.history.push("/show-transaction-details");
    });
  };

  return (
    <>
      <Helmet>
        <title>Quick Bill Pay - Pay online bill quickly.</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo logo="BharatBillPay" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Quick Bill Payment</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Field
                id="billerCategory"
                name="biller"
                label="Biller Category"
                component={BillerCategorySelect}
                required={true}
                autoComplete="off"
                billers={props.billers}
                onChange={selectedBillerCategory =>
                  onBillerCategorySelect(selectedBillerCategory)
                }
              />
              <Field
                id="biller"
                name="biller"
                label="Biller"
                component={BillerSelect}
                required={true}
                autoComplete="off"
                billers={props.billers}
                onChange={selectedBiller => onBillerSelect(selectedBiller)}
              />
              {open && (
                <Field
                  id="customerMobile"
                  name="payeeMobile"
                  label="Payee Mobile Number"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                  onChange={event => onCustomerMobileChange(event)}
                />
              )}
              {inputParams.map(inputParam => {
                return (
                  <Field
                    id={inputParam.paramName}
                    name={inputParam.paramName}
                    label={inputParam.paramName}
                    type={inputParam.dataType === "NUMERIC" ? "number" : "text"}
                    component={renderInput}
                    required={inputParam.isOptional === false ? true : false}
                    autoComplete="off"
                    fullWidth
                    key={inputParam.paramName}
                  />
                );
              })}
              {open && (
                <Field
                  id="billAmount"
                  name="billAmount"
                  label="Bill Amount"
                  component={renderInput}
                  required={true}
                  autoComplete="off"
                  fullWidth
                />
              )}
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Go"}
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  biller: {
    presence: { allowEmpty: false, message: "is required" }
  },
  payeeMobile: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 10
    }
  }
};
const validater = formValues => {
  if (
    formValues.biller &&
    formValues.biller.billerInputParams &&
    formValues.biller.billerInputParams.paramInfo
  ) {
    console.log(formValues.biller.billerInputParams.paramInfo);
  }
  let errors = {};
  //const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state, ownProps) {
  console.log("own props ==>> ", ownProps);
  const billers = state.billersState.billers.billerInfoResponse.biller;
  let electricityBillers = [];
  if (state.bbpsState.selectedBillerCategory) {
    electricityBillers = billers.filter(
      biller =>
        biller.billerCategory ===
          state.bbpsState.selectedBillerCategory.billerCategory &&
        biller.billerFetchRequiremet === "NOT_SUPPORTED"
    );
  }
  return {
    billers: electricityBillers,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "quickBillPayment", validate: validater })
)(QuickBillPayment);
