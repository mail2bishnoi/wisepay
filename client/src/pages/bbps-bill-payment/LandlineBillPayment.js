import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import BillerSelect from "components/BillerSelect";
import Title from "components/Title";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const LandlineBillPayment = props => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/landline-bill-payment");
  }, []);  
  const { handleSubmit } = props;
  const [inputParams, setInputParams] = React.useState([]);

  const onBillerSelect = selectedBiller => {
    const billerInputParams = selectedBiller.billerInputParams.paramInfo;
    if (Array.isArray(billerInputParams)) {
      setInputParams(billerInputParams);
    } else {
      let temp = [];
      temp.push(billerInputParams);
      setInputParams(temp);
    }
  };
  const onSubmit = formValues => {
    console.log("inside on submit function");
  };
  return (
      <>
      <Helmet>
        <title>Landline Bill Payment: Pay Landline Bill Online</title>
      </Helmet>
    <Grid container spacing={3}>
      <Grid item xs={12} md={6} lg={4}>
        <BbpsLogo />
        <Paper className={classes.paper} elevation={3}>
          <Title>Landline Bill Payment</Title>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Field
              id="biller"
              name="biller"
              label="Biller"
              component={BillerSelect}
              required={true}
              autoComplete="off"
              billers={props.billers}
              onChange={selectedBiller => onBillerSelect(selectedBiller)}
            />
            {inputParams.map(inputParam => {
              return (
                <Field
                  id={inputParam.paramName}
                  name={inputParam.paramName}
                  label={inputParam.paramName}
                  type={inputParam.dataType === "NUMERIC" ? "number" : "text"}
                  component={renderInput}
                  required={inputParam.isOptional === false ? true : false}
                  autoComplete="off"
                  fullWidth
                  key={inputParam.paramName}
                />
              );
            })}
            <Button
              className={classes.registerButton}
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Go"}
            </Button>
          </form>
        </Paper>
      </Grid>
    </Grid>
	</>
  );
};
const schema = {
  name: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 32
    }
  },
  pin: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 4
    }
  }
};
const validater = formValues => {
  let errors = {};
  //const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  const billers = state.billersState.billers.billerInfoResponse.biller;
  const landlineBillers = billers.filter(
    biller => biller.billerCategory === "Landline Postpaid"
  );
  return {
    billers: landlineBillers,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "landlineBillPayment", validate: validater })
)(LandlineBillPayment);
