import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import BillerSelect from "components/BillerSelect";
import Title from "components/Title";
import validate from "validate.js";

import {
  handleInputParams,
  getInputParamsFromFormValues,
} from "utils/jsonHandler";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

const MobileBillPayment = (props) => {
  const classes = useStyles();
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/mobile-postpaid-payment");
  }, []);
  const { handleSubmit } = props;
  const [inputParams, setInputParams] = React.useState([]);

  const onBillerSelect = (selectedBiller) => {
    props.onSelectBiller(selectedBiller);
    setInputParams(handleInputParams(selectedBiller));
  };
  const onCustomerMobileChange = (event) => {
    props.onCustomerMobileChange(event.target.value);
  };
  const onSubmit = (formValues) => {
    let inputFormObject = {};
    const clonedFormValues = Object.assign({}, formValues);

    if (clonedFormValues.biller && clonedFormValues.biller.billerId) {
      inputFormObject.billerId = clonedFormValues.biller.billerId;
      delete clonedFormValues.biller;
      delete clonedFormValues.payeeMobile;
    }
    const customerInfo = {
      customerMobile: formValues.payeeMobile,
      customerPan: "",
      customerEmail: "",
      customerAdhaar: "",
    };
    inputFormObject.customerInfo = customerInfo;
    inputFormObject.inputParams = {
      input: getInputParamsFromFormValues(clonedFormValues),
    };
    props.mobilePostpaidBillFetch(inputFormObject, (result) => {
      props.history.push("/show-bill-details");
    });
  };
  return (
    <>
      <Helmet>
        <title>
          Postpaid Bill Payment: Pay Postpaid Bill Online | WisePay.com
        </title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo logo="BharatBillPay" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Mobile Postpaid Bill Payment</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Field
                id="biller"
                name="biller"
                label="Biller"
                component={BillerSelect}
                required={true}
                autoComplete="off"
                billers={props.billers}
                onChange={(selectedBiller) => onBillerSelect(selectedBiller)}
              />
              <Field
                id="customerMobile"
                name="payeeMobile"
                label="Payee Mobile Number"
                component={renderInput}
                required={true}
                autoComplete="off"
                fullWidth
                onChange={(event) => onCustomerMobileChange(event)}
              />
              {inputParams.map((inputParam) => {
                return (
                  <Field
                    id={inputParam.paramName}
                    name={inputParam.paramName}
                    label={inputParam.paramName}
                    type={inputParam.dataType === "NUMERIC" ? "number" : "text"}
                    component={renderInput}
                    required={inputParam.isOptional === false ? true : false}
                    autoComplete="off"
                    fullWidth
                    key={inputParam.paramName}
                  />
                );
              })}
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Fetch Bill"}
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
const schema = {
  biller: {
    presence: { allowEmpty: false, message: "is required" },
  },
  payeeMobile: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 10,
    },
  },
};
const validater = (formValues) => {
  let inputParams = [];
  if (
    formValues.biller &&
    formValues.biller.billerInputParams &&
    formValues.biller.billerInputParams.paramInfo
  ) {
    const inputParamsTemp = formValues.biller.billerInputParams.paramInfo;
    if (!Array.isArray(inputParamsTemp)) {
      inputParams.push(inputParamsTemp);
    } else {
      inputParams = inputParamsTemp;
    }
    inputParams.map((inputParam) => {
      schema[`${inputParam.paramName}`] = {
        presence: {
          allowEmpty: `${inputParam.isOptional}`,
          message: "is required",
        },
        length: {
          minimum: parseInt(`${inputParam.minLength}`),
          maximum: parseInt(`${inputParam.maxLength}`),
        },
        format: {
          pattern: `${inputParam.regEx ? inputParam.regEx : ".*"}`,
        },
      };
    });
  }
  const errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  const billers = state.billersState.billers.billerInfoResponse.biller;
  const mobileBillers = billers.filter(
    (biller) => biller.billerCategory === "Mobile Postpaid"
  );
  return {
    billers: mobileBillers,
    loading: state.toggleLoading.toggleLoading,
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "mobileBillPayment", validate: validater })
)(MobileBillPayment);
