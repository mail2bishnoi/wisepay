import _ from "lodash";
import React from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import PaymentModeSelect from "components/PaymentModeSelect";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";

import { getInitialFormValueFromState } from "utils/jsonHandler";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const ShowBillDetails = props => {
  const classes = useStyles();
  const { handleSubmit } = props;

  if (props.error) {
    return <div>Something Went Wrong, Please try again</div>;
  }
  const updateTotalAmount = (value, previousValue, allValues) => {
    const ccf = parseFloat(value);
    if (isNaN(ccf)) {
      return 0;
    }
    if (ccf < 0) return 0;
    const amount = parseFloat(allValues["amount"]);
    const newValue = amount + ccf - parseFloat(previousValue);
    props.change("amount", newValue);
    return value;
  };
  const onSubmit = formValues => {
    console.log(formValues);
    const amountInfo = {
      amount: parseFloat(formValues.amount) * 100,
      currency: 356,
      custConvFee: 0,
      amountTags: ""
    };
    const paymentMethod = {
      paymentMode: "Cash",
      quickPay: "N",
      splitPay: "N"
    };
    const paymentInfo = {
      info: [{ infoName: "Remarks", infoValue: "Received" }]
    };
    let root = {};
    const {
      inputParams,
      billerResponse,
      additionalInfo
    } = props.billFetchResponse;

    root.inputParams = inputParams;
    root.billerResponse = billerResponse;
    root.additionalInfo = additionalInfo;
    root.requestId = props.requestId;
    root.amountInfo = amountInfo;
    root.paymentMethod = paymentMethod;
    root.paymentInfo = paymentInfo;

    props.mobilePostpaidBillPayment(root, result => {
      props.history.push("/show-transaction-details");
    });
  };

  return (
    <>
      <Helmet>
        <title>Confirm Bill Payment</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={6}>
          <BbpsLogo logo="BharatBillPay" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Confirm Bill Payment</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                  <Field
                    id="billerName"
                    name="billerName"
                    label="Biller Name"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="customerName"
                    name="customerName"
                    label="Customer Name"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="customerMobile"
                    name="customerMobile"
                    label="Customer Mobile"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="billAmount"
                    name="billAmount"
                    label="Bill Amount"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="billDate"
                    name="billDate"
                    label="Bill Date"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="billNumber"
                    name="billNumber"
                    label="Bill Number"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="billPeriod"
                    name="billPeriod"
                    label="Bill Period"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="customerName"
                    name="customerName"
                    label="Customer Name"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="dueDate"
                    name="dueDate"
                    label="Due Date"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
                {props.inputParams.map(inputParam => {
                  return (
                    <Grid item xs={12} md={6} key={inputParam.paramName}>
                      <Field
                        id={inputParam.paramName}
                        name={inputParam.paramName}
                        label={inputParam.paramName}
                        type={
                          inputParam.dataType === "NUMERIC" ? "number" : "text"
                        }
                        component={renderInput}
                        required={!inputParam.isOptional}
                        autoComplete="off"
                        fullWidth
                        disabled
                      />
                    </Grid>
                  );
                })}
                <Grid item xs={12} md={6}>
                  <Field
                    id="paymentMode"
                    name="paymentMode"
                    label="Total Amount"
                    component={PaymentModeSelect}
                    required={true}
                    autoComplete="off"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="custConvFee"
                    name="custConvFee"
                    label="Customer Convenience Fees"
                    type="number"
                    component={renderInput}
                    required={true}
                    autoComplete="off"
                    normalize={updateTotalAmount}
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Field
                    id="amount"
                    name="amount"
                    label="Total Amount"
                    component={renderInput}
                    required={false}
                    autoComplete="off"
                    fullWidth
                    disabled
                  />
                </Grid>
              </Grid>
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Pay"}
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
const validater = formValues => {
  let error = {};
  return error;
};

function mapStateToProps(state) {
  const simpleObject = getInitialFormValueFromState(state);

  try {
    return {
      loading: state.toggleLoading.toggleLoading,
      requestId: simpleObject.lastBillFetchRequestId,
      inputParams: simpleObject.inputParams.input,
      initialValues: simpleObject.initialFormValues,
      billFetchResponse: simpleObject.billFetchResponse
    };
  } catch (error) {
    return { error: error };
  }
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({
    form: "showBillDetails",
    validate: validater
  })
)(ShowBillDetails);
