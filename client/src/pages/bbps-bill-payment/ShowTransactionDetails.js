import React, { Component } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "store/actions";
import Title from "components/Title";

import jsPDF from "jspdf";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));
// data url maker -->> https://dataurl.sveinbjorn.org/#dataurlmaker
//jspdf example -->> http://mrrio.github.io/jsPDF/
const ShowTransactionDetails = props => {
  const classes = useStyles();
  const imageData =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWUAAAE8CAIAAACXZjfTAAAAA3NCSVQICAjb4U/gAAAAGXRFWHRTb2Z0d2FyZQBnbm9tZS1zY3JlZW5zaG907wO/PgAAIABJREFUeJztnWdgFEUbx2dmy90lhFACCYQeWui9N+lSVIqABQUEQcCCjRcExYoFCyhFEaSIKIiAIlXp0nsPLdSQBEhIILnb292Z98Mee5vL7eUI129+X9ydmZ178C7/nfLM80BCCKBQKBQ3QP42gEKhBA1ULygUirtQvaBQKO5C9YJCobgL1QsKheIuVC8oFIq7UL2gUCjuQvWCQqG4C9ULCoXiLlQvKBSKu1C9oFAo7kL1gkKhuAvVCwqF4i5ULygUirtQvaBQKO5C9YJCobgL1QsKheIuVC8oFIq7UL2gUCjuQvWCQqG4C9ULCoXiLlQvKBSKu1C9oFAo7kL1gkKhuAvVCwqF4i5ULygUirtQvaBQKO5C9YJCobgL1QsKheIuVC8oFIq7UL2gUCjuQvWCQqG4C9ULCoXiLlQvKBSKu7D+NoAS8MgiEXKBORsQ7FhljIK8CfAmf5hF8QNULyiAZN7A6RfJ7avkzg1y9xa5k0ay00l2OsnJJEIukMUCe4AR0YCPgLwJRsXAoqVhdGkYFQOLl4FFY1FcAixZHkA6kg0FICHE3zZQfArJuIavHJOvHCep53B6Mk6/CKxm734kZ0CxVVFsAoyrhuITmQp1YUxF734ixTtQvQh9iDkbn98rX9iHLx/DV46RnEx/WwRgRDQqXxdVqIcq1meqt4JFS/nbIopbUL0ITYjlLj67Wz67Sz67C187CbDsb4v0gRCVqcHUbMvUbIOqtYSmov42iKIL1YuQgty+Kh3fJB/dIJ/bBaSC1x0CDsQwCU2Zel2Zet1QbIK/raE4QvUiFMApSdLB1fKR9fj6KX/b4jFQbAJTrxvbsAeq0sTftlBsUL0IYvDV49Lu36Qj60nGNX/b4kVgsTJso55siwGoQj1/2xLuUL0IQkRBOrha3L4IJx8E4fT1ocqN2BYD2Ob9obGIv20JU6heBBPk9lVx+0Lpv1/IvQxv9A8NkTA6FkbFgKgYFF0aRsUAQyQ0FgER0ZCPACzvYA7JzQbCPWLJAdZcYs4m5rskK43cu02y0sndm0DI9YqRxii2RX+2/VBUpro3+qe4gOpFcCCf3yNunC0f3+TEybJwQIRiE1C5WrB0FVSqMoqtAktVhlElPdM5AAAAYrlLUi/g1LM49TxJu4BvJOH0ZI/t1EDIVG/NPfICU78bdQbzGVQvAhtC5OMbrRu+wxf2P3xnMKYiU7U5qlAXVaiHyteBhsiH7/PBEHLlayfwleP4ylF8+Ri+kfTw8ylUtgbX/RW2yRMAMR6xkeICqheBCpalfSvEDd/hG2cfphtYvCxTow1Tow1ToxUsUc5T1nkEcve2fG43Ttopn931kP9MVKoy120s22IAYDlPmUfJD9WLwIMQ6chacfWnOPVcIXtADFO5MVOvK1O3Cypbo/CG5GQCczaxWoAkEHM2kKzEkqP5FASNUTAiGhgioTESGItAY1ThP+tOqnx8o3RkvZy0E0jWwnUCS5TjH3ubbd6fzlC8BNWLwEJO2mld+TG+dLgwD7Mck9iBbdybqdMZFinh7lNYJhnXcfoFnHqBpJ3HmSnk7i1y5wa5e/uB/245IyoRD6NjYfF4WCwWlaoESyeguARYtLT7fRDLXfnEZvnw39KxjUC0PJgBAAAAUNma/BMTmXpdC/EsxTVULwIFnHbBumyyfHLzAz+JGKZaS7ZpH6ZhTxhZrMDmRMjB107iqyfwlWP46gl842yh3+duAo1RMLYKKlcbla+LytdB5Wq5s3RCLHflw2ulvb/LSf8VYpWXSWjG938PVW5cKJMpzqF6EQAIudZ134j/zH5QD25YvCzX5lm29dOwWJzrliQ3C5/fIyf9J5/dha+dcuvPj+VgVAyMKgVYHhoigKEIZHlg0sw4ZAlY7irbqMByj1jukbu33Dn8btuaqdaSqdGKqd6qwNEHuXND2vO7uHMxuXWl4M7zfhDb+in+iXceYLRFcQnVC79CiLR3ufWPj0h2+gM8xXBsg+5sm8FMzbYAQv3OMb58VDq2UT7xr6sjZxDBkuVQ6QQUVxXFVYUxFWGxOBhVqnB7qyT7pm06k5WO087jtIsk9Ry+dcmFFKK4akzNtkz97kz1loBxtVqJrxwT/50rHVjlliqpGCL4Hq9zXUYBRKO9PCxUL/wGybwh/PKWfPyfB3iG5dhm/bluY12dxZKs8snN0tEN8ol/SPbN/PXQEKlMClC52qhcHVS2htcDZGEJpyfjy0dtO6lXTxDLPSeGRUQzdTozDR5la3cEhgi9zkjGdXHzXGnHYiLk6LXJD6rU0DD4KxSfWBj7KfeheuEPCBY3zrKumeb+eh6MiGY7juDaD9V97WNZTtop7V8pH1lHcrPyP44SmjHVWzLVWqIKdf38piUYXzslJ+2Uk3bic3vyawc0RDINerDN+zE12+p5VRDLPWnXUnHDTJKV6u7nIpbrMorvPZ7uuRYaqhe+hmRcE35+Uz611d0HOAPXYRjXdayeUuCbl6Qdi6Q9y/OPJlBcVaZeN6Z+d6ZK4wDdYsSSnHxIPrZRProh//4xjI5jm/Vh2zyrN54ilnvStgXihm/zS6QeqHJjw5AZ9LB84aB64VOkPcusv050Ohp3AmLZVgP5nm/C4mWc1GJZPvGPuG2hfGqrw/oliq/FNuvLNHg0uP4qcPpF+eh66fBafPFAngoImZpt2XbPs/W7OR0ZkZw74vrp4pZ57m708Ca+zySuwzBXqz8UZ1C98BVCrvDrBGn3b242Zxv25J6Y6PwPXhTEXUvFjTPJ7avaYli8LNusL9usX7DP0vHNS/K+FdK+P3DaBW05LFaG6zCM7TDEqWMYuX3VunqqtH+Vm5uvTP3uhuenw4hozxgdHlC98AU4+aBl7kg3o1Sg+ET+qalM1Rb5q4iQI21fJP4zh2SlaR5gmHpduXbPMYntA3TSUVhw8iFx2wLpwCrtwAFGRLOPDOc6jnDqbIKvnhCW/s9xhKIDLFrKMPQ7JrG9xywOdaheeB1p5xLht3fcWtrkTXz3V7iuY/KdHAfAaha3zBM3ztRG64XRsWybZ7k2zzqfsIQK5O4taecScfsCknlDLYTGImz7IVzXsU5UA8vi1vniX18Qc3bBvTMc3+cdrtNIOjdxB6oX3kQWrcsmi9sWuNOWqfWI4ZnPYcnyjhWSKO5cLK6brh1ToNgErusYtnl/J8oSqmBJOvCnuHEmvnZSLYMR0Vz3V7hHXgCc0aE5yUqzLn9XOrDanb7Zxr0Nz013sYlLUaB64S1IxnXLnCH4yvECW8KoGH7QVLZx73xdEGn/Suvqqdp1ClS5Ed9tLFO/e4hNPdyFEPnkFnHjt/LZ3WoZLF6W7/0223JA/v8n8qktwuLXtQMTPVBsguGlhSiuqocNDi2oXngFnHzQMmdYnlUGHdi2g/k+k/KvuuHrp62/TpDP7VFLUJnq/OMTmAaPetjW4EQ+v8f6x0fadQpUsb7hqc9QpQYOLYk52/rHh9KOxQX2CSOLGYb/wCS287CtIQTVC88jH99kmftigUnDoCGSf/pTtvmTDuXk7m3r71OkfSvUdX5UqjL3xES2US86x3ZAPr3d+seH+Or9QRyEbLP+fL938ydAkg7+aV3yVsFuGixnePpzttVTXjA2FKB64WHEbQusv71TYNQ5VLmRccQP+QPYyMc3CT+/ZfdZ5E181zFct7H55+cUG1iSdi6xrp5Kcu4oBbBICf7J950IcfZN4aex8ultBXbJdRvL95nkeVODH6oXnoMQ66qPxQ3fFdiQ6zic7/uuw1IluZMqLHlLPr7Jdg8R2+YZvtebMDrWG8aGGCQ3S1zzhbj1J1WpmcT2hsFfwRLxedph2br2a3Ht1wUKOttyoGHwl/SImgNULzwEwdZlk8Ut8wpoxvKGpz/LP9yVj20Ulrylrneg2AT+mS+Y6q28YWkIgy8eEJb+D189odzCyGL8gI/Y5v0dmskn/hXmjy5wbsI262d4/hvXR2bDDaoXnoBgYfHr0q5fXbeC0XHGUfMcI7iIgnXF++K2n2yRbxHDdXqRf2w8nYAUElkU18+wrv1GPfPONurFD/7KIS0rTrsgzHrOwX80P0zdzsaR88No07ogqF48NFgS5r0kHfzLdSumRhvDi3NhZPE8j147Kfw4EqeeV25R+bqGIdNRfC1vmRo24NRzwoJX1LCGsHgZwwuzHVxmiZAjzBstH9vguiumWgvj2F+oa4YCM2XKFH/bEMxgWVj0urR/petWbIsBxhHfO2TlkvYuF75/QZ2DcO2HGEf8AIuFsqemz4BFSnItBgJC8IV9ABBguSftWwENkUzlxuoeE2R5tnFvcC8DXz7ioiuScQ1fO8E26k3zFQA6vngosCwsfE3au9x1K67rGL7PpDxboVi2rvhA/Pd75Q6aivKDv2Ib9fKepWGLfGaHMH+0etKfbdzb8PwMh/hA4saZ1pUfuc6EwtTpZBz1E52YUL0oPNal/yvA1xtCvt97XOdR2jJy95bw/Qvy+b3KLVO1ueGFOaF9AMS/kJxMYcHLahwzFJ9oHL3Iwe9e2rtcWPS66zB/TL1uxlHzwnzHhOpFIbGu+kRcP8NVC85gfGGOgzsmTr8ozHpOXbBg2zxrGDSVhnvyOgRb10wT136tDCJgdJxx9EJUsb62iXx2lzB7iOsjamyzvoahM8PZa47qRWGQdiwWlrzlqgXLG1/80SEFBk4+aJk52JYqGbH8gA+5DkO9aSYlD9LBP4WFr9r8bnmT8YXZTP3u2gb40mHLjEGu91n53m9zPV/3qp2BDNWLB0bau1xY8IqL6S40RBrGLmGq5VmNlw6sFha8rMRxgEVLG0fNR1WaeN1WSl5wyhnLrOdseQkg5Pu+y3V5KU+Dayct3zxp03Qd+EGfcB2GedXOgIXqxYOBkw+Zv+rrKpgFZzS+9BNT6xFtmbRzifDLeIAlAAAqVcnwyq+oVCUvW0pxDslOt8wcjC8fBQAACPneb3M9xmkb4ORDlhmDXE1MGM44ZjFTq4N3DQ1IqF48ACQzxfzpo65OnXIG4+jFDgccxQ3fWVd+pFyjCvWMY5fkPw1F8SVEyBHmDJVPb1duuQ7D+IEfa1clcPJBy/RBxHJXrwcYEW0cvza4wqN6hLCMoVA4hFzLrOddiQVijC/McRAL68qPVLFg6nYxvbmaioXfgYZI48tL2dZPK7fi1vnC4nHaqJ+ocmPDmMWAM+j1QHKzhFnPuR+UPGSgeuEeWLZ8/4L93HR+IDIM+dZhN0Rc+7V6/Ixt1Ms4cr7XMwNR3AQxhmence2HKHfSrl+ty9/Vrkkx1VoYX3S1e6p4lD9oCstgh/p3uoV1zTRp11IXDfjHxztsdoh/f2n963Plmm3Uy/DCbLpvGlhAyNTpBHLvKG7jOPkQsNxjatsXnlBsFRgR7SIDNsm4DqzmsFrIoHpRMPLpbdaf3wJAd6GH6zDUIVyCuGWe9Y8PlWubWNBjjgEIhEztjiAn875kHARYZmq0UeuZyo2A1Ywv7NfrAF88gOJrojLVfWFtAED1ogBIdrplxiCgn6qTSWxvGPadNnKkdPBP6+I3FH1hG/YyDKdiEcAoknHvtnKKBJ/bA6NiGE1QP6ZmO3IjCd84q9cBPr2NbdrH4fxrqELXL1yCJeGHEU6zFiugsjUNI/PMcuWkncJPY5XFMyaxPR1ZBAEQ8oOmsi0HKnfW396RDv+trTUMnYkqN9J7muRmWeYMC5OFDKoXrhDXTVcPeuQHRkQbRs7TnjrF108Js4cqTlmoYn3DyHl0zSI4gNDw7DSmdkcAAMCyMH+0NtIy4AzGEXNdbGzhK8esa77wvpX+h+qFLvjSEevfX+tWQ2QYPke7A08yb1hmPKVs2qP4WsbXljscYKcENAxnHDUfJTQFAABREGYOxilJaiUsEW9wGThH3PCdnPSfD8z0L1QvdBAFYcHLikemU7ger+Vx4pSslh9fVLwzYFSM8aUFYTKhDSk4o3HkfBhTEQBALHeFH0dqM2MzCU35/u/rPkuwsOg1dzNpBy1UL5xjXfkhTj2nV8vU6sD3etN+T4gwb7RtFd0QYXx5KYyp4H0bKZ4HFi1lfPkXJR0MTjkj/DBcGxmY6zCUbdZX71ly+6p1WYhHFaf+4E6Qz+6yfN1fL803jI4zTfoHRsWoJeKm2dYV7wMAAISGId/mDzBbaHDyQZyR4qneAABMlcaweFnlmqRvJZLuvk8hgFxRAO57VbMRgC8ODaUAG3yTMvnYRsvsIcoPgOsxjn9svFpFzNnmjzvbTqw5wzjqpxDOKUX1Ih+iYP6oo24kWAiNL/+inYnIZ3ZYZgxS3kJ877e4nm940BZh7ovSwT892KFhxA9s48eUa2lze5Jz2YOdOwdxkC8BIspBU1lgiocR5WFUDVi0ZoDriPjPHOvvUwAAAELjqJ+0J9/l83stX/XTm6vCEvGmyVtCdTYa1sGCnCJu/M5F2Giu/RCtWJCcO8Ki1xSxYOp0dDjpSAEAACwSSxqwpBFwUFMKYUQ5WKw+LNkclmwBo6r5zTwduE4j8aUj0oFVgBDh5zdNlRqqiWCYqs25R18R//7K6YMk47q4+lN+0Cc+NNZ30PWLPOC0C9Z13+jVojLV+X5TNK1l4YfhJOM6AACWLG8YNjtMcyAXBkJyr+KUNfLxydLWLuKmZvLJj0j2GX9bpQFCw+CvFMdNcveWMPdF7UIG3/P1/IlaVcRtC/AlVzGEgxf6+86D9deJuo43iDE897X2zKK49Sc5aaet6vnp+XMmU9zFko4v/iht6y5t74WvrQREd1vKpxgiDMNmKnuo8vm9eTLXIdYw5FvdA6wEC8smuQ4gHKRQvbAjH9vgIrkm98gL2lRD+GayddXHyjXfYxzNReYRSNYJ+fA4aXMHfOlnF5vZPgOVr8v3e0+5tq6Zhq+dtFfFVeO7v6L3IL54QDqwyuv2+RyqF/eRRGHZu3qVsEQ8/9j/7PcEWxe+pkSCRFWa0GULz0Jyr8nHJ0lbO+GUvwtu7WW4DsNscVhlUVj4mjaGONf9FVS+jt6D1hUfEP1jR0EK1Qsb4tZ55JbuZoFh0FRthivxnzmKnzg0FjEOm0Uz2XgDknNZPjhGPvQakHP9aQeEhue+UbbP8dXj1rUal1+GMwz+Sm/Rity5IW6c6RsbfQbVCwCU7N5rdV2/mXpdtZG+ye2r1r9shwW4xydQ1yyvgq+vkrb1IHd1j4f6AFikBN9/inItbvgWXz+tVqEK9djWjtmzVcR/5pDsdG+b50uoXgAAgLhplm5sNd5kGDRVWyD8NkmZiTA12oRtnGhfQnIuSf89Se4c86MNbPP+tneGJAq/vK1dy+T7TIZFSjh/TMh18R4KRqheAJKdLm3+Ua+W6zgClohXb+Xjm2wZelmef+bzcE5d41PELGn30+TOUT+aYHj6c2iMAgDgC/ul3b+p5TCyGNdDNyOJuHMJybjmC/t8AtULIG6YqbcuBYuU4LqNVW+JkCP8YnMN5rq/gkpX8YV9FAXpnrR3qC8cUnWAxeK4x21r3tYVU7Q5Srj2z+v+GCSrGpYxBAh3vSB3b4k7FunVcr3f1jr2iptmk8wUAAAsXpbvMtoX9lG0WDPk/S8C2eyvz+faPWfz4Mq5I2oDXjAc12ei3lPS3hU4/aIPzPMB4a4X4uYfbQny8oHiqnFtn1VvSXa6uGmWcs0/+YF2u4TiM8jdJPnU1ILbeQmG4wfanG7E7Yu0QfrYBj217jl5wLK4aY4PrPMBYa0XRMiRti/Qq+V6vq4NtCf+/RUQcgEATLUWbKNePjCP4hR8aRHJ2OevT2dqtmUb9gIAACxbV2uUC0K+l+5RQ2nPMhdRHYOIsNYLafsiknPHaRWKq8o2eVy9xannxB2LAQAAIvUNQ/EX8on39aIN+AC+37tKmEX5yDr57C61nKndkana3PkzokXcorumHkSEsV5gSdw8V6+S6zpG64cjrpmmHDdiG/VC5Wr7wjyKPiTrJL6xzl+fDmMqsC0HKdfimmnaKk7fQ1zatiAEom+Fr15IR9Yri5f5gSXLa2Pe4BtnpYN/AQAAy3FPTPCNeRTX4Iu6Wu8D+J6vA84IAJDP7lLzsAIAmDqdUIV6Th8huVnaXdggJYz1YttPelVc51HaJADi+hnK6Jdt+RQqVdkXxlEKgmQeIVknC27nHWCxMtwjLyjX4voZ2iqu6xi9p6RtC4L90GqY6gVOu6CdeWqBEdFsq0HaltL+lQAAgFi++8u+MY/iDvjqcj9+OtftZWiIBADISTtlTQI0tlFPvSMCOPWcfMFvK7UeIUz1Qtq+UE/p2dZPK78DBXHjTNvKRdMnYMnyPrKP4gYkdaMfPx1GFmPbDlau85wrQ6w69MiPtF3X2ScoCMt4fJJV2rPMeRVEas5uAAC5e0va+zsAAEDIdXnJF7Z5G80iLizbG1ozXLQtACwA8R6R7gExG1gziJDm44gVxJxCcq/BiHK+/FAtXMcR4pZ5QBblYxtx2gU1GQ3bcpB11VQgWvI/Ih36i3/yAxhV0reWeoxw1Avp2Ea9bVSmfjcl/YSt5c4lSrIyJrFDiGyLaM67MDXf1G/34BCZCDdBzhVyeze+uYNkHvFBmCxyaxesMMDbn6IHLBHPNusr7f4NECxtna9utMOIaLZ5P2nnEifPSFZp3wqu04s+NdRzhON8xDZkcEae86ZYVl3FXYwwKTYgA41xsGQzVP1VtvXvXLeDTM03Aa9zcNNDkMxDXu2/QLjOo5QLac9yxZ3PVt5+qN4j0v4/vG6W1wg7vSC5WfLJzU6rYEwFpkYb9VY6uk6J5YtiE5g6nXxkX8jARaNqY7nOO5nE/wHG5KUPITnJXurZTVB8IlO9JQCAmLNFzSQXla+jt7GKLx/FNy/5xjyPE3Z6Ie1fqUwx8sM2f1J7Pl3avthW3vppem69kDARqOootv1aGFmx4MaFwH/HVVXYNrZVT4cJCNv6aecPECLrj3ADnLDTC1k//Y/WAZxkXJfP7AAAAM6g+8VT3ANGVmZbLYfGMh7vmVjS/BytDwC2US81Wh++dFhbrj1/pEU6sNpHxnma8NILkpOpxN3MDypfVzmqrCDt+8Pmo1W/O4ws7iP7QhhjaabJbAA9vr5OiPAQWzwegeXZlrY1V+3SGIyKYRLbOX0Cp57TJn8PIsJLL+SjG7RZZ7SwLZ7U3kq7lt4vH+h1s8IDWLwBShjh+X5l/8fgZlvc14v9K7UBxF1k0pWP+u38y8MQZnpxfJPzCgjZhj3VO3zpiBLgBBYtzdRy/oqgFAKm2hjARXm4U8lv4XNUUNmaKD4RAEDuZcin7ClsmHpdlWMm+ZFO/Osj4zxKOOmF1Szp7Iyg8nW1QTqlg7bppYspKKUwsEVQXDcP9+nv9QsFexZrzdoENBZhEts7bY8vHgzG0OFhpBfy2V16obRYTboAAIB8xDZWZJr28bpZYQaM7ejpLgPiBBfTtI+yiSYfXa9NqcnW1dmJJ1g+ucU3tnmQcNILZb/DGUx9+0sPXz+lbI/D4mWYKk18YFhYAaOdeyUUHq5owW28DypVScl1Rix35bM71XKmThe9zXj5xD8+Ms5zhJNenP3PaTksEY/K2bPayUfWKxcuvmlKoYGmsgDppCkuHGwRT/b2EDD1bG8d+Zj9IBwsXkb769Iin9sTdMfbw0UvSG4WvnbKaRVTq0MeNy0lvQgAbP3uvrAs3IAI8J5MZA85T/b2MKizWunYRq0QMHU7O21Psm/itPO+sMxzhIte4LP/6e2kan3Ayb0MfOU4AAAYIpgarX1jW7gBGc+FVocMYD294VJYUPk6MDoWAEAyrmtDhzM12+o9Iic5H/MGLOGiF/LZ3c4rIGSqt7I3O7dbcdNiqrUAnEeHzRQVz51bhUWqAMQV3M43QMTUtq3mykmaJYwqTfR+S/iczs8yUAkbvbh4wGk5Kl1FeSco4PtfM1O9jdP2lIeHSB7bAYVFa3mqK4+gDiXyDBxYnqnU0Gl7+XyQhdsKD72QRXzd+eIF0gwugLLnCgAAgMlbTvEYshlYMz3VGSxW11NdeQR1DovP7dJmPEA6Pydy5wa5k+oLyzxEWOgFTjkDRMFplXbHlNzLUKad0FgEVXC+pk15SEjuFQ96TMDowPqaYHQsKlUJAEBy7uBU+1omU7WF3iP46gkfGOYpwkMvLh3Rq0IVG9ibJR9SlrVRQjPq1uklSIbnItxw0bB4wDnIqEMJfH6PvbBifb29eXxZ98cZgISFXshXjjkth8YiqEw19Vb95pDObJPy8JB0jzk1orguASjrTEJT5UK+fFQthBHRKMZ5BBBZ2Y8LEsJCL/SGfDA+URv/Vr6vF0ylBk7bUx4WawZO31ZwM/dAFQLx6LAaVgtr9AIAoOe1ha/R+UiAQVKde8UwFetrb9UvWDtJoXgQ+cJcgJ0vJD0oMLoOLNHUI115FlSmurJ7ilOStKtmqILzpVmScZ3kZvnIuIcm9PWCZKURy12nVahszTzNsm8CAGCxMrBoKR8ZF06Qu0n4osdyDjM1xnmqKw/DcLbAS7KIb9iD4iinS5xC0v0chdR9Ql8vcNoFvSoYV9Xe7H68I1Q+JPIGBBrCTXnfcIDFglu6AYztBGMDNwIzKm8bSuBr9oyNSPNjcwCn6/5EA43Q1wui76KPSieo16onP4qrptOcUkiIOUXa/SzJveqZ7hgTU3uSZ7ryDqhsDeVC+66CJcoB1rkrKg6e8UXALS97HJx20Wk5jIjWzjvI/a8WlaYZlT0MNJVlWiwiWcfJnRMk6zjJPAweIq8a0+ALGBnQ3xEqXUW5IOmXNKUMKlnB6WiXpDv/iQYgoa8X5Lbz1xosVUl7q36RUDPooHgKaIyFxlgQ2xkAAIhMsk+R9B345jaSefCBsiiiik+jsr28ZaWHgPf1AucVAliqEnCmF0GUjiT09QLr+NuivEm01a9WfTlQvAVkYHRdGF0XVRsNpBzEfOPGAAAgAElEQVR8ezdJ24xvrC9w0IHiujF1P/SNjQ8DKlkBIAZgmdy6DAhRPbVQ6cpOj0iTOzd8ad7DEPp6QbKc6wUspkmHQYjtO2M5WCzOJ3b5B3HTHNmzyS8YHhhs59MhZ4ARxUCREjCiGIwqCYuXRcXj1VrnsJEotjOI7czU/YDc2o1T/sapG5weMEFx3ZjG3wLIeNJ4L8FysEQ8uXWFCDnk3m0lOwkAABYr67Q5yboJCNa6AgUsoa4XBJMs51FVtcdSyb0MJeYijI4L7Zha+OIBXHArTwIjomHpyii2GoqrisrWQBXr51FqezsWlmrLlGrL1PsIp27EyQvIbfvZTVRhAFPvEy+kL/EWqFgZ+dYVAADJSrPrRXRp562xRO5lBkXS9qD5AgoHuZehzQehJY9e3B+DhPbgwi+Q3Cxy6Yj2CA+MjkWVGjKJ7ZnaHVCpfCuXkEVleqAyPUjOJXzlN3LtD5Q4HpXr61OjHxp1KZ1kpwNQ+36hjl4AQLLTqV74Hxch2/PoxZ005QJpCilegmSlyUfXy0fXAwBQqUpMo15s48fzuz/CyEpM4nhQ840gGlaoqL8u7fBWd3wBAMm+CeITvW7WQxN838QDQczZelV5NlPvywqkeuFb8M1LeMN34obvUGwC22IA22IALJ53thKEYgHyjC9uagr19eLeba/b5AmCYInloTDf06uBRnvcR7us0FSpfgKnXbCunpr7ThPLrOdw8kF/m/OwqGsW5J590wdG6Icmtuj+UAOKENcLIugn1zRq4tDfP2ACDZFetojiEizLxzaaP+tp+eZJ+cJ+f1vzENz/IeX5BSJGL5Cnqx9qIBHieuFCtqHRLg3EknO/MFCSWYQ58pkdli96W+YMdXH8J5Cxv3iEXOflDggBkdWxQEJcL4igoxecQRtqhaiyQvUikJCPrDO/3966bFKw/DnZcTq+0JQ7QKzB8Q8Mcb3QS5jqKPP3vy06Hwk4sCRu/tH82aPas56BD1S91Ky5zssdoPORgADreCehvG6C8v0jDIEX340CAMApSebPeoibPRY+w+uo6xRS3vhADO+0OdH7oQYYoa4XRO9rgM6boVB27gxuRMG6bJLw01g9B7wgIcgSpjoQ4nqh++U4+OrbZYXqRUAj7f3d8v1wvewQAcQDJlKGQXIKIcT1Qnd84fD1qF9uMJz5CXPkYxss0weQIHFYyDeSpeOLYMRBR9RpCHGek5kSUMjn9wo/vvhAgTN8TZDrgh4hrheQMzqvEC15bnmT8l8SdPt24Yp8YrPw85v+tkIfdQWdyRuDT0/j6HwkIOCdb1856AJUm+nsv1ICEGnXr9Lu3/xthXPsjj95d+hVz0BHgmQjP8T1At4fODgii3mW2Xnnu+WUAEf4dUKAOoDeX15x9BjWcSAMFsefUHc30NMLAIjVDE2cQzMS6uMLFF8LFo3xfL+ySHKzSW4WyM3Sy/biFYRc69L/GV9b7rtPdA/VrdPBQUv3nAjVi0BAe0jEEXM2MBW1Nbt/LFV7mjAk4Xq9zjb0crxcgnF6Mr5yDF85ji8fls/vA9iLq8jymR3Swb/Yxr299xGFgOTeP/F8/zcGAACSVQnjlp9gObgU4noBIkvo1ZDsW7BEOeVafeWSu7d8YVVoAxGKTUCxCaBpHwAAyb4p7V8pbV/ovYmDdcX7bP1ugHXuOukXyF1b2Av1YDsAgNzVD3KhlZUAJtTXL/TH3uo3CrTRCrKpXngYWLQU1+lF05SdhhE/oLw5HDwFybgm/veLN3ouNGqYnLx6cVOnuasfakAR6noR5UIv7GIPi9hCJ5KcEJ+P+A0I2caPmSZtZts8443uxXXT9Yb6fkEdqObRC/23EYwKjpS9oa4Xxii9Yap26qGG+SUZ131hVthiiDA8+yU/6BPH834PDblzQzryt2f7fBjI/aw32pidLma7Ll5sAUWI6wXQ/yZIxjV7m8jiSqw0cudGEJxNCHK4DsP4/u97vFtp608e77PQkFuXAQAAQlTSnhaL3Elx3prlIF2/CBAc8pip4JuXtbdQ+V4JwTr5EykehOs4nG3m4RQB8oV95NYVz/ZZOIjlnrLRBouU1KZrcvjJqaDi8dS/M1CAMRWdltveAPdRZcWhnOIl+AEfwchinuyREOnQGk92WFjUlL0Ovz29nxbUeaUFIKGvF7rji4yrWr8A9avF6cm+MCvsgUVKsB1e8Gyf8vGNnu2wcJCbtp+QY45eneGP3istAAl9vdAVb0nU5rlFZWsqFzjltA+sogAAuHbPeTaAgJx8KBCWn/B1208Ila1hLxUFvbzKqGR5H1jlEcJBL3TFW/1eAQDofnYpbSHFq8DoWKZKE0/2KFnl5EOe7LBQ4JQk5QKVsesFTjuv5+dKxxcBBCpbU28xCV87ZW9WprqyyUdSkkI1eEEAwiS282yH+MpRz3ZYGBtu2PQCxte0F6ac0Wuvjm0Dn9DXC2gqCouXdVqVZyjBGRTvQyLkELpF4itQpUae7VB9t/sN0YLTLgIAAGfUbqbqGsZyKLaKTyzzAKGvF8BhGqlBfQ/YmpW3pfyVgz8fX7CA4qp6tkOcdt6zHT4o8pVjSqgEpkI9rVuanl6gUpUdY+oEMOGhF/G1nJbjG2e154tRQlNb+YV9vjCLAgAsXsazvp4kU8cnylfgiweUC1SlcZ5ynZcQCoa07CrhoRd680Ms48vH1DumUkPlQr5Ixxe+guE8e5SbZKf7d/kJXzqiXKCKDdRCknFdzxlc72UWmISHXugvwuOL9qS+qHxdJc0Mvn46+BLwBS86MRMLiST6N3exfN42OGUq25dmXMxwHYYhAU546EVMRVjEeSAM+dJh+w3LMco7QRbl83t8YhoFAMbTQVgcgjn7EJx2gWSlAgBgiXJQ41WBtT8zLRChCvV8Y5tHCAu9ABCiys5VHF88oB2+qtt78qmtPrCLAoAXYqb6z2VLPrNDuWBqts1Tfm630/YoNiFYTpophIdeAMDojPpI9k3txjiq0Ua5kM/s9IVZFEI8n3nI4wMWt8Fnd9lMqN5SLSS5WfjKcaftg2syAsJHL1CVpnpV8pnt6jVTuZGy/IZTTqshkijeg2Slenw4AI1Rnu3QXbAkn96mXDL3XzwAAHx+j55nJ1OtlS8M8xzhohdMQlO9WOHy6e2adhyq3goAAAiRT/zrE9PCGpx+ycM9IsZFUHivIp/fR3KzAACoXG2ti6Cc9J/eI0yN1r6wzHOEi14AltcOEbXIZ3cByWpv2OBR5UI6HBCHo0MbfFb3b6lwQEOkv2JJyEfXKxfM/Z+QrfyY81OzKDYBloj3ulkeJWz0AgCmRlvnFVazdjmKqdtF8SCSz+yku6reRvL0urLeRpgPkI//o1ywdTqrhTjtAr55yWl7VM35CyyQCSe9qNVer0o6vFa9hlExjOLoKVqkU5t9YFjYgi8fVb0hPQWMq+bZDt0EXz2O0y8CAGCxMqhifbVcPrZB7xGmVgcfGOZZwkgvUNlEGB3ntEo+sk6bsZ1p0MNWfuBPX1gWroibZnu8T48fSHETaf8q5YJt3Fs7IdKbjACWc/ECC1jCSC8AhGyjnk5rSHa6fG6vess27q1MSaRjG4g52+kjlIdEPr1NOrDK490iv4wvCFH/LWyTJ+zFd26o7p4OMDXa+m0f5yEIJ70AgKnXTa9KPqKZkhQrY1u4FgX5IB1ieB6SmSLMH+ONnv1yfEs+v1fJRAFjKqL7p5AAAFLecasWpm5np+UBTpjpRfWWejFmpQOrAJbUW7bFk7byPQGXyzfYIbcum794zBupJ2FUjHbtwGdIO5coF2zTPnkmIwdWO38AQrZeVx8Y5nHCSy8AwzE63xPJvimf2mZv2KAHNEQCAOQL+7yX+DMMwZcOm7/qq03+4kGYxPaeDQjqDiQ3Szr0FwAAQMS1fspenpki6wRGQJUbq7l7g4sw0wsA2EaP6VVphxLQEMkoKb8JkbYt8L5dYYDVbF091TztMe8lkWPqdPJSzy6Q9ixXTrgxNdtoI3FKe5brnaxnm/bxkXGeJuz0gqndARZ1nqtSOrpeu7rJtR9qK9/9m3+PSAc75NYV6+pPc99p4t0sp7zJD3pBiLR9oXLJtnlWU47F/5Y4fwQxrPIqCkLCTi8AYnXVXbRIe1fYG1asjyo3AgAQc7a893ffWBciEEIyrstndoh/f2n+vFfu5Obium+0Ca69AdtigJLU0pfIp7bg1HMAABgdy9a3u3XKp7frJVtjarSGRUs7rQp8/HaSz4+wzZ8U//3BaZW07Seu/RB1yYrrMFRIPgQAEDf/yLYd7Pu5sceRti/WLtN4EquFCDlAuEey0vHNZF8fKoeI6/SiTz8RAACAuPlH5YJrPwSw9jCc0s6f9R5hWw7yulleIxz1AlWoi8rWdBrfHd84K5/bzVS3nRpkGz9uXfEhyU7HqefkoxsczgUEI+oByhCDqdMJxSb4+ENx6jn51BYAAOCMbNvn1HKSmSIdWe/0ERhZjG3o3AkoKAj6F2bhYNsP0atS3xgAAMDyXOeRyqV1w7deNopSWBiO7zvZ9x8rrpuurGiyzfvDqJL28n++1+7Na2FbPaXEfAxSwlUvmvVVtkvzIx/bqM0/wrUfqvwUcPKhUH05BztcpxGoTHUffyhOOSPt+wMAAFiO7/GaWk4s96Tdv+o9FdSTERC2egFNRZlm/ZzXYUncNMt+a4hg2z2vXIrrZ3jfNMqDAUuU43u95fvPFTfOUnw32caPa50ppF1LlSgY+WGqt9RLhRMshKleAAC4TiP0AiWIO5eQO6n2lh1HKEG35KT/aFzPwAJCw6BPfB8gB6ddsA0uEMM9ah9cANEirtedt3KdX/K+ad4lfPUCxVVjqrVwXidZxa3z1DsYWZztPEq5tq6eSrOrBg58zzf0HHa9irhqqrJCwTZ5XHsiVtqznGSnO30ExlTwizuZZwlfvQAAcF3H6lVJ2xaSnDv2lp1HKoFY8OWj0uG/fWEcpSCYOh25nq/7/nNx8iHpyN8AAMByXO+3NRWSi0VxrsMwz2Zy8wthrRdM7Y6ofB2nVcScLWq+e2iM4rq9rFyLKz/Wxu+j+AVUvo5h6Ey/eMRYV36kjDG5ds8rOboVpL0r9Hy0oKko2/pp35jnVcJaLwCEnGZl2wFxy4/akw5ch6HKsha+mSz+M8cX5lF0QJUaGl//A0YW9/1HSwf/lM/uAsor5NFx9gqr2bpqqt5TXOeRwZVnRI/w1gsA2AY9dZesRcG6Zpr9ljPy/d6z1aybTjJveN86ihNQ+brGsUv88udHhBzr77bfAPfoq3l8Lv79Qclslh8YEc12HOEL+7xPuOsFgJDrohu4Rdr7u/YwO9u4txJHhwg51hXv+8I8Sl7YVoNM4//2V1Bfcc2XynsCxSZwnUaq5STnTp49+Lyw7Z4PjcEFoHoBAGCb99MN4iaL1uXvagv4AR8BxAIApAOraIISn8Jwhme+MDz3DWB5v3w+vnZS3DxXueYHfaI9LSKu/UrP5wIai3D3N9dCAKoXACCG7/OOXqV84l81rwQAAMUnco+8oFwLS94ilrteN48CACpT3Tjud7btYL9ZgCVh4WtAFgEAbKNeTKI9VC++elzcMk/vOa77K35MceBxqF4AAABTv7teQmYAgLD8Pe1pS/6JCah0FQAAyUyx/j7FB+aFM9AQyQ/40DR5M1O1uR/NsK79Bl89DgCAUTH805/bKwgWlryll+4QFi/rl1Oz3oPqhQ2+1xt6VeTWZXGL5hAaZ+Sfmqr4hkr/LXWR7Y7yUCCWbTnQNHkL13GEMgf0FzjljHoUgO87WTtekPatwJeO6D3IP/oq4Ixet8+HUL2wwdTu6CJks3XNNHwz2d44sb0tmBLBwvwx5F6GDywMIxiObdbX9N42w/PTYUwFPxtjNQs/jFA8bpj63dmWA9UakpNpXfGB3nMorirb5hlfWOhDqF7YMTz1me5JBKtZmD9WGxue7zNJSX5JslKtv07wjYUhD6pQ1/DMFxHTThqGzfJ9PAunWFd+ZIugFRFtGPhxnqpfJ5Lsm84fg5B/9kv/Dou8AdULO7BEPNdltF4tTj4oagL/wohow9CZtrRGB1ZLe5b5wMJQBZWqzHUdbRr/t2niJrbt4MDZfZSP/yNuna9c809/pk2PLB9dL+1fqfcg26y/fxdcvESo6d9DwncdI+36Rc8XS1z9KVuvm/qjYaq14Hu+Yf3rcwCAsOQtVDYRVajrO1uDHFi8DFO5MarcmKn9CCpb09/mOAHfTBZ+GmOLiNPmmTyJy4QcYZluhB5oLOJixy2ooXqRF0OEYeAnljlDnVYSc7awYKxx3Ar12AL36CvSiX9x8kEgCsK8UcYJ64MxyZ0PgMYisFRlVKY6KlsTlamGKtSHxcv42yiXiILw4yjFqwKVqsT3n6KttC6doA2q5ADX+21YzHmm3mAHEno6Ox/C/DHSvhV6tXzvt7XHIknGdfMnXZQlT6ZBD+PIeXphNQqBfHo7uXXZU735DoiAqShAEEbFwKKlUbEyvg9R8ZAIS96SdiwGAADeZHp7DSpXW62Sdi0VFo3Te5Cp2tz4xsoQCA3tFKoXTiA5meYpbXUT9iHW9NZqrb+GfHq75dunAJYBZzD9bx2Kr+UjQyleQ9q/Ulg0DogWw9Bv2eZPquU49bx5alcg5Dp/jDOYJm0OkJVab8BMmTLF3zYEHJA3wagY+eg659UE4/N72ZYD4X3HZFSqIkQsTj1nenkpqlDPd4ZSvAaKT2TrdETF4rhHhttLJatl5rPktm4yR67HOLZRL1/Y5yfo+EIXy8zB8vFNerVsw16GF+fapx6EkHsZ2gOLlNBDWPK2tGORXi2Kr2WasEF7riT0CM1ZlkcwDP1Ou3/mgHR4jbjuG/s9hFQsQhvpv19ciAU0FjGMmhfaYgGoXrgARkQbnp3mYvHSuuZLOWmnL02i+AtiznZ9VojrMwmVquwrc/wG1QtXMLUe4fQjnUBDhHJgkRLyQFNRw0sL9YaQTL1u3P2kE6ENXb8oCMlq/rwnvnLcoRjFJhheWqiNDU0JfEh2Or56AqddBKIFRhSFJcqhhGZKsgi3Hs+4bpkzxOHHAEvEmyZuCqVD6y6gelEwJDPF/EkXbXpxpsGjhqHf6WVIowQchEhH/pZ2LJHPbHc8e45YpmZb/rG3UaWGbnUlWYVfJ0g7l9hueZNp/N/hs4NO9cIt5KSdlukDAZYBhFzPN/ieb3jQKQtYzdKxDUxiBxhZzGN9Uu6Db5y1Lv2fEqRXFwjZFk+6OnCYF2nHYuHXiUAWDcNmsc36esbQYIDqhbtY//pC3PCt4enP2VYeSoGJJfnEZmnvcunYRiAKKL6W8fU/qGR4Fvn4JsuPI3Xdq/KC4msZRs3Xpghw1fOprfKZ7XzfdwtuGkJQvXAbgnFKEopPfPie8NXj0u7fpIN/kaw0bTmq3Mj46jL3p9MU10i7lgo/v6VNlQ6NRZjE9rBkeYAlnJ4sn9vtICUwpqJp/Fq6Na4H1QvfQczZ8sG/pD3L5Av79JIqGobP0Z6DpBQaae/vwoJX1JAlMLIY1+strvXTeWYcVrO4ea64bjoRctSy0D4A8pBQvfA+kigdXS/tXCwn/acX6FEBRsVETD3kr/jXoYR8ZJ3lhxHqyAKVr2sc+zOMjnXaGN84K8wegtMvqiWGITPYFgN8YWiwQfXCi+CUM9KeZdLeFQ7zDgVoKso0eZzcvSUfsR1U4bq8pKZEohQa+fR2y8xn1ZyVqHJj46u/uZ7lkYxr5s96qQmHYPGyER/vD4F0px6Hxr/wPOT2VXH3b/KBVTj1vJNqhmMbdGfbDGZqtAYY5068f84VMWqmAkqhkS/st8wZYheL+FrGsUsKXBKCJcoZhky3TLfF5iSZKfL5fUz1lt61NQiheuExiJAj7/tD2rNcvnhAG+lTBZWvy7V7jmnUW90EkQ6tIdnpyjVT+xElPyul0OCrx4WZz6pLmKh0FeOrv7q55cQktmcS28mntyu38rENVC/yQ/XCA+BLh6U9y6X9K0lOptMGTK1HuB6vMQnNHLw2pO0L1Wuu7XPetTLUwWkXLDMGafOM8c98DouWdr8HtskTql5o82BSVOgi8MNiXT3V/Omj4tb5emIBAMDXT8PoWAexwClJqhMRLFGOqdPJu4aGOrBoKRSbxz1fmD8W3zjrfg+oYn31mtxxnjw5zKF68bDwPd90SFwCo2K4Li8ZX1+pev6QrFTL1/1JxnVtM2nbT+o11+45urpWOEhuFrCaAQDQVNT4yq9MrQ72qqxUy1d98p/90QNGaGYudJfKGVQvHhqWM476ianbBTAc27i38dVlEZ8f4/u9x1RvaXxDIxkZ18zTHlODxJKcO+Lu32w9cAZ/ZgYNXrAkbp5rntTM+veXthLeZBy9iGnwqNqE3L1t+aa/fGG/O/2Re/YjQjD6ASYy4QPVC0/AcMbh30d8tNcwYi6T2E519YHFyhhe/kXd9icZ1y0zn1UiA0v7VihvRQAA27AXjCzuF8ODF/nUVvMHHazLJpPcLPHf7+3LDSxvfHGu9kwHyc2yzBgon9lRcJ+aMybaAL8UFep/4XXwzWTLtCdUFwxUtqZx3ArL573UBIum8WtR5UauO5Ev7JdPbVVihaNSlZg6nVHF+p488xY8kOx068qPpT3LtD6ybJMnDMPnaBph4ZfxtgDfCpzBOGIuU6+rfr/Y/GFHnHJGuTNN3ESzyeSH6oUvwFePW77ury7dw5gK5NYV5RqVr2N65x8Xz5LsdOHHUfmPV6LydfjHxjN1u3jD4ABFFKwbvhU3zlSHZgAAgBiuw1Cu55uO+6aEWFe8L/6jERGGMwz9jm3yuNO+pT3LhAWv2LosW8M0eWt4yrFrqF74CHztpOXrfiTnjkO54bmv2VZP6T1FcrMsnz6qdVV2gG31FD/wo3CIxCEd/Etc9TG+eUlbyCQ04wd+7GIgYP3rC1Fd3QAAIMbw7LT8/8NxyhnL572I5Z5yaxw1n2nQw1OWhxJUL3wHTj5omT5Q/VECAGBksYhPjwDOqPeI68xJCii+lvG1ZTAqxmOGBhg4Jcm6bJLDAgQsWprvO4lt/mSBowBx0yzrHx/aJy8Q8gM+0rrSkozr5i96k8wU5Zap1sL4+ko6uHAK1QufIp/aapn1nOqtzHUczg/4SK8xTj1vfr+d6irKNurFPT4BRsXgayfFjd/JJzarLVFcNeObq0MvJByx3BX//EzctjBPnFSW57q/wncZDQwRbvYj7VshLHhVe7Cd6zaW7zMJKGIx7TF1qxvGVDRNWE+Xn/Wg/p0+BZWvY7+BiNXmwsmHfPhv+3HsYmUML8wGDAcAYKq3Yqq3kv77RVg6HkgiAACnnrMuedsw8kcvmu5jCJH2Lreu/NjhqB5TpxM/4ENUusoDdcY26wcIERa+pkqGuOE7ABHXdYxl1nN2sTBEGl+cS8XCBVQvfIq0c4k6uGAS27sO5YQ1mVOZmm0VsVBhWz8NDJHCvJcUTZEOr2EOrQmZ5FriP3OsK97XlsDiZfh+77GNH3cxUyC5WTAi2mkV27w/4E3CvFGKwgIAxPUzpF1LSfZNWwvOYBi9kKancw31v/Ahsihu1fh0ti8gAr12foGTD+bPXcA2eZzrPEq9Fdd+rReGJ+hgWwzQ/uWjUpUjpvzHNnlCTyyIOdu6/D3zO00dnGjz9Nmwp/HV5dAYZX9KIxbG0YuZGm08Y33oQvXCd0iH19ojLJQsX+BWKFPbfqIEp10Q103P34Z/bLx6qhUaIhVnsBAARpXk+7yj3uKbyboBe7Esbp5rfqep+O/3xJwtLJvsolumWgvjuN8dZxyINQ7/nkls5wG7Qx263uk7zJ/3whcPKNf84xO4R199oEcAYo2j5jH1ujm0kXYukU9v4zqPyu/0JV/YL21bIJ/eqiRDgEVLoYr12YY9mcaP+XkLVhJx2nlyM5nIMjRGovJ1YdFSjm2wZP64C75+WrlDcdVMkzc7TMrwpSPCbxNx8iFtofGNVUy1Fi4+HKckWaYPsK2MQGQYMoNt3v+h/0lhAdULH0Fys3InNLSFZmC4iE8O6IWH04JvnDV/0hWIFts9yxvHLGYS2xf8eVgWfhkv7fzZaSU0FmE7vch3fxVwBrf/BQ8NluTkQ/Lp7fLpbTj5sHa3AkDIVG/N9RjH1GitfUK+sN8y7TF1ksX3ncx1HaNck4zrwtL/OSTEhqaiXO+3uA5DASpgYQ6nXbB8059k3uCf+pRrP+Sh/23hAtUL3yFf2G+ZMRAIuWyzvoZhs9x96vgmy5xh9sULQ4Rp3IoCk+tY//hA3FjAR6DSVQwjvkflvev1jFPPy6e3yae347P/aX1PnAAh33cy12W0tkz4aay093dbvbGIacpOWKSEdf0MRy9PiLhHhnE933B/dwOnXZBP/Mt1evGB/jlhDtULnyKf3maZOdg47ncmodkDPHV0veX74eoLGUYWM761xkUqRpySZP7wEU1o7OJMjdaAj8A3kvCV49rYX9AQaRg+x2Elhdy6AoDtVwFLlCvcQXs5aae093f59DaSeeMBHoPQOGYJU6ej3ZjMlNwpbdSQWUztjiQzRT3loYDiE/mBHzPVWxXCTsoDQfXC18int+lNKPD1U9LeFahUpfzH26Udi4Rfxqsjc1S6inH8Wr1Ic9bf3hG3zLO1jKtqfGO1mlCDZN4QN80St8yzqwZnML2xSjtgyRldTtWmiK+S9HYobf+cs7vUxOVM5Ub8U58q1+L6GdZVnzh/BiJUvg4qVxuVLA8AkE78i5MPqpUoNsE0Zad2H0TcONP6x4fOe4qK4fu9646XJ8UjUP8LX+NULMi9DMtXfZXXJoypwLZ5xiH/Bdv2OZKbbV1pcwbF6Retv03Um9TI5/ep11znl7TZd2DxMvyAD5l6XYUfhtvOv4mC8OtE0/i1tj85gvMk+CkobAy5extfOWZrrPFJZ2q2A83ZmmwAAAm/SURBVCCPXsAS5Zha7ZnE9kyNNtqtYq7n6+K/31uX2wKj47QL8vm92gVL7pHh0o7FDidHAERs66f5xye4zi0kH1kHy9Z4UP8uih50PzUggEVKqHl0yK0r0v5V+dtw3caymhif0v6VOCXJaW/kjn0K4HRVlanZ1vjmn8rwhKna3Dh6of39LEt5mxb0RpGtmsb2zQtUoa52+GMYNjPikwOGZ79kGz+W32+d6zRSu7kjn9met9rAP/mBtgDFVTP9b53h2WkuxAKnnrNMH2iZM9TqcoeV8kBQvQgUuEeGqdfWPz50ujTI938PFi9ruyFEOvSX0660Mwhp16/Og5WXrWEY/gPX6UXjuN+1QXGJ1isMwgI3GrT6ArWbnYjRuj/hG+dcd8Nq9onJzcsOtUy9rtqNZHL3FoypoNcVuXtb+Oll8/vt5dPbAADyiX/lI2tdfzrFTaheBAps076oUgPlmty5IfwwQvVcVoGGSFYbbC492WlXTO1H1Gvp8Bph/pj8XQEAmMR2/JMfOHg05GmJ2ILXBSTN+ILN05XWA8pxyJAfl6skAAC+37tq/yQnU/z7KyeNCJZ2LTV/2EHau1yrktLJLQV8OsU9qF4EDIgxDP5a/ZOQT22xzH5e8bPSgtPssTCgKQo4g+s6Ns8QY/9K89Su8vk9bpmR5yQop9/OBtHOXxgHvbCv1ODLR7WR/vODz9nNc7r1g2ITuC5j1Ftxyzx86Yi2gXxqq/n9dsKicXYvbwBQ2RrGcb8bnvmiwH8IxR2oXgQQKD7R8PTn6itdPrnZ/FEnae/v6gKktG+FfHqr2l7PhRkWizOMnK897o2vn7Z82UdYNI6YswswQqMXkClYL/LoS97FDhhTEcZUvG+BLCft1O3j+Cbp4J/2buo7+rAq8D1es6d0IlhYPlnZMCK3r1pmDrbMGKRNKAdNRfkBH5re+YeeCvEgdH8ksGBbPUWsZuuvE5VbkpUq/DTW+sdHqHQlcidVu0eAYhPy+4arMDVam95YLfw40h6bixBp11L5zA7DkG9dZe7S//svsH3+zRQmsZ0aRBOf2QEa9nRoQHKzxE2zxQ3f2cN8NO+P4ms5/yzOyPd7V5hr87DCF/ZLOxfjrPT8vltsm2f43m89ULIiijtQvQg4uA7DUGxVYcHLaugHkpUqZ+VNn8Nyhhdmu/akQhXqmiZvtv75ufjv92peeJJxzfJ1P67n63zPN5yuTeSdX7iRg8OlvjCJ7VW9UFOHAULwtZPy6e3yme3y2V3aFRBUqQH/9GcuPo1t/Ji0faGc9J9yKyx526EBqtLEMPBjbeYhigehehGIMIntTBPWW3+bJB1Z6+SIOm8yDpvlVqQGzsj3e5dt3FtY/Lp6cAsQLK6ZBoRcvt+7Th7Rrl+6Mb5wrS9MjTYAImXsgNMviptm48tH5aQd+ddlAABMgx6GITMKPAjHD/zE/HHn/Kf7YdFSfN/J1HfLq9D1iwAFFitjGDnPNHEj13U0Kl9XWXqExcuybQdHTNmhTclTIKhSQ9PEjfzj/9OuX4qbZsmnnO0ayA/grAVAAeMLGFlMG4zXuuJ96cAqp2IBIGSqtyowkToAAJWtwbXLm2uW5bhuY01TdrItBlCx8Cp0fBHQoPJ1ebfPg+GUM8LiN/gnJjhZ4WM47tHXUMUGwvcvECFHKRM3zWZqPeLYUn6w8UWe/Vdn+sIktseXjzp9FBoiiSjYVnMJsS6bhFNOGwZ9WuC+DNf7bVV3mNod+QEfotiEgk2lPDR0fBESyKL495fmT7rg5IPC/DF6uYKZWh34AfaDGLIaWUNDHn+tB9wfgc70RZvQFAAAWJ5JbMf3mWSauDHim3PGN1ZqvcilnUvMn/VwESPL9kER0fzjE2BMBePoRcaXf6Fi4TOoXoQCxHJP3Pmz8qonWWmWr/vpSUaegMPOnLjynFvBThxDHT9aFOw3913atTBVmmjLjS8vNb66jOs2FlWoByBiEpoa31ytPd+Brx43T3scXzvp+nPZVk+ZJm12la+M4gWoXoQCMLK4YZh9uwSnXTB/1kPdRNCi/Tt0+lrWriAU7KwBALl7y/6s0/1Lltdu3+J8gxoUm2CcuFH7l08yrpk/fVTavczVByPGncUOimehehEiMNVaGJ76VJUMkpli+eZJ4Yfh8snN6paHfOJf7Rlzpm7n/P3ASPthMJJ5PX9CNge0AqQXMYypqXEMP73NyYcaixhH/cR1G2svkqzCwlesyyapO8GUQICud4YObNvBoEgJYf5ooMwRCJYOrZEOrQGIgcXiQM4ddaUTAAB4E9d+aP5OYLE4WLyMLcgNIfLB1Ww73Tjm+NpJoiY9YDm9LV6mZlv1Wk4+CESLk5RuiOH7TIJFS1lXfKBqhLj5R3In1TBstjue6RQfQMcXIQXbsKdp4iaU0DRPKZZJxvU8YgEA3+89WCLeaSdMdfv2inXddGK56/zDCLb+YU/OxlRpqjdBQPG17EMPUZCTdCJ9A8B1Gml8fQUsUgKWLM+2HWwcvYh/fjoVi8CBxtcKRQiW9iwXty/Sxq2yY4gw9J+iDaXhAL54wPy5Pe8RSmhqHDEXFovL00gShV/elnYttfc6ch6bz91bRZj3krR/pXLNdRnt3FVMNT83C5qKUk+KAITqRSiDU5Lkk5vxxQM4Kw3IIixaiqnWim3er8DQ5JaZg/OE3uZNbMNeTNWmIKI4wBK+ekLav1JNUAwAQJUbm95e4+IvXNq9TFj4iq1xfKJpMj1gHpRQvaA4geRkmj/pSm5fdacxjIoxTdigN7uxdZiZkjvhfgQtiCKmnaRpSoMRqhcU55Dsm8L3L8gX9rluhsrXMQz/3h2PKctXfWHxeCWEp5PsRJRggOoFRR+CpSPrpC3z5PP78qQXAgAoAWw6j2JbDqLrkeED1QtKwRDLXXz5KMlKA0Iu4E0wsgSqUIdGlwhDqF5QKBR3of4XFArFXaheUCgUd6F6QaFQ3IXqBYVCcReqFxQKxV2oXlAoFHehekGhUNyF6gWFQnEXqhcUCsVdqF5QKBR3oXpBoVDcheoFhUJxF6oXFArFXaheUCgUd6F6QaFQ3IXqBYVCcReqFxQKxV2oXlAoFHehekGhUNyF6gWFQnEXqhcUCsVdqF5QKBR3oXpBoVDcheoFhUJxF6oXFArFXaheUCgUd6F6QaFQ3IXqBYVCcReqFxQKxV2oXlAoFHehekGhUNyF6gWFQnEXqhcUCsVdqF5QKBR3oXpBoVDcheoFhUJxF6oXFArFXaheUCgUd6F6QaFQ3IXqBYVCcReqFxQKxV2oXlAoFHehekGhUNyF6gWFQnEXqhcUCsVdqF5QKBR3+T9IrnD7rqecWAAAAABJRU5ErkJggg==";
  if (props.error) {
    return <div>Something Went Wrong, Please try again</div>;
  }
  console.log("inside show transaction details", props.transactionDetails);
  const {
    approvalRefNumber,
    CustConvFee,
    RespAmount,
    RespBillDate,
    RespBillNumber,
    RespBillPeriod,
    RespCustomerName,
    RespDueDate,
    txnRefId,
    responseReason
  } = props.transactionDetails.ExtBillPayResponse;

  const { billerId, billerName } = props.selectedBiller;

  const { customerMobile } = props.customerInfo;

  const generatePdf = () => {
    const doc = new jsPDF();
    doc.addImage(imageData, "JPEG", 160, 15, 30, 20);
    doc.setFont("helvetica");
    doc.setFontType("bold");
    doc.setFontSize(18);
    doc.text("Bill Payment Transaction Details", 20, 35);
    doc.setFontType("normal");
    doc.setFontSize(12);
    doc.text("Transaction Id", 20, 45);
    doc.text(`${txnRefId}`, 80, 45);

    doc.text("Transaction Date", 20, 55);
    doc.text("14-08-2020 06:38:55", 80, 55);

    doc.text("Transaction Status", 20, 65);
    doc.text(`${responseReason}`, 80, 65);

    doc.text("Payment Channel", 20, 75);
    doc.text("AGT", 80, 75);

    doc.text("Biller Name", 20, 85);
    doc.text(`${billerName}`, 80, 85);

    doc.text("Biller Id", 20, 95);
    doc.text(`${billerId}`, 80, 95);

    doc.text("Consumer Name", 20, 105);
    doc.text(`${RespCustomerName}`, 80, 105);

    doc.text("Payment Mode", 20, 115);
    doc.text("Cash", 80, 115);

    doc.text("Payee Mobile No", 20, 125);
    doc.text(`${customerMobile}`, 80, 125);

    doc.text("Bill Amount", 20, 135);
    doc.text(`${parseFloat(RespAmount) / 100}`, 80, 135);

    doc.text("Consumer No", 20, 145);
    doc.text("", 80, 145);

    doc.text("Customer Conv Fee", 20, 155);
    doc.text(`${CustConvFee}`, 80, 155);

    doc.text("Bill Date", 20, 165);
    doc.text(`${RespBillDate}`, 80, 165);

    doc.text("Bill Period", 20, 175);
    doc.text(`${RespBillPeriod}`, 80, 175);

    doc.text("Bill Number", 20, 185);
    doc.text(`${RespBillNumber}`, 80, 185);

    doc.text("Due Date", 20, 195);
    doc.text(`${RespDueDate}`, 80, 195);

    doc.text("Total Amount", 20, 205);
    doc.text(`${parseFloat(RespAmount) / 100}`, 80, 205);

    doc.text("Approval Number", 20, 215);
    doc.text(`${approvalRefNumber}`, 80, 215);

    doc.save(`${txnRefId}.pdf`);
  };
  return (
    <>
      <Helmet>
        <title>Transaction Details</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo logo="BeAssured" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Transaction Details</Title>
            <TableContainer className={classes.table} component="div">
              <Table size="small" aria-label="a dense table">
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Transaction ID
                    </TableCell>
                    <TableCell align="right">{txnRefId}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Biller ID
                    </TableCell>
                    <TableCell align="right">{billerId}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Biller Name
                    </TableCell>
                    <TableCell align="right">{billerName}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Customer Name
                    </TableCell>
                    <TableCell align="right">{RespCustomerName}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Customer Number
                    </TableCell>
                    <TableCell align="right">{customerMobile}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Bill Date
                    </TableCell>
                    <TableCell align="right">{RespBillDate}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Bill Period
                    </TableCell>
                    <TableCell align="right">{RespBillPeriod}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Bill Number
                    </TableCell>
                    <TableCell align="right">{RespBillNumber}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Due Date
                    </TableCell>
                    <TableCell align="right">{RespDueDate}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Bill Amount
                    </TableCell>
                    <TableCell align="right">
                      {parseFloat(RespAmount) / 100}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Customer Convenience Fees
                    </TableCell>
                    <TableCell align="right">{CustConvFee}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Total amount
                    </TableCell>
                    <TableCell align="right">
                      {parseFloat(RespAmount) / 100}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Transaction Date and Time
                    </TableCell>
                    <TableCell align="right">{`14-08-2020 06:38:55`}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Initiating Channel
                    </TableCell>
                    <TableCell align="right">{`AGT`}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Payment Mode
                    </TableCell>
                    <TableCell align="right">{`Cash`}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Transaction Status
                    </TableCell>
                    <TableCell align="right">{responseReason}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Approval Number
                    </TableCell>
                    <TableCell align="right">{approvalRefNumber}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <Button
              className={classes.registerButton}
              size="large"
              color="primary"
              variant="contained"
              onClick={generatePdf}
            >
              Download
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

function mapStateToProps(state) {
  try {
    return {
      loading: state.toggleLoading.toggleLoading,
      transactionDetails: state.bbpsState.transactionDetails,
      selectedBiller: state.bbpsState.selectedBiller,
      customerInfo: state.bbpsState.customerInfo
    };
  } catch (error) {
    return { error: error };
  }
}

export default compose(connect(mapStateToProps, actions))(
  ShowTransactionDetails
);
