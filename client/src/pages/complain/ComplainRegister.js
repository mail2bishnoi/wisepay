import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import Title from "components/Title";
import ComplainDetails from "./ComplainDetails";
import DispositionSelect from "components/DispositionSelect";
import ComplainTypeRadio from "components/ComplainTypeRadio";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const ComplainRegister = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/complain-register");
  }, []);
  const { handleSubmit } = props;

  const onDispositionSelect = selectedDisposition => {
    console.log("selected disposition ", selectedDisposition);
  };

  const onSubmit = formValues => {
    console.log(formValues);
    props.complainRegister(formValues, result => {
      console.log(result);
      setOpen(true);
      //props.history.push("/show-bill-details");
    });
  };
  return (
    <>
      <Helmet>
        <title>Register Complain</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo logo="BharatBillPay" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Register Complain</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Field
                id="complaintType"
                name="complaintType"
                label="Complaint Type"
                component={ComplainTypeRadio}
                required={true}
                autoComplete="off"
                fullWidth
              />
              <Field
                id="txnRefId"
                name="txnRefId"
                label="Transaction Ref ID"
                component={renderInput}
                required={true}
                autoComplete="off"
                fullWidth
              />
              <Field
                id="complaintDisposition"
                name="complaintDisposition"
                label="Select Disposition"
                component={DispositionSelect}
                required={true}
                autoComplete="off"
                billers={props.billers}
                onChange={selectedDisposition =>
                  onDispositionSelect(selectedDisposition)
                }
              />
              <Field
                id="complaintDesc"
                name="complaintDesc"
                label="Description"
                component={renderInput}
                required={true}
                autoComplete="off"
                multiline
                rows={3}
                fullWidth
              />
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Register Complain"}
              </Button>
            </form>
            {open && (
              <ComplainDetails status={props.complainRegistrationStatus} />
            )}
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  txnRefId: {
    presence: { allowEmpty: false, message: "is required" }
  },
  complaintDisposition: {
    presence: { allowEmpty: false, message: "is required" }
  },
  complaintDesc: {
    presence: { allowEmpty: false, message: "is required" }
  }
};
const validater = formValues => {
  let errors = {};
  errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  if (!state.bbpsState.complainRegistrationStatus) {
    return {
      loading: state.toggleLoading.toggleLoading
    };
  }
  return {
    complainRegistrationStatus:
      state.bbpsState.complainRegistrationStatus.jsonResponse
        .complaintRegistrationResp,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "complainRegister", validate: validater })
)(ComplainRegister);
