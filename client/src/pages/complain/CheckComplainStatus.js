import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import BbpsLogo from "components/bbpsbrand/BbpsLogo";
import { makeStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import ComplainDetails from "./ComplainDetails";
import Title from "components/Title";
import ComplainTypeRadio from "components/ComplainTypeRadio";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const CheckComplainStatus = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  useEffect(() => {
    sessionStorage.setItem("lastScreen", "/check-complain-status");
  }, []);
  const { handleSubmit } = props;

  const onSubmit = formValues => {
    props.checkComplainStatus(formValues, result => {
      console.log(result);
      setOpen(true);
      //props.history.push("/show-bill-details");
    });
  };
  return (
    <>
      <Helmet>
        <title>Check Complain Status</title>
      </Helmet>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={4}>
          <BbpsLogo logo="BharatBillPay" />
          <Paper className={classes.paper} elevation={3}>
            <Title>Check Transaction Status</Title>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
              <Field
                id="complaintType"
                name="complaintType"
                label="Complaint Type"
                component={ComplainTypeRadio}
                required={true}
                autoComplete="off"
                fullWidth
              />
              <Field
                id="complaintId"
                name="complaintId"
                label="Complaint Id"
                component={renderInput}
                required={true}
                autoComplete="off"
                fullWidth
              />
              <Button
                className={classes.registerButton}
                size="large"
                color="primary"
                variant="contained"
                type="submit"
                disabled={!props.valid}
              >
                {props.loading ? <Loading /> : "Check Complain Status"}
              </Button>
            </form>
            {open && (
              <ComplainDetails status={props.complainRegistrationStatus} />
            )}
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

const schema = {
  complaintId: {
    presence: { allowEmpty: false, message: "is required" }
  }
};
const validater = formValues => {
  let errors = {};
  errors = validate(formValues, schema);
  return errors;
};
function mapStateToProps(state) {
  if (!state.bbpsState.complainRegistrationStatus) {
    return {
      loading: state.toggleLoading.toggleLoading
    };
  }
  return {
    complainRegistrationStatus:
      state.bbpsState.complainRegistrationStatus.jsonResponse
        .complaintTrackingResp,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "checkComplainStatus", validate: validater })
)(CheckComplainStatus);
