import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Title from "components/Title";
import Toolbar from "@material-ui/core/Toolbar";
import Moment from "react-moment";
import "moment-timezone";

const useStyles = makeStyles(theme => ({
  table: {
    marginTop: theme.spacing(2)
  }
}));

const ComplainDetails = props => {
  const status = props.status;
  const classes = useStyles();
  return (
    <>
      <Toolbar></Toolbar>
      <Title>Complain Registration Details</Title>
      <TableContainer className={classes.table} component="div">
        <Table size="small" aria-label="a dense table">
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                Complaint Id
              </TableCell>
              <TableCell align="right">{status.complaintId}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Complaint Assigned
              </TableCell>
              <TableCell align="right">{status.complaintAssigned}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Status
              </TableCell>
              <TableCell align="right">{status.responseReason}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default ComplainDetails;
