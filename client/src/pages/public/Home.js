import React from "react";
import { Helmet } from "react-helmet";
import {
  AppBar,
  Toolbar,
  Typography,
  Box,
  Card,
  CardHeader,
  CardMedia,
  CardActions,
  CardContent,
  Grid,
  Button,
  Container,
  Divider,
  List,
  ListItemIcon,
  ListItem,
  ListItemText,
  Link
} from "@material-ui/core";

import DoneIcon from "@material-ui/icons/Done";

import { makeStyles } from "@material-ui/core/styles";

import { Link as RouterLink } from "react-router-dom";
import H2 from "components/Heading/Secondary";

const useStyles = makeStyles(theme => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none"
    }
  },
  AppBar: {},
  title: {
    flexGrow: 1
  },
  links: {
    color: "inherit",
    textDecoration: "none"
  },
  hero: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 64,
    height: "100vh",
    backgroundImage: `linear-gradient(to right bottom, 
      rgba(63,81,181,0.8),
      rgba( 26,35,126 , .8)),url("https://images.unsplash.com/photo-1556740738-b6a63e27c4df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80")`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
  },
  heroContent: {
    width: "80vw",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  headingPrimary: {
    color: " #DAF7A6 "
  },
  headingPrimaryContent: {
    color: "#FFF"
  },
  textCenter: {
    textAlign: "center"
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  sectionServices: {
    marginTop: theme.spacing(3),
    padding: theme.spacing(3)
  },
  cardGrid: {
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(6)
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6)
    }
  }
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://wisepay.app/">
        JK SOFTWARE LABS LLP.
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}
const Home = () => {
  const classes = useStyles();
  return (
    <>
	      <Helmet>
        <title>Online Recharge & Bill Payments, Money Transfer and School Fee Payments</title>
      </Helmet>
      <AppBar position="fixed" className={classes.AppBar}>
        <Toolbar>
          <Typography
            variant="h4"
            color="inherit"
            noWrap
            className={classes.title}
          >
            <RouterLink to="/" className={classes.links}>
              WisePay
            </RouterLink>
          </Typography>
          <RouterLink to="/signin" className={classes.links}>
            Login
          </RouterLink>
        </Toolbar>
      </AppBar>
      <Box className={classes.hero}>
        <Box className={classes.heroContent}>
          <Typography
            variant="h2"
            className={classes.headingPrimary}
            gutterBottom
          >
            One Stop Solution for All Your Bills.
          </Typography>
          <Typography component="p" className={classes.headingPrimaryContent}>
            By using WisePay BBPS you can pay all your bills like Electricity,
            Water, Gas and Postpaid at one place. WisePay is directly connected
            to NPCI for BBPS Bill Payments.Also, Wisepay offers AEPS, Domestic
            Money Transfer and PAN card services.
          </Typography>
          <div className={classes.heroButtons}>
            <Button
              component={RouterLink}
              to="/signup"
              variant="contained"
              color="secondary"
            >
              Get Started
            </Button>
          </div>
        </Box>
      </Box>
      <Box className={classes.sectionServices}>
        <Box className={classes.textCenter}>
          <H2 className={classes.textCenter} gutterBottom>
            We Provide Awesome Services
          </H2>
        </Box>
        <Box className={classes.cardGrid}>
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6} md={4}>
              <Card variant="outlined">
                <CardHeader
                  title={
                    <Typography variant="h4" component="h2" color="primary">
                      Bill Payments
                    </Typography>
                  }
                />
                <CardContent>
                  <List>
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Bill Payments" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Mobile Recharge" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="DTH Recharge" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Gas Booking" />
                    </ListItem>
                  </List>
                </CardContent>
                <CardActions>
                  <Button variant="outlined" color="primary">
                    Read more
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardHeader
                  title={
                    <Typography variant="h4" component="h2" color="primary">
                      AePS
                    </Typography>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <List>
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Balance Check" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Cash Withdrawl" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Cash Deposite" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Money Transfer" />
                    </ListItem>
                  </List>
                </CardContent>
                <CardActions>
                  <Button variant="outlined" color="primary">
                    Read more
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardHeader
                  title={
                    <Typography variant="h4" component="h2" color="primary">
                      Domestic Money Transfer
                    </Typography>
                  }
                />
                <CardContent className={classes.cardContent}>
                  <List>
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Send Money Instantly" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Available 24/7" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Safe and Secure Transaction" />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    <ListItem alignItems="flex-start">
                      <ListItemIcon>
                        <DoneIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary="Instant Confirmation to Sender by SMS" />
                    </ListItem>
                  </List>
                </CardContent>
                <CardActions>
                  <Button variant="outlined" color="primary">
                    Read more
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Container component="footer" className={classes.footer}>
        <Grid container spacing={4} justify="space-evenly">
          <Grid item xs={6} sm={3}>
            <Typography variant="h6" color="textPrimary" gutterBottom>
              Company
            </Typography>
            <ul>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  About us
                </Link>
              </li>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  Contact us
                </Link>
              </li>
            </ul>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Typography variant="h6" color="textPrimary" gutterBottom>
              Products
            </Typography>
            <ul>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  BBPS Bill Payment
                </Link>
              </li>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  AePS
                </Link>
              </li>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  Domestic Money Transfer
                </Link>
              </li>
            </ul>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Typography variant="h6" color="textPrimary" gutterBottom>
              Legal
            </Typography>
            <ul>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  Privacy Policy
                </Link>
              </li>
              <li>
                <Link href="#" variant="subtitle1" color="textSecondary">
                  Terms of use
                </Link>
              </li>
            </ul>
          </Grid>
        </Grid>
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
    </>
  );
};

export default Home;
