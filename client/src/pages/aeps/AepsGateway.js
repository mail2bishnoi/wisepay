import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import * as actions from "store/actions";

class AepsGateway extends Component {
  aeps = null;
  openAePSGateway = () => {
    this.aeps.open();
  };
  componentDidMount = () => {
    sessionStorage.setItem("lastScreen", "/aeps-gateway");
    this.aeps = new window.EkoAEPSGateway();
    this.props.getEcoSecrets((secrets) => {
      this.aeps.config({
        partner_name: "JK SOFTWARE LABS LLP",
        initiator_logo_url:
          "https://wisepayapp.com/static/images/aeps-logo.png",
        initiator_id: secrets.a,
        developer_key: secrets.b,
        secret_key: secrets.c,
        secret_key_timestamp: secrets.d,
        user_code: this.props.user_code,
        environment: "uat",
      });
      this.aeps.setCallbackURL(
        "https://wisepayapp.com/api/eco/aeps/transaction"
      );
    });
  };
  render() {
    return (
      <>
        <Helmet>
          <title>Pay using Aadhar Card (AePS)</title>
        </Helmet>
        <Button
          size="large"
          color="primary"
          variant="outlined"
          onClick={this.openAePSGateway}
        >
          Pay using Aadhar Card (AePS)
        </Button>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    user_code: state.authState.user.ecousercode || 20810200,
  };
}
export default connect(mapStateToProps, actions)(AepsGateway);
