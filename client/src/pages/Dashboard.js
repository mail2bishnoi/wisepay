import React, { Component } from "react";
import { Helmet } from "react-helmet";

class Dashboard extends Component {
  componentDidMount() {
    sessionStorage.setItem("lastScreen", "/dashboard");
  }
  render() {
    return (
      <>
        <Helmet>
          <title>Dashboard</title>
        </Helmet>
        <div>Admin Dashboard</div>
      </>
    );
  }
}
export default Dashboard;
