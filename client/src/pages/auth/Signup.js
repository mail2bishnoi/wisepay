import React from "react";
import { Helmet } from "react-helmet";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import validate from "validate.js";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  links: {
    color: "inherit",
    textDecoration: "none"
  }
}));

const Signup = props => {
  const classes = useStyles();

  const { handleSubmit } = props;

  const onSubmit = function(formValues) {
    props.signup(formValues, response => {
      if (response.redirect) {
        props.history.push("/signin");
      }
    });
  };
  return (
    <>
      <Helmet>
        <title>Sign Up</title>
      </Helmet>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h4" color="inherit" noWrap>
            <Link to="/" className={classes.links}>
              WisePay
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
      <Container component="main" maxWidth="sm">
        <Toolbar></Toolbar>
        <div className={classes.paper}>
          <Typography component="h1" variant="h3" gutterBottom>
            Register for Agent
          </Typography>

          <Typography variant="subtitle1">
            Please fill the details below to register with wisepay.app
          </Typography>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
            noValidate
          >
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Field
                  name="firstname"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="firstname"
                  label="First Name:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="lastname"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="lastname"
                  label="Last Name:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="phone"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="phone"
                  label="Mobile:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="email"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="email"
                  label="Email:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="state"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="state"
                  label="State:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="city"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="city"
                  label="City:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="pincode"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="pincode"
                  label="Pin:"
                  autoComplete="off"
                />
              </Grid>
              <Grid item xs={6}>
                <Field
                  name="businessname"
                  type="text"
                  component={renderInput}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  required
                  fullWidth
                  id="businessname"
                  label="Shop/Business Name:"
                  autoComplete="off"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Create Account"}
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link to="/signin" className={classes.links}>
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </>
  );
};

const schema = {
  firstname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  lastname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  phone: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 10
    }
  },
  email: {
    presence: { allowEmpty: false, message: "is required" },
    email: true,
    length: {
      maximum: 128
    }
  },
  city: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  },
  pincode: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      is: 6
    }
  },
  businessname: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 128
    }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};

function mapStateToProps(state) {
  return {
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "signup", validate: validater })
)(Signup);
