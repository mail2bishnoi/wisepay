import React from "react";
import { Helmet } from "react-helmet";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "store/actions";
import { renderInput } from "components/Field";
import Loading from "components/progressbar/Loading";
import validate from "validate.js";

import { Card, CardActions, CardContent } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  links: {
    color: "inherit",
    textDecoration: "none"
  },
  details: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  image: {
    width: theme.spacing(9),
    height: theme.spacing(6)
  }
}));

const Signin = props => {
  const classes = useStyles();
  const { handleSubmit, isAuthenticated } = props;
  React.useEffect(() => {
    if (isAuthenticated) {
      const lastScreen = sessionStorage.getItem("lastScreen");
      if (lastScreen) return props.history.push(lastScreen);

      return props.history.push("/dashboard");
    }
  }, [isAuthenticated, props.history]);
  const onSubmit = function(formValues) {
    props.signin(formValues, result => {
      if (result.redirect) {
        props.history.push("/dashboard");
      }
    });
  };
  return (
    <>
      <Helmet>
        <title>Sign In</title>
      </Helmet>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h4" color="inherit" noWrap>
            <Link to="/" className={classes.links}>
              WisePay
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
      <Container component="main" maxWidth="xs">
        <Toolbar></Toolbar>
        <Card>
          <CardContent>
            <div className={classes.details}>
              <div>
                <Typography component="h1" variant="h1" color="primary">
                  WisePay
                </Typography>
              </div>
              <div>
                <img
                  alt="B"
                  className={classes.image}
                  src={`/static/images/B.png`}
                />
                <Typography component="h1" variant="h4" color="secondary">
                  Bill Pay
                </Typography>
              </div>
            </div>
          </CardContent>
        </Card>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
            noValidate
          >
            <Field
              name="email"
              type="text"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="email"
              label="Email:"
              autoComplete="off"
            />
            <Field
              name="password"
              type="password"
              component={renderInput}
              variant="outlined"
              margin="normal"
              size="small"
              required
              fullWidth
              id="password"
              label="Password:"
              autoComplete="off"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={!props.valid}
            >
              {props.loading ? <Loading /> : "Login to your account"}
            </Button>
            <Grid container>
              <Grid item xs></Grid>
              <Grid item>
                <Link to="/signup" className={classes.links}>
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </>
  );
};
const schema = {
  email: {
    presence: { allowEmpty: false, message: "is required" },
    email: true,
    length: {
      maximum: 128
    }
  },
  password: {
    presence: { allowEmpty: false, message: "is required" },
    length: {
      maximum: 64,
      minimum: 8
    }
  }
};
const validater = formValues => {
  const errors = validate(formValues, schema);
  return errors;
};

function mapStateToProps(state) {
  return {
    isAuthenticated: state.authState.isAuthenticated,
    loading: state.toggleLoading.toggleLoading
  };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin", validate: validater })
)(Signin);
