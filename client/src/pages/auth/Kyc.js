import React from "react";
import { Helmet } from "react-helmet";
import Typography from "@material-ui/core/Typography";

const Kyc = () => {
  return (
  <>
        <Helmet>
        <title>KYC Page</title>
      </Helmet>
    <Typography variant="h4" color="inherit">
      Your KYC is not complete. Please email ADHAAR Card, PAN Card, Passport
      Size Photo to "admin@wisepay.app". To use this service your kyc must be
      completed
    </Typography>
	</>
  );
};

export default Kyc;
