const crypto = require("crypto");
const md5 = require("md5");
const { v4: uuidv4 } = require("uuid");

exports.generateRandomString = () => {
  const fix = "jk1";
  const uuid36 = uuidv4();
  const uuid32 = uuid36.replace(/-/g, "");
  const uuid35 = uuid32.concat(fix);
  return uuid35;
};
exports.encrypt = (plainText, key) => {
  if (process.versions.openssl <= "1.0.1f") {
    throw new Error("openSSL Version too old, vulnerability to Heartbleed");
  }
  const md5Key = md5(key);
  const secretKey = Buffer.from(md5Key, "hex");
  const initVector = Buffer.from([
    0x00,
    0x01,
    0x02,
    0x03,
    0x04,
    0x05,
    0x06,
    0x07,
    0x08,
    0x09,
    0x0a,
    0x0b,
    0x0c,
    0x0d,
    0x0e,
    0x0f
  ]);
  const cipher = crypto.createCipheriv("aes-128-cbc", secretKey, initVector);

  const encryptedText =
    cipher.update(plainText, "utf8", "hex") + cipher.final("hex");
  return encryptedText;
};

exports.decrypt = (encryptedText, key) => {
  const md5Key = md5(key);
  const secretKey = Buffer.from(md5Key, "hex");
  const initVector = Buffer.from([
    0x00,
    0x01,
    0x02,
    0x03,
    0x04,
    0x05,
    0x06,
    0x07,
    0x08,
    0x09,
    0x0a,
    0x0b,
    0x0c,
    0x0d,
    0x0e,
    0x0f
  ]);
  encryptedText = Buffer.from(encryptedText, "hex");
  const decipher = crypto.createDecipheriv(
    "aes-128-cbc",
    secretKey,
    initVector
  );

  const decryptedText =
    decipher.update(encryptedText, "hex", "utf8") + decipher.final("utf8");
  return decryptedText;
};
