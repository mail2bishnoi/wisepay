exports.setBBPSParams = (encRequest, requestId) => {
    let params = {
        accessCode: "",
        instituteId: "JK01",
        ver: 1.0,
    };
    params = {
        ...params,
        encRequest: encRequest,
        requestId: requestId
    };
    return params;
};