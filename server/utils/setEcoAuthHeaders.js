const crypto = require("crypto");
const ecoPassword = ""; //"03c60613-26fa-4af2-ac2e-09de4f4d9732"; // key provided by Eko via email

function generateSecretKeys(key) {
    const buff = new Buffer.from(key);
    const encodedKey = buff.toString("base64");
    const secret_key_timestamp = "" + new Date().getTime();
    const secret_key = crypto
        .createHmac("sha256", encodedKey)
        .update(secret_key_timestamp)
        .digest()
        .toString("base64");

    return { secret_key, secret_key_timestamp };
}
exports.setEcoAuthHeaders = headersObject => {
    //console.log("inside setEcoAuth Headers ===>> ", headersObject);
    const { secret_key, secret_key_timestamp } = generateSecretKeys(ecoPassword);
    let headers = { ...headersObject };
    headers = {
        ...headers,
        developer_key: "becbbce45f79c6f5109f848acd540567",
        "secret-key": secret_key,
        "secret-key-timestamp": secret_key_timestamp
    };
    return headers;
};

exports.getSecretParams = () => {
    const { secret_key, secret_key_timestamp } = generateSecretKeys(ecoPassword);
    const developer_key = "becbbce45f79c6f5109f848acd540567";
    const initiator_id = "9962981729";

    const secretParams = { a: initiator_id, b: developer_key, c: secret_key, d: secret_key_timestamp }
    return secretParams;
}

exports.generateRequestHash = (salt) => {
    const buff = new Buffer.from(ecoPassword);
    const encodedKey = buff.toString("base64");
    const requestHash = crypto
        .createHmac("sha256", encodedKey)
        .update(salt)
        .digest()
        .toString("base64");

    return requestHash;
}