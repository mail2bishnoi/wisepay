exports.handleEcoError = response => {
  const { status, message } = response;
  return { message: `${status} ${message}` };
};
