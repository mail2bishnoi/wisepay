const x2jParser = require("fast-xml-parser");
const j2xParser = require("fast-xml-parser").j2xParser;

exports.json2xmlParser = jsonData => {
  const parserj2x = new j2xParser();
  return parserj2x.parse(jsonData);
};

exports.xml2jsonParser = xmlData => {
  return x2jParser.parse(xmlData);
};
