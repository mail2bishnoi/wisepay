const mongoose = require("mongoose");
const User = require("../models/user");
const keys = require("../config/keys");
mongoose
  .connect(keys.mongoURI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(async () => {
    console.log("mongodb connected successfuly");
    const user = new User({
      firstname: "Admin",
      lastname: "Admin",
      email: "admin@wisepay.app",
      email_verified: true,
      password: "12345678",
      role: "superadmin",
      activate: true
    });

    const admin = await User.findOne({ email: "admin@wisepay.app" });
    if (!admin) {
      await user.save();
      console.log("admin user created successfully");
    }
  })
  .catch(err =>
    console.log("something went wrong, database connection failed: ", err)
  );
