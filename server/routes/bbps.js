const express = require("express");
const router = express.Router();

const auth = require("../middlewares/auth");
const action = require("../middlewares/ActionMiddleware");
const bbps = require("../middlewares/bbps");
const credit = require("../middlewares/requireCredit");

const {
    BillerInfo,
    BillFetch,
    QuickBillPay,
    BillPay,
    TransactionStatus,
    TransactionComplainRegistration,
    ComplainTracking,
    getBBPSLog,
    getBBPSLogByAgentId
} = require("../controllers/bbps");

router.get("/api/billerInfo/12345678", bbps.BillerInfo, BillerInfo);
router.post("/api/billFetch", auth.simple, action.ActionLogger, bbps.BillFetch, BillFetch);
router.post("/api/quickBillPay", auth.simple, action.ActionLogger, credit.checkCredit, bbps.QuickBillPay, QuickBillPay);
router.post("/api/billPay", auth.simple, action.ActionLogger, credit.checkCredit, bbps.BillPay, BillPay);
router.post(
    "/api/transactionStatus",
    auth.simple,
    action.ActionLogger,
    bbps.TransactionStatus,
    TransactionStatus
);
router.post(
    "/api/transactionComplainRegistration",
    auth.simple,
    action.ActionLogger,
    bbps.TransactionComplainRegistration,
    TransactionComplainRegistration
);
router.post(
    "/api/complainTracking",
    auth.simple,
    action.ActionLogger,
    bbps.ComplainTracking,
    ComplainTracking
);
router.get("/api/getBBPSLog", auth.simple, action.ActionLogger, getBBPSLog);
router.get("/api/getBBPSLogByAgentId/:agentid", auth.enhance, action.ActionLogger, getBBPSLogByAgentId);

module.exports = router;