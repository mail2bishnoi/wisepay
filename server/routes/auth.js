const express = require("express");
const router = express.Router();

const auth = require("../middlewares/auth");
const action = require("../middlewares/ActionMiddleware");

const {
    signup,
    signin,
    signout,
    signoutAll,
    userInfo,
    listAllUsers,
    getUserById,
    updateUser,
    updateUserById,
    deleteUserById,
    deleteUser,
    changePassword,
    resetPassword,
    listActionHistory,
    addCredit
} = require("../controllers/auth");

router.post("/api/signup", signup);
router.post("/api/signin", signin);
router.post("/api/signout", auth.simple, action.ActionLogger, signout);
router.post("/api/signoutAll", auth.enhance, action.ActionLogger, signoutAll);
router.get("/api/users/me", auth.simple, action.ActionLogger, userInfo);

router.get("/api/users", auth.enhance, action.ActionLogger, listAllUsers);
router.get("/api/users/:id", auth.enhance, action.ActionLogger, getUserById);
router.patch("/api/users/:id", auth.enhance, action.ActionLogger, updateUserById);
router.delete("/api/users/:id", auth.enhance, action.ActionLogger, deleteUserById);
router.post("/api/users/me", auth.simple, action.ActionLogger, updateUser);
router.delete("/api/users/me", auth.simple, action.ActionLogger, deleteUser);
router.post("/api/users/changepassword", auth.simple, action.ActionLogger, changePassword);
router.post("/api/users/resetpassword", auth.enhance, action.ActionLogger, resetPassword);
router.get("/api/actions", auth.simple, action.ActionLogger, listActionHistory);
router.post("/api/users/credit/:id", auth.enhance, action.ActionLogger, addCredit);

module.exports = router;