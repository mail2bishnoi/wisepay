const express = require("express");
const router = express.Router();
const upload = require("../utils/multer");
const auth = require("../middlewares/auth");
const action = require("../middlewares/ActionMiddleware");
const ecoMiddleware = require("../middlewares/ecoMiddleware");


const {
    GetAllEcoServices,
    OnboardUser,
    ActivateService,
    ActivateServiceAePS,
    ListActivatedService,
    GetEcoSecretParams,
    AepsTransactionConfirmation
} = require("../controllers/eco");

router.get(
    "/api/eco/user/services",
    ecoMiddleware.getAllEcoServices,
    GetAllEcoServices
);

router.post("/api/eco/user/onboard", auth.enhance, action.ActionLogger, ecoMiddleware.onboardEcoUser, OnboardUser);

router.post(
    "/api/eco/user/service/activate",
    auth.enhance, action.ActionLogger,
    ecoMiddleware.activateService,
    ActivateService
);

var cpUpload = upload("users").fields([
    { name: "pan_card", maxCount: 1 },
    { name: "aadhar_front", maxCount: 1 },
    { name: "aadhar_back", maxCount: 1 }
]);
router.post(
    "/api/eco/user/service/activate/aeps",
    auth.enhance, action.ActionLogger,
    cpUpload,
    ecoMiddleware.activateServiceAePS,
    ActivateServiceAePS
);

router.get(
    "/api/eco/user/service/activated/:user_code",
    auth.enhance, action.ActionLogger,
    ecoMiddleware.listActivatedService,
    ListActivatedService
);

router.get("/api/eco/getecosecrets", auth.simple, ecoMiddleware.getEcoSecretParams, GetEcoSecretParams);

router.post("/api/eco/aeps/transaction", ecoMiddleware.aepsTransactionConfirmation, AepsTransactionConfirmation)
module.exports = router;