const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
var fs = require("fs");
var path = require("path");
var rfs = require("rotating-file-stream");
const { handleError } = require('./utils/error');

if (process.env.NODE_ENV !== "production") {
    require("dotenv").config({ path: path.join(__dirname, "../.env") });
}

require("./db/mongoose");

const authRouter = require("./routes/auth");
const ecoRouter = require("./routes/eco");
const bbpsRouter = require("./routes/bbps");

const app = express();
app.disable("x-powered-by");
app.use(cors());
var accessLogStream = rfs.createStream("access.log", {
    interval: "1d",
    path: path.join(__dirname, "logs")
});

app.use(morgan("dev", { stream: accessLogStream }));

// Serve static files from the React app
app.use(express.static(path.join(__dirname, "../client/build")));
app.use("/uploads", express.static(path.join(__dirname, "../uploads")));

app.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader("Access-Control-Allow-Origin", "*");

    // Request methods you wish to allow
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );

    // Request headers you wish to allow
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization"
    );

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});
app.use(express.json());
app.use(authRouter);
app.use(bbpsRouter);
app.use(ecoRouter);

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get("/*", (req, res) => {
    res.sendFile(path.join(__dirname + "../client/build/index.html"));
});

const port = process.env.PORT || 8000;
app.listen(port, () => console.info(`app is running in PORT: ${port}`));
