const User = require("../models/user");
const AepsWallet = require("../models/aepswallet");
exports.GetAllEcoServices = (req, res) => {
    res.status(200).send({ jsonResponse: req.servicesList });
};

exports.OnboardUser = async (req, res) => {
    const user = await User.findOneAndUpdate({ email: req.body.email }, { ecousercode: req.ecoUserCode }, { returnOriginal: false });
    res.status(200).send({ jsonResponse: user });
};

exports.ActivateService = (req, res) => {
    res.status(200).send({ jsonResponse: req.jsonResponse });
};

exports.ActivateServiceAePS = (req, res) => {
    res.status(200).send({ jsonResponse: req.jsonResponse });
};

exports.ListActivatedService = (req, res) => {
    res.status(200).send({ jsonResponse: req.jsonResponse });
};
exports.GetEcoSecretParams = (req, res) => {
    res.status(200).send({ jsonResponse: req.seretParams });
};
exports.AepsTransactionConfirmation = async (req, res) => {
    const transactionDetails = req.body;
    const secretKey = req.secretKey;
    const secretKeyTimestamp = req.secretKeyStamp;
    const reuestHash = req.requestHash;
    const post = {};
    let aepsWallet;
    let client_ref_id;


    if (transactionDetails.action === "debit-hook") {
        const data = transactionDetails.detail.data;
        client_ref_id = transactionDetails.detail.client_ref_id;
        const user = await User.findOne({ ecousercode: data.user_code });
        if (!user) {
            post.action = "go";
            post.allow = false;
            post.message = "User does not exist in the wisepay system";
            return res.send(post);
        }
        aepsWallet = new AepsWallet({ ecousercode: data.user_code, status: "pending", clientrefid: client_ref_id, customerid: data.customer_id });
        await aepsWallet.save();
        post.action = "go";
        post.allow = true;
        post.secret_key_timestamp = secretKeyTimestamp;
        post.secret_key = secretKey;
        post.request_hash = reuestHash;
        return res.send(post);

    } else if (transactionDetails.action === "eko-response" && transactionDetails.detail.is_debithook_response && transactionDetails.detail.is_final) {
        console.log("inside eco response");
        const response = transactionDetails.detail.response;
        const transactionData = transactionDetails.detail.response.data;
        client_ref_id = transactionDetails.detail.client_ref_id;

        if (response.status === 1) {
            aepsWallet = await AepsWallet.findOneAndUpdate({ clientrefid: client_ref_id }, { status: "failed" });
        } else if (response.status === 0) {
            aepsWallet = await AepsWallet.findOneAndUpdate({ clientrefid: client_ref_id }, { status: "success", amount: transactionData.amount, credit: transactionData.amount, debit: 0, narration: "aeps credit", tid: transactionData.tid, tds: transactionData.tds, commission: transactionData.commission });
        } else {
            aepsWallet = await AepsWallet.findOneAndUpdate({ clientrefid: client_ref_id }, { status: "unknown" });
        }
        res.status(200).send({});
    } else {
        res.status(400).send({});
    }

};