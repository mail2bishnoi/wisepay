const BBPSLogger = require("../models/bbps");
const Wallet = require("../models/wallet");


exports.BillerInfo = (req, res) => {
    res.status(200).send({ jsonResponse: req.jsonResponse, requestId: req.requestId });
};
exports.BillFetch = async (req, res) => {
    const { responseCode } = req.jsonResponse.billFetchResponse;
    const billAmount = responseCode === 0 ? req.jsonResponse.billFetchResponse.billAmount : 0;
    try {
        res.status(200).send({ jsonResponse: req.jsonResponse, requestId: req.requestId });
    } catch (e) {
        res.status(400).send({ e });
    }
};

exports.QuickBillPay = async (req, res) => {
    const { ExtBillPayResponse } = req.jsonResponse;
    const { responseCode } = ExtBillPayResponse;

    let txnrefid = 0,
        txnresptype = "NA",
        billAmount = 0,
        respamount = 0,
        responsereason = "NA";

    if (responseCode === 0) {
        txnrefid = ExtBillPayResponse.txnRefId;
        txnresptype = ExtBillPayResponse.txnRespType;
        respamount = ExtBillPayResponse.RespAmount;
        responsereason = ExtBillPayResponse.responseReason;
    }
    billAmount = responseCode === 0 ? req.jsonResponse.ExtBillPayResponse.billAmount : 0;
    const bbpsLogger = new BBPSLogger({
        requestid: req.requestId,
        agentid: req.agentId,
        jsonrequest: JSON.stringify(req.jsonRequest),
        jsonResponse: JSON.stringify(req.jsonResponse),
        requesttype: "quickpay",
        responsecode: responseCode,
        billamount: billAmount,
        txnrefid,
        txnresptype,
        respamount,
        responsereason


    })
    try {
        await bbpsLogger.save();
        res.status(200).send({ jsonResponse: req.jsonResponse, requestId: req.requestId, user: req.user });
    } catch (e) {
        res.status(400).send({ e });
    }
};
exports.BillPay = async (req, res) => {
    const { ExtBillPayResponse } = req.jsonResponse;
    const { responseCode } = ExtBillPayResponse;

    const credit = parseInt(req.user.credit) * 100;
    const inpBillAmount = parseInt(req.body.amountInfo.amount);
    let wallet;

    if (responseCode !== 0) {
        req.user.credit = (credit + inpBillAmount) / 100;
        wallet = new Wallet({
            debit: 0,
            credit: inpBillAmount / 100,
            user: req.user._id

        });
    }

    let txnrefid = 0,
        txnresptype = "NA",
        billAmount = 0,
        respamount = 0,
        responsereason = "NA";

    if (responseCode === 0) {
        txnrefid = ExtBillPayResponse.txnRefId;
        txnresptype = ExtBillPayResponse.txnRespType;
        respamount = ExtBillPayResponse.RespAmount;
        responsereason = ExtBillPayResponse.responseReason;
        billnumber = ExtBillPayResponse.RespBillNumber;
        customername = ExtBillPayResponse.RespCustomerName
    }
    billAmount = responseCode === 0 ? req.jsonResponse.ExtBillPayResponse.billAmount : 0;
    const bbpsLogger = new BBPSLogger({
        requestid: req.requestId,
        agentid: req.agentId,
        jsonResponse: JSON.stringify(req.jsonResponse.ExtBillPayResponse.inputParams),
        responsecode: responseCode,
        billamount: billAmount,
        txnrefid,
        txnresptype,
        respamount,
        responsereason,
        billnumber,
        customername


    })
    try {
        await bbpsLogger.save();
        if (responseCode !== 0) {
            await req.user.save();
            await wallet.save();
        }
        res.status(200).send({ jsonResponse: req.jsonResponse, requestId: req.requestId, user: req.user });
    } catch (e) {
        res.status(400).send({ e });
    }
};
exports.TransactionStatus = (req, res) => {
    res
        .status(200)
        .send({ jsonResponse: req.jsonResponse, requestId: req.requestId });
};
exports.TransactionComplainRegistration = (req, res) => {
    res
        .status(200)
        .send({ jsonResponse: req.jsonResponse, requestId: req.requestId });
};
exports.ComplainTracking = (req, res) => {
    res
        .status(200)
        .send({ jsonResponse: req.jsonResponse, requestId: req.requestId });
};

exports.getBBPSLog = async (req, res) => {

    try {
        const bbpsLogs = await BBPSLogger.find({ agentid: req.user.agentid }).sort({ _id: -1 });
        res.status(200).send({ jsonResponse: bbpsLogs, requestId: req.requestId });
    } catch (e) {
        res.status(400).send({ e });
    }

};
exports.getBBPSLogByAgentId = async (req, res) => {

    try {
        const bbpsLogs = await BBPSLogger.find({ agentid: req.params.agentid }).sort({ _id: -1 });
        res.status(200).send({ jsonResponse: bbpsLogs, requestId: req.requestId });
    } catch (e) {
        res.status(400).send({ e });
    }

};