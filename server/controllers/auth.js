const User = require("../models/user");
const Action = require("../models/action");
const Wallet = require("../models/wallet");

exports.signup = async (req, res) => {
    req.body.password = "12345678";
    console.log(req.body);
    try {
        const { role } = req.body;
        if (role) {
            return res.status(401).send({ message: "You are not allowed to set the role" })
        };
        const user = new User(req.body);
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({ user, token });
    } catch (e) {
        res.status(400).send(e);
    }
};
exports.signin = async (req, res) => {
    try {
        const user = await User.findByCredentials(
            req.body.email,
            req.body.password
        );
        const token = await user.generateAuthToken();
        res.status(200).send({ user, token });
    } catch (e) {
        res.status(400).send({ message: "You have entered an invalid email or password" });
    }
};
exports.signout = async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter(token => {
            return token.token !== req.token;
        });
        await req.user.save();
        res.status(200).send({ message: "You have logged out successfully" });
    } catch (e) {
        res.status(400).send(e);
    }
};
exports.signoutAll = async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send({ message: "User logged out successfully from all the devices" });
    } catch (e) {
        res.status(400).send(e);
    }
};
exports.userInfo = async (req, res) => {
    try {
        res.send(req.user);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.listAllUsers = async (req, res) => {
    if (req.user.role !== "superadmin")
        return res.status(401).send({
            message: "Only administrator can see all the users."
        });
    try {
        const users = await User.find({});
        res.status(200).send(users);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.getUserById = async (req, res) => {
    if (req.user.role !== "superadmin")
        return res.status(401).send({
            message: "Only administrator can see the user."
        });
    const _id = req.params.id;
    try {
        const user = await User.findById(_id);
        if (!user) return res.status(404).send({ message: "User does not exist in the system" });
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.updateUser = async (req, res) => {
    const updates = Object.keys(req.body);
    const { user } = req;
    const allowedUpdates = [
        "firstname",
        "middlename",
        "lastname",
        "dob",
        "phone",
        "username",
        "email",
        "state",
        "district",
        "area",
        "city",
        "pincode",
        "line",
        "businessname"
    ];
    const isValidOperation = updates.every(update =>
        allowedUpdates.includes(update)
    );
    if (!isValidOperation)
        return res.status(401).send({
            message: "Invalid updates!"
        });
    if (user.kyc === true) {
        return res.status(401).send({ message: "You can not update your profile after kyc is done. Please contact administrator" });
    }
    try {
        updates.forEach(update => (user[update] = req.body[update]));
        await user.save();
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
};
exports.changePassword = async (req, res) => {
    const { password, newpassword } = req.body;

    try {
        const user = await User.findByCredentials(req.user.email, password);
        if (!user) {
            return res.status(404).send({ message: "No user found for the given email." });
        }
        user.password = newpassword;
        user.tokens = [];
        await user.save();
        res.send({});
    } catch (e) {
        res.status(400).send(e);
    }
};
exports.resetPassword = async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(404).send({ message: "No user found for the given email" })
        };
        user.password = password;
        await user.save();
        res.status(200).send({ message: "Password has been successfully reset" })
    } catch (e) {
        return res.status(500).send({ message: "Internal server error" })
    }
}
exports.updateUserById = async (req, res) => {
    console.log(req.body);
    if (req.user.role !== "superadmin")
        return res.status(401).send({
            message: "Only administrator can update the user."
        });
    const _id = req.params.id;

    const updates = Object.keys(req.body);
    const allowedUpdates = [
        "firstname",
        "middlename",
        "lastname",
        "dob",
        "phone",
        "username",
        "email",
        "state",
        "district",
        "area",
        "city",
        "pincode",
        "line",
        "businessname",
        "kyc",
        "activate",
        "credit",
        "agentid"
    ];
    const isValidOperation = updates.every(update =>
        allowedUpdates.includes(update)
    );

    if (!isValidOperation)
        return res.status(401).send({ message: "Invalid updates!" });

    try {
        const user = await User.findById(_id);
        updates.forEach(update => (user[update] = req.body[update]));
        await user.save();

        if (!user) return res.status(404).send({ message: "User does not exist in the system." });
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.deleteUserById = async (req, res) => {
    if (req.user.role !== "superadmin")
        return res.status(401).send({
            message: "Only administrator can delete the user."
        });
    const _id = req.params.id;

    try {
        const user = await User.findByIdAndDelete(_id);
        if (!user) return res.status(404).send({ message: "User does not exists in the system" });

        res.status(200).send({ message: "User deleted successfully" });
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.deleteUser = async (req, res) => {
    if (req.user.role !== "superadmin")
        return res.status(400).send({
            message: "You cannot delete yourself!"
        });
    try {
        await req.user.remove();
        res.send(req.user);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.listActionHistory = async (req, res) => {
    try {
        const limit = parseInt(req.query.limit); // Make sure to parse the limit to number
        const page = parseInt(req.query.page); // Make sure to parse the skip to number
        const skip = (page - 1) * limit;
        const count = await Action.count({});
        const actions = await Action.find({ username: req.user.email }).sort({ _id: -1 }).skip(skip).limit(limit);
        res.status(200).send({ actions, count });
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.addCredit = async (req, res) => {
    if (req.user.role !== "superadmin")
        return res.status(401).send({
            message: "Only administrator can add credit."
        });

    const _id = req.params.id;
    const credit = parseFloat(req.body.credit);
    const wallet = new Wallet({
        debit: 0,
        credit: credit,
        user: _id

    });
    try {
        const user = await User.findById(_id);
        if (!user) return res.status(404).send({ message: "User does not exists in the system" });
        user.credit = parseFloat(user.credit) + credit;
        await user.save();
        await wallet.save();
        res.status(200).send({ message: "Added Credit successfully" });
    } catch (e) {
        res.status(400).send({ message: "Something went wrong. Please try again" });
    }
}