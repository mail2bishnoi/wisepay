const mongoose = require("mongoose");
const { Schema } = mongoose;

const bbpsSchema = new Schema({
    agentid: {
        type: String,
        trim: true,
        required: true
    },
    requestid: {
        type: String,
        required: true,
        default: Date.now
    },
    responsereason: {
        type: String,
        required: false
    },
    txnrefid: {
        type: String
    },
    txnresptype: {
        type: String
    },
    billAmount: {
        type: Number
    },
    respamount: {
        type: Number
    },
    jsonResponse: {
        type: String
    },
    billnumber: {
        type: String
    },
    customername: {
        type: String
    },
    time: {
        type: Date,
        required: true,
        default: Date.now
    }
})

const BBPSLogger = mongoose.model("bbps", bbpsSchema);

module.exports = BBPSLogger;