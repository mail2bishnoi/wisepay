const mongoose = require("mongoose");
const { Schema } = mongoose;
const walletSchema = new Schema({
    debit: {
        type: Number,
        required: true,
        default: 0
    },
    credit: {
        type: Number,
        required: true,
        default: 0
    },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    time: {
        type: Date,
        required: true,
        default: Date.now
    }
})

const Wallet = mongoose.model("wallet", walletSchema);

module.exports = Wallet;