const mongoose = require("mongoose");
const { Schema } = mongoose;
const aepsWalletSchema = new Schema({
    ecousercode: {
        type: Number,
        required: true
    },
    clientrefid: {
        type: String,
        required: true,
        unique: true,
    },
    status: {
        type: String,
        required: true
    },
    tid: {
        type: Number
    },
    amount: {
        type: Number
    },
    customerid: {
        type: String,
        required: true
    },
    debit: {
        type: Number
    },
    credit: {
        type: Number
    },
    balance: {
        type: Number
    },
    narration: {
        type: String
    },
    tds: {
        type: Number
    },
    commission: {
        type: Number
    }
})

const AepsWallet = mongoose.model("aepswallet", aepsWalletSchema);

module.exports = AepsWallet;