const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { Schema } = mongoose;

const userSchema = new Schema({
    agentid: {
        type: String
    },
    ecousercode: {
        type: Number,
        required: false
    },
    firstname: {
        type: String,
        trim: true,
        required: true,
        max: 128
    },
    middlename: {
        type: String,
        trim: true,
        required: false,
        max: 128
    },
    lastname: {
        type: String,
        trim: true,
        required: true,
        max: 128
    },
    dob: {
        type: Date,
        required: false
    },
    phone: {
        type: String,
        unique: true,
        trim: true,
        validate(value) {
            if (!validator.isMobilePhone(value)) {
                throw new Error("Phone is invalid");
            }
        }
    },
    phone_verified: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        trim: true,
        required: true,
        lowercase: true,
        unique: true
    },
    email_verified: {
        type: Boolean,
        default: false
    },
    state: {
        type: String,
        trim: true
    },
    district: {
        type: String,
        trim: true,
        required: false,
        max: 128
    },
    area: {
        type: String,
        trim: true,
        required: false,
        max: 128
    },
    city: {
        type: String,
        trim: true
    },
    pincode: {
        type: String,
        trim: true
    },
    line: {
        type: String,
        trim: true
    },
    businessname: {
        type: String,
        trim: true
    },
    password: {
        type: String,
        trim: true,
        minlength: 7,
        validate(value) {
            if (value.toLowerCase().includes("password")) {
                throw new Error("Password should not contain word: password");
            }
        }
    },
    salt: String,
    role: {
        type: String,
        default: "guest",
        enum: ["guest", "agent", "superadmin"]
    },
    credit: {
        type: Number,
        default: 0
    },
    kyc: {
        type: Boolean,
        default: false
    },
    activate: {
        type: Boolean,
        default: false
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    devicenumber: {
        type: Number,
        trim: true,
        required: false,
        unique: true
    },
    modelname: {
        type: String,
        trim: true,
        lowercase: true,
    },
    pannumber: {
        type: String,
        trim: true,
        required: false,
        unique: true
    },
    lat: {
        type: Number
    },
    lng: {
        type: Number
    },
    pancardurl: String,
    pancards3url: String,
    aadharfronturl: String,
    aadharfronts3url: String,
    aadharbackurl: String,
    aadharbacks3url: String,
    votercardfronturl: String,
    votercardfronts3url: String,
    votercardbackurl: String,
    votercardbackurl: String
}, { timestamps: true });

userSchema.methods.toJSON = function() {
    const user = this;
    const userObject = user.toObject();
    if (!userObject.role === "superadmin") {
        delete userObject.updatedAt;
        delete userObject.__v;
    }
    delete userObject.password;
    delete userObject.tokens;

    return userObject;
};

userSchema.methods.generateAuthToken = async function() {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, "mySecret");
    user.tokens = user.tokens.concat({ token });
    await user.save();
    return token;
};

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email });
    if (!user) throw new Error("User Dosn't exists");

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) throw new Error("Unable to login");

    return user;
};

// Hash the plain text password before save
userSchema.pre("save", async function(next) {
    const user = this;
    if (user.isModified("password")) {
        user.password = await bcrypt.hash(user.password, 8);
    }
    next();
});

const User = mongoose.model("user", userSchema);

module.exports = User;