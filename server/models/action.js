const mongoose = require("mongoose");
const { Schema } = mongoose;

const actionSchema = new Schema({
    action: {
        type: String,
        trim: true,
        required: true
    },
    actiontime: {
        type: Date,
        required: true,
        default: Date.now
    },
    username: {
        type: String,
        required: true
    }
})

const Action = mongoose.model("action", actionSchema);

module.exports = Action;