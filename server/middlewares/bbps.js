const axios = require("axios");
const fs = require("fs");
const https = require("https");
const crypto = require("./../utils/crypto");
const parser = require("./../utils/json2xml");
const params = require("./../utils/setBBPSParams");

const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
    cert: fs
        .readFileSync(
            "C:/projects/wisepay-prod/wisepay/server/config/billavenueUATCertificate.crt"
        )
        .toString()
});

const bbpsAPI = axios.create({
    baseURL: "https://api.billavenue.com/billpay/",
    headers: {
        Accept: "*/*",
        "Content-Type": "text/html; charset=UTF-8"
    },
    httpsAgent: httpsAgent
});

const key = "";

const BillerInfo = async (req, res, next) => {
    const plainText = `<?xml version="1.0" encoding="UTF-8"?><billerInfoRequest></billerInfoRequest>`;
    const encRequest = crypto.encrypt(plainText, key);
    const requestId = crypto.generateRandomString();
    try {
        const response = await bbpsAPI.post(
            "extMdmCntrl/mdmRequest/xml",
            null, {
                params: params.setBBPSParams(encRequest, requestId)
            }
        );
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.requestId = requestId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};

const BillFetch = async (req, res, next) => {
    try {
        const customerInfo = parser.json2xmlParser(req.body.customerInfo);
        const inputParams = parser.json2xmlParser(req.body.inputParams);
        const billerId = req.body.billerId;
        const agentId = req.user.agentid;
        if (!agentId) {
            return res.status(400).send({ message: "Your agent id is not created. Please contact system administrator." });
        }

        const plainText = `<?xml version="1.0" encoding="UTF-8"?>
  <billFetchRequest>
  <agentId>${agentId}</agentId>
   <agentDeviceInfo>
        <ip>192.168.2.73</ip>
        <initChannel>AGT</initChannel>
        <mac>01-23-45-67-89-ab</mac>
     </agentDeviceInfo>
   <customerInfo>
    ${customerInfo}
     </customerInfo>
  <billerId>${billerId}</billerId>
   <inputParams>
    ${inputParams}
   </inputParams>
     </billFetchRequest>`;
        const encRequest = crypto.encrypt(plainText, key);
        const requestId = crypto.generateRandomString();
        const response = await bbpsAPI.post(
            "extBillCntrl/billFetchRequest/xml",
            null, {
                params: params.setBBPSParams(encRequest, requestId)
            }
        );
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.jsonRequest = parser.xml2jsonParser(plainText);
        req.requestId = requestId;
        req.agentId = agentId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};

const QuickBillPay = async (req, res, next) => {
    const amountInfoXML = parser.json2xmlParser(req.body.amountInfo);
    const paymentMethodXML = parser.json2xmlParser(req.body.paymentMethod);
    const paymentInfoXML = parser.json2xmlParser(req.body.paymentInfo);

    const agentId = req.user.agentid;

    const plainText = `<?xml version="1.0" encoding="UTF-8"?>
  <billPaymentRequest>
  <agentId>${agentId}</agentId>
  <billerAdhoc>true</billerAdhoc>
    <agentDeviceInfo>
          <ip>192.168.2.73</ip>
          <initChannel>AGT</initChannel>
          <mac>01-23-45-67-89-ab</mac>
      </agentDeviceInfo>
      <customerInfo>
          <customerMobile>9898990083</customerMobile>
          <customerEmail></customerEmail>
          <customerAdhaar></customerAdhaar>
          <customerPan></customerPan>
      </customerInfo>
    <billerId>OTNS00005XXZ43</billerId>
    <inputParams>
      <input>
         <paramName>a</paramName>
         <paramValue>10</paramValue>
      </input>
      <input>
         <paramName>a b</paramName>
         <paramValue>20</paramValue>
      </input>
      <input>
         <paramName>a b c</paramName>
         <paramValue>30</paramValue>
      </input>
      <input>
         <paramName>a b c d</paramName>
         <paramValue>40</paramValue>
      </input>
      <input>
         <paramName>a b c d e</paramName>
         <paramValue>50</paramValue>
      </input>
   </inputParams>
    <amountInfo>
    ${amountInfoXML}
    </amountInfo>
    <paymentMethod>
    ${paymentMethodXML}
    </paymentMethod>
    <paymentInfo>
    ${paymentInfoXML}
    </paymentInfo>
   </billPaymentRequest>`;

    const encRequest = crypto.encrypt(plainText, key);
    const requestId = crypto.generateRandomString();
    try {
        const response = await bbpsAPI.post(
            "extBillPayCntrl/billPayRequest/xml",
            null, {
                params: params.setBBPSParams(encRequest, requestId)
            }
        );
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.jsonRequest = parser.xml2jsonParser(plainText);
        req.requestId = requestId;
        req.agentId = agentId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};

const BillPay = async (req, res, next) => {
    const inputParamsXML = parser.json2xmlParser(req.body.inputParams);
    const billerResponseXML = parser.json2xmlParser(req.body.billerResponse);
    const additionalInfoXML = parser.json2xmlParser(req.body.additionalInfo);
    const amountInfoXML = parser.json2xmlParser(req.body.amountInfo);
    const paymentMethodXML = parser.json2xmlParser(req.body.paymentMethod);
    const paymentInfoXML = parser.json2xmlParser(req.body.paymentInfo);

    const agentId = req.user.agentid;

    const plainText = `<?xml version="1.0" encoding="UTF-8"?>
  <billPaymentRequest>
  <agentId>${agentId}</agentId>
  <billerAdhoc>true</billerAdhoc>
    <agentDeviceInfo>
          <ip>192.168.2.73</ip>
          <initChannel>AGT</initChannel>
          <mac>01-23-45-67-89-ab</mac>
      </agentDeviceInfo>
      <customerInfo>
          <customerMobile>9898990084</customerMobile>
          <customerEmail></customerEmail>
          <customerAdhaar></customerAdhaar>
          <customerPan></customerPan>
      </customerInfo>
    <billerId>OTME00005XXZ43</billerId>
    <inputParams>
      ${inputParamsXML}
    </inputParams>
    <billerResponse>
      ${billerResponseXML}
    </billerResponse>
    <additionalInfo>
      ${additionalInfoXML}
    </additionalInfo>
    <amountInfo>
    ${amountInfoXML}
    </amountInfo>
    <paymentMethod>
    ${paymentMethodXML}
    </paymentMethod>
    <paymentInfo>
    ${paymentInfoXML}
    </paymentInfo>
   </billPaymentRequest>`;

    const encRequest = crypto.encrypt(plainText, key);
    let requestId;
    if (req.body.requestId && req.body.requestId !== 0) {
        requestId = req.body.requestId;
    } else {
        requestId = crypto.generateRandomString();
    }

    try {
        const response = await bbpsAPI.post(
            "extBillPayCntrl/billPayRequest/xml",
            null, {
                params: params.setBBPSParams(encRequest, requestId)
            }
        );
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.requestId = requestId;
        req.agentId = agentId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};

const TransactionStatus = async (req, res, next) => {
    const trackValue = req.body.trackValue;
    const plainText = `<?xml version="1.0" encoding="UTF-8"?>
  <transactionStatusReq>
     <trackType>TRANS_REF_ID</trackType>
     <trackValue>${trackValue}</trackValue>
  </transactionStatusReq>`;
    const encRequest = crypto.encrypt(plainText, key);
    const requestId = crypto.generateRandomString();
    try {
        const response = await bbpsAPI.post(
            "transactionStatus/fetchInfo/xml",
            null, {
                params: params.setBBPSParams(encRequest, requestId)
            }
        );
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.requestId = requestId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};

const TransactionComplainRegistration = async (req, res, next) => {
    const txnRefId = req.body.txnRefId;
    const complaintDesc = req.body.complaintDesc;
    const complaintDisposition = req.body.complaintDisposition.dispositionType;

    const plainText = `<?xml version="1.0" encoding="UTF-8"?>
  <complaintRegistrationReq>
  <complaintType>Transaction</complaintType>
     <participationType />
     <agentId />
     <txnRefId>${txnRefId}</txnRefId>
     <billerId />
     <complaintDesc>${complaintDesc}</complaintDesc>
     <servReason />
     <complaintDisposition>${complaintDisposition}</complaintDisposition>
  </complaintRegistrationReq>`;

    const encRequest = crypto.encrypt(plainText, key);
    const requestId = crypto.generateRandomString(35);
    try {
        const response = await bbpsAPI.post("extComplaints/register/xml", null, {
            params: params.setBBPSParams(encRequest, requestId)
        });
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.requestId = requestId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};

const ComplainTracking = async (req, res, next) => {
    const complaintId = req.body.complaintId;
    const plainText = `<?xml version="1.0" encoding="UTF-8"?>
  <complaintTrackingReq>
     <complaintType>Transaction</complaintType>
     <complaintId>${complaintId}</complaintId>
  </complaintTrackingReq>`;

    const encRequest = crypto.encrypt(plainText, key);
    const requestId = crypto.generateRandomString();
    try {
        const response = await bbpsAPI.post("extComplaints/track/xml", null, {
            params: params.setBBPSParams(encRequest, requestId)
        });
        const xmlResponse = crypto.decrypt(response.data, key);
        req.jsonResponse = parser.xml2jsonParser(xmlResponse);
        req.requestId = requestId;
        next();
    } catch (error) {
        res.status(401).send({ error });
    }
};
module.exports = {
    BillerInfo,
    BillFetch,
    QuickBillPay,
    BillPay,
    TransactionStatus,
    TransactionComplainRegistration,
    ComplainTracking
};