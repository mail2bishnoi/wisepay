const Wallet = require("../models/wallet");

const checkCredit = async (req, res, next) => {

    try {
        const credit = parseInt(req.user.credit) * 100;
        const billAmount = parseInt(req.body.amountInfo.amount);
        if (credit < billAmount) {
            return res.status(403).send({ message: "Not enough credits" });
        }
        const wallet = new Wallet({
            debit: billAmount / 100,
            credit: 0,
            user: req.user._id

        });
        req.user.credit = (credit - billAmount) / 100;
        await req.user.save();
        await wallet.save();
        next();
    } catch (e) {
        console.log(e);
        res.status(400).send({ message: "Something went wrong. Please try again" });
    }
}

module.exports = { checkCredit };