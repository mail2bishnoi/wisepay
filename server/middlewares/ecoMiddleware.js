const axios = require("axios");
const https = require("https");
const qs = require("qs");
var FormData = require("form-data");
var fs = require("fs");

const headers = require("../utils/setEcoAuthHeaders");
const errors = require("../utils/errorHandling");

const httpsAgent = new https.Agent({
    rejectUnauthorized: false
});

const ecoAPI = axios.create({
    baseURL: "https://staging.eko.in:25004/ekoapi/v1/",
    headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
    },
    httpsAgent: httpsAgent
});

const getAllEcoServices = async (req, res, next) => {
    try {
        const response = await ecoAPI.get("user/services", {
            params: { initiator_id: 9962981729 },
            headers: headers.setEcoAuthHeaders()
        });
        if (response.data.status !== 0) {
            return res.status(400).send(errors.handleEcoError(response.data));
        }
        req.servicesList = response.data.data.service_list;
        next();
    } catch (e) {
        res.status(400).send(e);
    }
};

const onboardEcoUser = async (req, res, next) => {
    const ecoUser = {};
    const residence_address = `{"line":"${req.body.line}","city":"${req.body.city}","state":"${req.body.state}","pincode":"${req.body.pincode}","district":"${req.body.district}","area":"${req.body.area}"}`;
    ecoUser.initiator_id = req.body.initiator_id;
    ecoUser.pan_number = req.body.pannumber;
    ecoUser.mobile = req.body.phone;
    ecoUser.first_name = req.body.firstname;
    ecoUser.last_name = req.body.lastname;
    ecoUser.dob = req.body.dob;
    ecoUser.shop_name = req.body.businessname;
    ecoUser.email = req.body.email;
    ecoUser["residence_address"] = residence_address;

    const data = qs.stringify(ecoUser);
    try {
        const response = await ecoAPI.put("user/onboard", data, {
            headers: headers.setEcoAuthHeaders()
        });
        if (response.data.status !== 0) {
            return res.status(400).send(errors.handleEcoError(response.data));
        }
        req.ecoUserCode = response.data.data.user_code;
        next();
    } catch (e) {
        res.status(400).send(e);
    }
};

const activateService = async (req, res, next) => {
    const user = {};
    user.initiator_id = req.body.initiator_id;
    user.user_code = req.body.user_code;
    user.service_code = req.body.service_code;
    const data = qs.stringify(user);
    try {
        const response = await ecoAPI.put("user/service/activate", data, {
            headers: headers.setEcoAuthHeaders()
        });
        if (response.data.status !== 0) {
            return res.status(400).send(errors.handleEcoError(response.data));
        }
        req.jsonResponse = response.data;
        next();
    } catch (e) {
        res.status(400).send(e);
    }
};
const activateServiceAePS = async (req, res, next) => {
    const { pan_card, aadhar_front, aadhar_back } = req.files;

    const pan_card_file = fs.createReadStream(pan_card[0].path);
    const aadhar_front_file = fs.createReadStream(aadhar_front[0].path);
    const aadhar_back_file = fs.createReadStream(aadhar_back[0].path);
    const residence_address = `{"line":"${req.body.line}","city":"${req.body.city}","state":"${req.body.state}","pincode":"${req.body.pincode}"}`;

    const formData = `service_code=${req.body.service_code}&initiator_id=${req.body.initiator_id}&user_code=${req.body.user_code}&devicenumber=${req.body.devicenumber}&modelname={req.body.modelname}&office_address=${residence_address}&address_as_per_proof=${residence_address}`;

    var data = new FormData();
    data.append("form-data", formData);

    data.append("pan_card", pan_card_file);
    data.append("aadhar_front", aadhar_front_file);
    data.append("aadhar_back", aadhar_back_file);
    try {
        const response = await ecoAPI.put("user/service/activate", data, {
            headers: headers.setEcoAuthHeaders(data.getHeaders())
        });
        if (response.data.status !== 0) {
            return res.status(400).send(errors.handleEcoError(response.data));
        }
        req.jsonResponse = response.data;
        next();
    } catch (e) {
        res.status(400).send(e);
    }
};

const listActivatedService = async (req, res, next) => {
    try {
        const response = await ecoAPI.get(
            `user/services/${req.params.user_code}?initiator_id=9962981729`, {
                headers: headers.setEcoAuthHeaders()
            }
        );
        if (response.data.status !== 0) {
            return res.status(400).send(errors.handleEcoError(response.data));
        }
        req.jsonResponse = response.data.data.service_status_list;
        next();
    } catch (e) {
        res.status(400).send(e);
    }
};

const getEcoSecretParams = (req, res, next) => {
    req.seretParams = headers.getSecretParams();
    next();

}

const aepsTransactionConfirmation = (req, res, next) => {
    console.log("entry into eco middleware and body is ", req.body);
    const transactionRequest = req.body;
    let salt;
    // for confirming transaction
    if (transactionRequest.action === "debit-hook") {
        const { type, customer_id, amount, user_code } = transactionRequest.detail.data
        const { c, d } = headers.getSecretParams();
        req.secretKey = c;
        req.secretKeyStamp = d;
        if (type === '2') {
            salt = `${d}${customer_id}${amount}${user_code}`;

        } else if (type === '3' || type === '4') {
            salt = `${d}${customer_id}${user_code}`;
        } else {
            salt = `${d}`
        }

        req.requestHash = headers.generateRequestHash(salt);
    }
    next();
}

module.exports = {
    getAllEcoServices,
    onboardEcoUser,
    activateService,
    activateServiceAePS,
    listActivatedService,
    getEcoSecretParams,
    aepsTransactionConfirmation
};