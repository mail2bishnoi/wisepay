const jwt = require("jsonwebtoken");
const User = require("../models/user");

const simple = async (req, res, next) => {
    console.log(req.header("Authorization"));
    try {
        const token = req.header("Authorization").replace("Bearer ", "");
        const decoded = jwt.verify(token, "mySecret");
        const user = await User.findOne({
            _id: decoded._id,
            "tokens.token": token
        });
        if (!user) {
            return res.status(404).send({ message: "No user exists in the system for given email or username" })
        };
        req.token = token;
        req.user = user;
        next();
    } catch (e) {
        res.status(401).send({ message: "Please authenticate." });
    }
};

const enhance = async (req, res, next) => {
    try {
        const token = req.header("Authorization").replace("Bearer ", "");
        const decoded = jwt.verify(token, "mySecret");
        const user = await User.findOne({
            _id: decoded._id,
            "tokens.token": token
        });
        if (!user || user.role !== "superadmin") {
            return res.staus(404).send({ message: "No user exists in the system for given email or password" })
        }
        req.token = token;
        req.user = user;
        next();
    } catch (e) {
        res.status(401).send({ error: "Please authenticate." });
    }
};

module.exports = { simple, enhance };