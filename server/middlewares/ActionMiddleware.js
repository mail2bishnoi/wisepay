const Action = require("../models/action");

const ActionLogger = async (req, res, next) => {
    const { email } = req.user;

    const action = new Action({ username: email, action: req.originalUrl });
    try {
        await action.save();
        next();
    } catch (e) {
        res.status(400).send(e);
    }
};

module.exports = { ActionLogger };